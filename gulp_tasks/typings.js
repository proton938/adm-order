const gulp = require('gulp');
const gulpTypings = require("gulp-typings");
 
const conf = require('../conf/gulp.conf');

gulp.task("installTypings", installTypings);

function installTypings() {
	return gulp.src("typings.json").pipe(gulpTypings())
}