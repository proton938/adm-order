const gulp = require('gulp');
const tslint = require('gulp-tslint');
const typescript = require('gulp-typescript');
const tsConf = require('../tsconfig.json').compilerOptions;
const conf = require('../conf/gulp.conf');

var map = require('map-stream');
var vfs = require('vinyl-fs');

gulp.task('mockscr', mockScripts);

function mockScripts() {
    var mocksArray = [];
    var collect = function (file, cb) {
        var fired = false;
        if (file.path.indexOf('mocks') !== -1 && file.path.indexOf('app') !== -1) {
            console.log("Mock file from path " + file.path);
            mocksArray.push(file.path.substr(file.path.lastIndexOf('\\') + 1));
            cb(null, file);
            fired = true;
        }

        var isMockExist = false;
        mocksArray.every(function (entry) {
            if (file.path.indexOf(entry) !== -1) {
                isMockExist = true;
                return false;
            }
            else return true;
        });

        if (!isMockExist) {
            cb(null, file);
            fired = true;
        }
        if (!fired)
            cb();
    };


    return gulp.src([conf.path.src('**/mocks/*.ts'), conf.path.src('**/*.ts'), 'typings/**/*.d.ts'])
        .pipe(map(collect))
        .pipe(typescript(tsConf))
        .pipe(gulp.dest(conf.path.tmp()));
}
