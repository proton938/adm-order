class OrderType {
    code: string;
    name: string;
    accessEI: string;
    singElectron: boolean;
}