class ApproveDocument {
    approveDocumentType: string;
    approveDocumentNumber: string;
    approveDocumentDate: string;
    approveOrganization: string;
    approveOrganizationCode: string;
}