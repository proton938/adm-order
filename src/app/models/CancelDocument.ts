class CancelDocument{
    cancelDocumentType: string;
    cancelDocumentValue: string;
    cancelDocumentDate: string;
    cancelOrganization: string;
    cancelOrganizationCode: string;
}