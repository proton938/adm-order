class AuthUser {
    login: string;
    authenticated: boolean;
    user: User;
}