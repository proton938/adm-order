class ApprovalType {
	approvalType: string;
	approvalTypeCode: string;
	approvalDuration: string;
	buttonNo: string;
	buttonNoColor: string;
	buttonYes: string;
	buttonYesColor: string;
    department: string;
    duration: any;
}