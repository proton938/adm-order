class ViewType {
    constructor(current: string, types: ViewValue[]) {
        this.current = current;
        this.types = types;
    }

    current: string;
    types: ViewValue[];
}

class ViewValue {
    code: string;
    name: string;
    cssClass: string;

    constructor(code: string, name: string, cssClass: string) {
        this.code = code;
        this.name = name;
        this.cssClass = cssClass;
    }
}