class User {
    accountName: string;
    fio: string;
    post: string;
    email: string;
    phoneNumber: string;
    iofShort: string;
    fioShort?: string;
    department?: string;
}