class RDType {
    DOC_CODE: string;
    DOC_NAME: string;
    DOC_ACCESS_ACTION?: string;
    DOC_ACCESS_EI?: string;
    APPLICATION?: boolean;
    PREREG?: boolean;
    DOC_SING_ELECTRON?: boolean | string;
    PUBLISHING: boolean;
    commentVisib: boolean;
    textVisib: boolean;
    approvalUmpl?: boolean;
}
