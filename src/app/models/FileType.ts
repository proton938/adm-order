class FileType {
    fileID: string;
    fileType: string;
    fileName: string;
    fileSize: string;
    fileSigned: boolean;
    fileDate: Date;
    mimeType: string;
}
