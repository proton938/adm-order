class LoadingStatus {
    static LOADING: string = "LOADING";
    static SUCCESS: string = "SUCCESS";
    static ERROR: string = "ERROR";
}
