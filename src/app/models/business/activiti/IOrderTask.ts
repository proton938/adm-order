interface IOrderTask extends oasiBpmRest.ITask {
	documentTypeValue: string;
	documentContent: string;
    delegatorFio: string;
    isDelegated: boolean;
    foreign: boolean;
    delegationRuleId: number;		
}