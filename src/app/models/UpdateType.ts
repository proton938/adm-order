class UpdateType{
    updatedDocumentID: string;
    updatedDocumentType: string;
    updatedDocumentTypeCode: string;
    updatedDocumentDate: Date;
    updatedDocumentNum: string;
}