class OrderStatus {
    static CANCELED: string = 'CANCELED';
    code: string;
    name: string;
    color: string;
}