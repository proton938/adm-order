class CancelType{
    canceledDocumentID: string;
    canceledDocumentNum: string;
    canceledDocumentDate: Date;
    canceledDocumentType: string;
    canceledDocumentTypeCode: string;
}