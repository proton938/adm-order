class Order {
    documentID: string;
    operationId?: string;
    bpmProcess: OrderProcess;
    documentTypeCode: string;
    documentTypeValue: string;
    documentYear: number;
    documentNumber: string;
    documentDate: Date;
    documentExpireDate: string;
    canceledDocument?: CancelType;
	updatedDocument?: UpdateType[];
    documentStatusCode: string;
    documentStatusValue: string;
    documentStatusColor: string;
    documentContent: string;
    address: string;
    approvedDocumentNumber: string;
    createDateTime: string;
    createAuthor: User;
    barcode: number;
    barcodeText?: string;
    folderID: string;
    files: FileType[];
    draftFilesRegInfo: FileType[];
    draftFiles: OrderDraftFile;
    draftFilesWithoutAtt: OrderDraftFile;
    attach: OrderDraftFile;
    head: any;
    executor: User[];
    registar: User | User[];
    mailList: User[];
    mailDateTime: string;
	previousDocument?: PreviousDocument;
    psoInputID: number;
    psoOutputID: number[];
    signingElectronically: boolean | string;
    approval: OrderApproval;
    approvalHistory: OrderApprovalHistory;
    SigningOnPaper: OrderSigningOnPaper[];
    documentThemeCode: string;
    documentThemeValue: string;
    additionalMaterials: FileType[];
    PublishingSite?: boolean | string;
    linkedDocument?: LinkedDocument[];
    isogdXmlGuid?: string;
    systemCode?: string;
    systemParentID?: string;
	activeApprover?: string;
}

class LinkedDocument {
	linkedDocumentID: string;
	linkedDocumentType: string;
}

class PreviousDocument {
	previousDocumentID: string;
	previousDocumentNum: string;
	previousDocumentDate: Date;
}

class OrderSigningOnPaper {
	taskId: string;
	signingResult: boolean;
	signingFactDate: Date;
	noteSigning: OrderNote;
	File: FileType[];
	constructor(taskId: string) {
		this.taskId = taskId;
	}	
}

class OrderApproval {
	approvalCycle: OrderApprovalList; 
	constructor(approvalCycleNum: number) {
		this.approvalCycle = new OrderApprovalList(approvalCycleNum);
	}
}

class OrderApprovalHistory {
	approvalCycle: OrderApprovalHistoryList[];
	constructor() {
		this.approvalCycle = [];
	}
}

class OrderApprovalList {
	approvalCycleNum: string;
	approvalCycleDate: Date;
	agreed: OrderApprovalListItem[];
	constructor(approvalCycleNum: number) {
		this.approvalCycleNum = '' + approvalCycleNum;
		this.approvalCycleDate = new Date();
		this.agreed = [];
	}
}

class OrderApprovalHistoryList extends OrderApprovalList {
	approvalCycleFile: FileType[];
}

class OrderApprovalListItem {
	approvalNum: string;
	approvalTime: string;
	agreedBy: OrderApprover;
	approvalPlanDate: Date;
	approvalFactDate: Date;
	approvalResult: string;
	approvalType: string;
	approvalTypeCode: string;
	approvalNote: string;
	fileApproval: FileType[];
	agreedDs: OrderApprovalListItemSignature;
	added: {
        additionalAgreed: {
            accountName: string;
            fio: string;
            iofShort: string;
		}
        addedNote: string;
	}[];
	constructor() {
	}
}

class OrderApprovalListItemSignature {
	agreedDsLastName: string;
	agreedDsName: string;
	agreedDsPosition: string;
	agreedDsCN: string;
	agreedDsFrom: Date;
	agreedDsTo: Date;
}

class OrderApprover {
	post: string;
	fioFull: string;
	accountName: string;
	fioShort: string;
	iofShort: string;
	phone: string;
}

class OrderDraftFile {
	draftFilesID: string;
	draftFilesType: string;
	draftFilesName: string;
	draftFilesSize: string;
	draftFilesSigned: boolean;
	draftFilesDate: Date;
	draftFilesMimeType: string;
}

class OrderNote {
	noteText: string;
	noteAuthor: string;
	noteAuthorFIO: string;
	noteDate: Date;
}

class OrderProcessType {
	date: Date;
	bpmProcessName: string;
	bpmProcessCode: string;
	note: OrderNote; 
}

class OrderProcess {
	bpmProcessId: string;
	bpmProcessDel: OrderProcessType;
	bpmProcessStart: OrderProcessType;
}
