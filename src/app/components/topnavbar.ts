class TopNavBarComponent {
    public bindings: any;
    public controller: any;
    public templateUrl: string;

    constructor() {
        this.controller = TopNavBarController;
        this.templateUrl = 'app/components/topnavbar.html';
        this.bindings = {};
    }
}

class TopNavBarController {
    static $inject = ['$rootScope', 'prSession', 'session', '$state', '$stateParams', 'authenticationService', 'userActionsStack', 'eventListener'];
    userLogin: string;
    query: string = "";
    post: string;

    constructor(private $rootScope: any,
                private prSession: ISessionStorage,
                private session: oasiSecurity.ISessionStorage,
                private $state: ng.ui.IStateService,
                private $stateParams: ng.ui.IStateParamsService,
                private authenticationService: IAuthenticationService,
                private userActionsStack: UserActionsStack, private eventListener: IEventListener) {
        this.userLogin = this.session.name();
        this.post = this.session.post();
    }

    $onInit() {
        this.query = this.$stateParams['query'];
        var listener = (event: ng.IAngularEvent, toState: ng.ui.IState, toParams: any, fromState: ng.ui.IState, fromParams: any) => {
            this.query = toParams.query;
        };
        this.$rootScope.$on('$stateChangeStart', listener);
    }

    public logout() {
        this.userActionsStack.register('logout');

        this.authenticationService.destroySession(() => {
            this.prSession.cleanSession();
            this.session.cleanSession();
            let currentLocation = window.location.pathname + window.location.search + window.location.hash;
            // # решетку нужно заменить иначе роутер обрубит остаток урла после
            window.location.href = '/?from=' + currentLocation.replace(/#/, '^');
        }, (params) => {
            console.error(params);
        });
    }

    public search() {
        this.$state.go('app.adm-order.orders', {
            query: this.query,
            filter: null
        }, { reload: true });
    }
}

angular.module('app').component('topnavbar', new TopNavBarComponent());
