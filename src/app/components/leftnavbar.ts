import IQService = angular.IQService;
class LeftNavBarComponent {
    public bindings: any;
    public controller: any;
    public templateUrl: string;

    constructor() {
        this.controller = LeftNavBarController;
        this.templateUrl = 'app/components/leftnavbar.html';
        this.bindings = {};
    }
}

class LeftNavBarController {
    static $inject = ['$state', 'orderResource', '$q', 'prSession', '$rootScope'];

	private myTasksCount: number;
    private notSentCount: number;
    private notScannedCount: number;

    constructor(private $state: any, private orderResource: IOrderResource, private $q: IQService, private session: SessionStorage, private $rootScope: IScope) {
        $rootScope.$on('updateCounters', this.updateCounters.bind(this));
        $rootScope.$on('updateMyTasksCount', (event, data) => {
        	this.myTasksCount = data.count;
        });
    }

    updateCounters() {
        let self: LeftNavBarController = this,
            counters = [];

        if (self.session.hasPermission(orders.Permissions.OASI_ADMORDER_VIEW_MAIL)) {
            counters.push(this.orderResource.notScannedCount().$promise);
        } else {
            counters.push(Promise.resolve(0));
        }

        if (self.session.hasPermission(orders.Permissions.OASI_ADMORDER_VIEW_SCAN)) {
            counters.push(this.orderResource.notSentCount().$promise);
        } else {
            counters.push(Promise.resolve(0));
        }

        this.$q.all(
            counters
        ).then((values: OrderCountResult[]) => {
            self.notScannedCount = values[0].foundTotal;
            self.notSentCount = values[1].foundTotal;
        });
    }

    $onInit() {
       this.updateCounters();
    }
}

angular.module('app').component('leftnavbar', new LeftNavBarComponent());
