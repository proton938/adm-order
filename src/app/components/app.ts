class AppComponent {
    public bindings: any;
    public controller: any;
    public templateUrl: string;

    constructor() {
        this.controller = AppController;
        this.templateUrl = 'app/components/app.html';
        this.bindings = {};
    }
}

class AppController {
   static $inject = ['$state', 'session', '$localStorage', 'toastr', 'sessionManager', '$rootScope', '$window'];
   isWhiteTop: boolean;


   changeToMapListener: any;
   changeFromMapListener: any;
   mapIsLoadingListener: any;
   isMap: boolean;
   /*isLoadingMap: boolean;*/

   MOBILE_SCREEN_WIDTH = 768;
   isMobile: boolean;

   constructor(private $state: any,
               private session: any,
               private $localStorage: any,
               private toastr: Toastr,
               private sessionManager: any,
               private $rootScope: ng.IRootScopeService,
               private $window: ng.IWindowService) {

      this.isWhiteTop = (this.$state.current.name === 'app.main' || this.$state.current.name === 'app.my-tasks');

      let vm = this;
      angular.element($window).bind('resize', () => vm.onResize());
      vm.onResize();
   }

   $onInit() {
      this.isMap = (this.$state.current.name === 'app.map');

      this.changeToMapListener = this.$rootScope.$on('goToMap', this.changeToMap.bind(this));
      this.changeFromMapListener = this.$rootScope.$on('goFromMap', this.changeFromMap.bind(this));
      /*this.mapIsLoadingListener = this.$rootScope.$on('mapIsLoading', this.mapIsLoading.bind(this));*/

      this.registerNavbarColorListener();
      this.sessionManager.start();
   }

   changeToMap() {
      this.isMap = true;
   }

   changeFromMap() {
      this.isMap = false;
     /* this.isLoadingMap = false;*/
   }

   onResize () {
     let vm = this;
     vm.isMobile = vm.$window.innerWidth < vm.MOBILE_SCREEN_WIDTH;
     this.$localStorage.isMobile = vm.isMobile;
   }
/*
   mapIsLoading() {
      this.isLoadingMap = true;
   }*/

   $onDestroy() {
      this.changeToMapListener();
      this.changeFromMapListener();
     /* this.mapIsLoadingListener();*/
   }


   showBreadcrumbs(): boolean {
      let r = true;
      if (r) r = this.$state.current.name !== 'app.map';
      if (r) r = this.$state.current.name !== 'app.main';
      if (r) r = this.$state.current.name !== 'app.my-tasks';
      if (r) r = !this.isMap;
      return r;
   }

   registerNavbarColorListener() {
      this.$rootScope.$on('$stateChangeSuccess', () => {
         this.isWhiteTop = (this.$state.current.name === 'app.main' || this.$state.current.name === 'app.my-tasks');
      });
   }
}

angular.module('app').component('app', new AppComponent());
