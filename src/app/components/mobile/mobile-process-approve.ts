import {DssSignHttpService} from 'oasi-cryptopro';

class MobileProcessApproveComponent {
    public bindings: any;
    public require: any;
    public controller: any;
    public templateUrl: string;

    constructor() {
        this.controller = MobileProcessApproveController;
        this.templateUrl = 'app/components/mobile/mobile-process-approve.html';
        this.bindings = {};
        this.require = {
            taskWrapper: '^taskWrapper'
        }
    }
}

class MobileProcessApproveController extends AbstractProcessController {
    static $inject = [
    	'$q',
    	'$filter',
    	'$scope',
    	'$timeout',
    	'$state',
    	'$stateParams',
    	'$uibModal',
		'$window',
    	'toastr',
    	'orderResource',
    	'nsiUserService',
        'nsiRestService',
    	'fileResource',
    	'orderTaskService',
    	'activityHttpService',
    	'activityRestService',
    	'alertService',
    	'redirectToTaskService',
		'cancelDocumentService',
    	'session',
    	'ApprovalType',
    	'dateFilter',
        'registerDocumentService',
        'signService',
        'dssSignHttpService'
	];

    private loadingStatus: LoadingStatus;
    private taskId: string;
    private taskInfo: ITaskInfo;
 	private order: Order;
 	private rdType: RDType;
 	private showDocumentText: boolean;

 	private loopCounter: number;
 	private agreed: OrderApprovalListItem[];
 	private lastApprovalListItem: boolean;
 	private approvalState: boolean;

 	private approvalNote: string;

 	private dropzone: Dropzone;

 	private sendingBackToWork: boolean;
 	private agreeing: boolean;
 	private approving: boolean;
	private addingApprovals: boolean = false;

	private openDraftSignDialog: () => void;
	private openSignDialog: () => void;
	private orderFile: oasiFileManager.ExFileType = null;
    private checkSign: boolean;
    private signingEDS: boolean;
    private signButtondisabled: boolean;

    constructor(
		public $q: ng.IQService,
		private $filter: any,
		private $scope: any,
		private $timeout: any,
        private $state: any,
        private $stateParams: any,
        private $uibModal: any,
		private $window: any,
        private toastr: Toastr,
        public orderResource: IOrderResource,
        public nsiUserService: INsiUserService,
        private nsiRestService: oasiNsiRest.NsiRestService,
        private fileResource: IFileResource,
        private orderTaskService: IOrderTaskService,
        public activityHttpService: IActivityHttpService,
        public activityRestService: oasiBpmRest.ActivityRestService,
        private alertService: oasiWidgets.IAlertService,
        private redirectToTaskService: IRedirectToTaskService,
        private cancelDocumentService: ICancelDocumentService,
        private sessionStorage: oasiSecurity.ISessionStorage,
        private ApprovalType: any,
        private dateFilter: any,
        private registerDocumentService: IRegisterDocumentService,
        private signService: SignService,
        private dssSignHttpService: DssSignHttpService
    ) {
		super($q, orderResource, nsiUserService, activityRestService);
    }

    $onInit() {
        this.loadingStatus = LoadingStatus.LOADING;
        this.signService.checkSert().then( res => {
            this.checkSign = res;
        });
        let id: string = this.$stateParams.documentID;
        this.taskId = this.$stateParams.taskId;
        this.getTaskInfo(this.$stateParams.documentID, 'NegotiationProjectOrder', this.$stateParams.taskId).then((taskInfo: ITaskInfo) => {
        	this.taskInfo = taskInfo;
        	this.order = taskInfo.document;
            if(this.order.operationId) {
                this.dssSignHttpService.getSignStatus(this.order.operationId).then( res => {
                    if (res.status === 'CONFIRMATION') {
                        this.toastr.success('Перейдите в приложение myDSS для подписания файла');
                        this.signButtondisabled = true;
                    } else if (res.status === 'NEW') {
                        this.toastr.success('Ожидайте прихода PUSH-уведомления');
                        this.signButtondisabled = true;
                    } else {
                        this.signButtondisabled = false;
                    }
                });
            }
        	const loopCounterVar = _.find(this.taskInfo.taskVariables, (taskVar: any) => {
        		return taskVar.name === 'loopCounter';
			});
        	if (!loopCounterVar) {
        		this.toastr.error('Не удалось определить номер согласующего в листе согласования.');
                this.loadingStatus = LoadingStatus.ERROR;
                return;
			}
            this.loopCounter = loopCounterVar.value;
        	this.agreed = this.order.approval.approvalCycle.agreed;
            this.approvalState = this.agreed[this.loopCounter].approvalTypeCode === this.ApprovalType.approval;
            this.lastApprovalListItem = this.loopCounter === this.order.approval.approvalCycle.agreed.length - 1;

            this.orderTaskService.getApprovalDict().then((response) => {
                let approval = _.filter(response, at => {
                    return at.approvalTypeCode === this.agreed[this.loopCounter].approvalTypeCode ;
                });
                this.signingEDS = approval[0].signingEDS;
            });

            if (!this.order.activeApprover) {
                this.order.activeApprover = this.sessionStorage.fioShort();
            }

        	if (!this.order.documentDate)
        	    this.order.documentDate = new Date();

        	let login = this.sessionStorage.login();

            if (!this.agreed[this.loopCounter].approvalPlanDate) {
				this.agreed[this.loopCounter].approvalPlanDate = this.taskInfo.task.dueDate;
				return this.updateDocument(this.order).then(() => {
                    return this.loadDictionaries();
				});
            } else {
                return this.loadDictionaries();
			}
        }).then(() => {
        	this.loadingStatus = LoadingStatus.SUCCESS;
            this.initDropzone();
        }, () => {
        	this.loadingStatus = LoadingStatus.ERROR;
        })
    }

    loadDictionaries(): ng.IPromise<any> {
        return this.$q.all([
            this.nsiRestService.get('RD_TYPES'),
        ]).then((values: any[]) => {
            this.rdType = values[0].find(val => val.DOC_CODE  === this.order.documentTypeCode);
            this.showDocumentText = this.rdType.DOC_ACCESS_ACTION === 'OASI_ADMORDER_OGS';
        });
    }

    initDropzone() {
        this.$timeout(() => {
        	let self = this;
            let options: any = {
                autoProcessQueue: true,
                withCredentials: true,
                parallelUploads: 1,
                paramName: "file",
                url: this.fileResource.getApprovalUploadUrl(this.order.documentID, this.taskId),
                init: function() {
                    this.on("error", function(file, errorMessage) {
                        self.toastr.warning(errorMessage.message ? errorMessage.message : errorMessage, 'Ошибка');
                        this.removeFile(file);
                    });
                    this.on("success", function(file, result) {
                    	self.$scope.$apply(() => {
	                    	self.agreed[self.loopCounter].fileApproval = self.agreed[self.loopCounter].fileApproval || [];
	                    	self.agreed[self.loopCounter].fileApproval.push(result);
                    	});
                    	this.removeFile(file);
                    });
                }
            };

            this.dropzone = new Dropzone("#file-dropzone", options);
        });
    }

    sendBackToWork() {
    	if (!this.approvalNote) {
    		this.toastr.error('Не указан комментарий');
    		return;
    	}
    	this.sendingBackToWork = true;

    	this.approve(this.order.documentID, this.taskId, false, {
    		approvalNote: this.approvalNote
    	}).then((approvalListItem) => {
            this.agreed[this.loopCounter] = approvalListItem;
    		return this.orderTaskService.updateProcess(this.taskId, [{
	            "name": "ApprovalApprovedVar",
	            "value": false
	        }]);
    	}).then(() => {
    		this.sendingBackToWork = false;
    		this.toastr.success('Документ отправлен на доработку.');
			this.$window.location.href = this.$window.location.protocol + '//' + this.$window.location.host + '/oasi/#/app/my-tasks';
    	}, () => {
    		this.sendingBackToWork = false;
    	})
    }

    agreeAndSendForwardHandler() {
        this.agreeing = true;
        if (this.lastApprovalListItem) {
            this.registerDocumentService.checkLinkedDocuments(this.order).then(() => {
            	this.agreeAndSendForward();
            }).catch(err => {
                this.agreeing = false;
                if (err.documentID && err.documentTypeValue) {
                    this.redirectToTaskService.redirectToTask({ linked: {documentID: err.documentID, documentTypeValue: err.documentTypeValue}});
                }
			})
        }
        else {
            this.agreeAndSendForward();
		}
	}

    agreeAndSendForward() {
		this.agreeing = true;
		if (this.agreed[this.loopCounter].approvalTypeCode === this.ApprovalType.assent) {
			this.approve(this.order.documentID, this.taskId, true, {
	    		approvalNote: this.approvalNote
	    	}).then((approvalListItem) => {
	    	    this.agreed[this.loopCounter] = approvalListItem;
                if (this.lastApprovalListItem) {
                    this.order.activeApprover = null;
                    return this.updateDocument(this.order).then(() => {
                        return this.registerOrder();
                    });
                } else {
                    let currentApproverNum = this.agreed[this.loopCounter].approvalNum;
                    let nextApprover = _.find(this.order.approval.approvalCycle.agreed, (approver) => approver.approvalNum == (+currentApproverNum + 1));
                    this.order.activeApprover = nextApprover.agreedBy.fioShort;
                    return this.updateDocument(this.order);
                }
            }).then(() => {
	    		return this.orderTaskService.updateProcess(this.taskId, [{
		            "name": "ApprovalApprovedVar",
		            "value": true
		        }]);
	        }).then((result: any) => {
	    		this.agreeing = false;
	    		this.toastr.success('Документ успешно согласован.');
				this.$window.location.href = this.$window.location.protocol + '//' + this.$window.location.host + '/oasi/#/app/my-tasks';
	    	}, () => {
	    		this.agreeing = false;
	    	});
		} else {
			this.openDraftSignDialog();
    	}
    }

    afterDraftSign(result: any) {
    	if (result.error) {
            this.agreeing = false;
            return;
        }
        let certEx: oasiCryptopro.ICertificateInfoEx = result.result.certEx;
    	this.order.draftFiles.draftFilesSigned = true;
        this.order.activeApprover = null;
        if (this.lastApprovalListItem) {
            this.order.activeApprover = null;
        }
        else {
            let currentApproverNum = this.agreed[this.loopCounter].approvalNum;
            let nextApprover = _.find(this.order.approval.approvalCycle.agreed, (approver) => approver.approvalNum == (+currentApproverNum + 1));
            this.order.activeApprover = nextApprover.agreedBy.fioShort;
        }
    	this.updateDocument(this.order).then(() => {
            return this.approve(this.order.documentID, this.taskId,true, {
                approvalNote: this.approvalNote,
                agreedDs: {
                    agreedDsLastName: certEx.lastName,
                    agreedDsName: certEx.firstName,
                    agreedDsPosition: certEx.position,
                    agreedDsCN: certEx.serialNumber,
                    agreedDsFrom: moment(certEx.from, 'DD.MM.YYYY HH:mm:ss').toDate(),
                    agreedDsTo: moment(certEx.till, 'DD.MM.YYYY HH:mm:ss').toDate()
                }
            })
		}).then((approvalListItem) => {
		    this.agreed[this.loopCounter] = approvalListItem;
            if (this.lastApprovalListItem) {
                return this.registerOrder();
            } else {
                return true;
            }
        }).then(() => {
            return this.orderTaskService.updateProcess(this.taskId, [{
                "name": "ApprovalApprovedVar",
                "value": true
            }]);
        }).then((result: any) => {
            this.agreeing = false;
            this.toastr.success('Документ успешно согласован.');
            this.$window.location.href = this.$window.location.protocol + '//' + this.$window.location.host + '/oasi/#/app/my-tasks';
        }, () => {
            this.agreeing = false;
        });
	}

    private afterDraftSignBound = this.afterDraftSign.bind(this);

    registerOrder(): ng.IPromise<any> {
    	const deferred = this.$q.defer();
    	if (!this.order.documentNumber) {
            this.orderResource.register({id: this.order.documentID}).$promise.then((file) => {
    			this.order.files = [file];
    			deferred.resolve();
			}).catch(() => {
                deferred.reject();
			})
		} else {
    		deferred.resolve();
		}
		return deferred.promise;
	}

	beforeSign(fileInfo, folderGuid, certEx) {
		return this.prepareSign(this.order.documentID, {
            dsLastName: certEx.lastName,
            dsName: certEx.firstName,
            dsPosition: certEx.position,
            dsCN: certEx.serialNumber,
            dsFrom: moment(certEx.from, 'DD.MM.YYYY HH:mm:ss').toDate(),
            dsTo: moment(certEx.till, 'DD.MM.YYYY HH:mm:ss').toDate()
        }).then((file: FileType) => {
        	this.orderFile = {
                idFile: file.fileID,
        		nameFile: file.fileName,
				sizeFile: parseInt(file.fileSize),
				dateFile: file.fileDate.getTime(),
				mimeType: file.mimeType,
                typeFile: file.fileType,
                signed: file.fileSigned
			};
			return this.loadDocument(this.order.documentID).then((order) => {
                this.order = order;
            }).then(() => {
                return this.$q.resolve(this.orderFile);
            });
        });
	}

    private beforeSignBound = this.beforeSign.bind(this);
    private saveSignOperationIds = this.saveSignOperationId.bind(this);

    saveSignOperationId(id: string) {
        this.order.operationId = id;
        return this.updateDocument(this.order).then(res => {
            return this.$q.resolve(res);
        })
    }

	afterSign(result: any) {
        if (result.error) {
            this.approving = false;
            return;
        }
        let canceledDocument = this.order.canceledDocument;
        let certEx: oasiCryptopro.ICertificateInfoEx = result.result.certEx;
        this.orderFile.signed = true;
        _.find(this.order.files, f => f.fileID === this.orderFile.idFile).fileSigned = true;
        if (this.lastApprovalListItem) {
            this.order.activeApprover = null;
        }
        else {
            let currentApproverNum = this.agreed[this.loopCounter].approvalNum;
            let nextApprover = _.find(this.order.approval.approvalCycle.agreed, (approver) => approver.approvalNum == (+currentApproverNum + 1));
            this.order.activeApprover = nextApprover.agreedBy.fioShort;
        }
        this.updateDocument(this.order).then(() => {
            return this.approve(this.order.documentID, this.taskId,true, {
                approvalNote: this.approvalNote,
                agreedDs: {
                    agreedDsLastName: certEx.lastName,
                    agreedDsName: certEx.firstName,
                    agreedDsPosition: certEx.position,
                    agreedDsCN: certEx.serialNumber,
                    agreedDsFrom: new Date(certEx.from),
                    agreedDsTo: new Date(certEx.till)
                }
            })
        }).then((approvalListItem) => {
            this.agreed[this.loopCounter] = approvalListItem;
            return this.cancelDocumentService.cancelDocument(canceledDocument, this.order.documentDate);
        }).then(() => {
            return this.orderTaskService.updateProcess(this.taskId, [{
                "name": "ApprovalApprovedVar",
                "value": true
            }]);
        }).then(() => {
            this.approving = false;
            this.toastr.success('Электронный документ успешно создан и подписан');
            this.$window.location.href = this.$window.location.protocol + '//' + this.$window.location.host + '/oasi/#/app/my-tasks';
        }, (err) => {
            if (err.documentID && err.documentTypeValue) {
                this.redirectToTaskService.redirectToTask({ linked: {documentID: err.documentID, documentTypeValue: err.documentTypeValue}});
            }
            this.approving = false;
        });
    }

    private afterSignBound = this.afterSign.bind(this);

    approveAndSendForward() {

    	let canceledDocument = this.order.canceledDocument;

        this.registerDocumentService.checkLinkedDocuments(this.order).then(() => {
            return this.cancelDocumentService.checkCanceledDocument(canceledDocument);
        })
		.then(() => {
            this.approving = true;
            this.openSignDialog();
        });
    }

    deleteApprovalFile(fileInfo: any) {
        let fileID = fileInfo.idFile;
    	this.fileResource.deleteApprovalFile({
    		id: this.order.documentID,
			taskId: this.taskId,
    		fileGuid: fileID
    	}, {}, () => {
            _.remove(this.agreed[this.loopCounter].fileApproval, f => {
                return f.fileID === fileID;
            });
        });
    }

    addApproval() {
		//this.addingApprovals = true;
        let modalInstance = this.$uibModal.open({
			component: 'add-approval-modal',
            size: "lg",
            keyboard: false,
            resolve: {
			    systemCode: () => {
                    return this.order.systemCode;
                },
                documentTypeCode: () => {
					return this.order.documentTypeCode;
				},
				dueDate: () => {
                	return this.taskInfo.task.dueDate;
				},
				except: () => {
					return []; /*_.map(this.order.approval.approvalCycle.agreed, a => {
						return a.agreedBy.accountName;
					});*/
				}
            }
        });
        modalInstance.result.then((agreed: AddedOrderApprovalListItem[]) => {
			this.addingApprovals = true;
        	this.orderResource.addApprovers({id: this.order.documentID, taskId: this.taskId}, agreed).$promise.then((agreed: OrderApprovalListItem[]) => {
                this.order.approval.approvalCycle.agreed = agreed;
        		return this.activityHttpService.addApprovers(this.taskId, agreed);
			}).then(() => {
				this.$window.location.href = this.$window.location.protocol + '//' + this.$window.location.host + '/oasi/#/app/my-tasks';
			});
        });
	}

}

angular.module('app').component('mobileProcessApprove', new MobileProcessApproveComponent());
