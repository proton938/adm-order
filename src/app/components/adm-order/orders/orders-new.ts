class OrdersNewComponent implements ng.IComponentOptions {
    public bindings: any;
    public controller: any;
    public templateUrl: string;

    constructor() {
        this.controller = OrdersNewController;
        this.templateUrl = 'app/components/adm-order/orders/orders-new.html';
        this.bindings = {
            close: '&',
            dismiss: '&'        
        };
    }
}

class OrdersNewController implements ng.ui.bootstrap.IModalServiceInstance {
    static $inject = [
    	'$q',
    	'$state',
    	'session',
    	'orderResource',
    	'orderTaskService',
    	'activityRestService',
    	'nsiRestService'
    ];
    
    public close: (result?: any) => {};
    public dismiss: (reason?: any) => {};
    public result: ng.IPromise<any>;
    public opened: ng.IPromise<any>;
    public rendered: ng.IPromise<any>;
    public closed: ng.IPromise<any>;

    private rdTypes: RDType[];
    private availableYears: number[];
    private themes: RDTheme[];
    private availableThemes: RDTheme[] = [];
    
    private saving: boolean = false;
    
    private order: Order;

    constructor(
    	private $q: ng.IQService,
    	private $state: any,
    	private session: oasiSecurity.ISessionStorage,
    	private orderResource: IOrderResource,    	
    	private orderTaskService: IOrderTaskService,
    	private activityRestService: oasiBpmRest.ActivityRestService,
        private nsiRestService: oasiNsiRest.NsiRestService
	) {
    }

    $onInit() { 
    	this.order = new Order();
    	this.order.documentYear = new Date().getFullYear();
    	this.loadDictionaries();
    }
    
    loadDictionaries() {
    	this.availableYears = _.range(2016, 2028);
        this.$q.all([
            this.nsiRestService.get('RD_TYPES'),
            this.nsiRestService.get('RD_THEME')
        ]).then((values: any[]) => {
            this.rdTypes = values[0];
            this.themes = values[1];
            this.documentTypeChanged();
        });
    }

    documentTypeChanged() {
        if (this.order.documentTypeCode) {
            this.availableThemes = _.filter(this.themes, th => {
                return th.DOC_CODE === this.order.documentTypeCode;
            })
        } else {
            this.availableThemes = [];
        }
    }

    themeChanged() {
        if (this.order.documentThemeCode) {
            this.order.documentThemeValue = _.find(this.availableThemes, th => {
                return th.THEME_CODE === this.order.documentThemeCode;
            }).THEME_NAME;
        } else {
            delete this.order.documentThemeValue;
        }
    }
    
    save() {
    	this.saving = true;
    	this.orderResource.create({document: this.order}, (data: OrderCreateResult) => {
    		this.orderResource.getById({id: data.id}, (order: Order) => {
	        	this.orderTaskService.getProcessDefinition('rd_coordinationOrders_ID').then((process: oasiBpmRest.IProcessDefinition) => {
					return this.orderTaskService.initProcess(process.id, this.getInitProcessProperties(order));            		
	        	}).then((id) => {
	        		order.bpmProcess = new OrderProcess();
	        		order.bpmProcess.bpmProcessId = id.toString();
			        return this.updateOrder(order).then(() => {
			        	return this.getTaskId(id);
			        });	        		
	        	}).then((taskId) => {
	        		this.saving = false;
	        		this.$state.go('app.adm-order.process.PrepareRD', {
	        			documentID: order.documentID,
	        			taskId: taskId
	        		});
	        		this.close();
	        	});    		
    		});
    	});
    } 
    
    updateOrder(order: Order) {
    	let deferred = this.$q.defer();
		this.orderResource.update({id: order.documentID}, {
            document: order
        }, () => {
        	deferred.resolve();
        }, () => {
        	deferred.reject();
        });
    	return deferred.promise;    
    }
    
    getTaskId(processId: any) {
    	let deferred = this.$q.defer();
		this.activityRestService.getTasks({
			processInstanceId: processId
		}).then((result: any) => {
			deferred.resolve(result.data[0].id)
		}, () => {
        	deferred.reject();
        });
    	return deferred.promise;
    } 
    
    getInitProcessProperties(order: Order): oasiBpmRest.ITaskVariable[] {
        const rdType = _.find(this.rdTypes, _ => {
            return _.DOC_CODE === order.documentTypeCode;
        });
    	return [{
            "name": "EntityIdVar",
            "value": order.documentID
        },{
            "name": "EntityDescriptionVar",
            "value": order.documentTypeValue + " " + order.documentContent
        }, {
            "name": "ContractorVar",
            "value": this.session.login()
        }, {
            "name": "reg_group_var",
            "value": rdType.DOC_ACCESS_ACTION
        }, {
            "name": "scan_group_var",
            "value": rdType.DOC_ACCESS_ACTION
        }];
    }
    
    cancel() {
        this.dismiss();
    }    

}

angular.module('app').component('ordersNew', new OrdersNewComponent());
