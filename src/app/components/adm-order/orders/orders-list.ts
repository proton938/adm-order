import Sorting = orders.Sorting;

class OrdersListComponent {
    public bindings: any;
    public require: any;
    public controller: any;
    public templateUrl: string;

    constructor() {
        this.controller = OrdersListController;
        this.templateUrl = 'app/components/adm-order/orders/orders-list.html';
        this.bindings = {};
        this.require = {
            parent: '^orders'
        }
    }
}

class OrdersListController {
    static $inject = [
        'orderResource', 'orderByFilter', '$q', 'fileResource',
        'prAuthorizationService', 'toastr'];

    private sorting: Sorting;
    private sortingAsc: boolean;

    private loadingAppStatus: LoadingStatus;
    private searchResult: OrderSearchResult;

    private filter: orders.ExtSearchFilter | string;

    private parent: OrdersController;

    private hasViewPermission: boolean;

    constructor(private orderResource: IOrderResource,
                private orderBy: ng.IFilterOrderBy,
                private $q: ng.IQService,
                private fileResource: IFileResource,
                private authorizationService: IAuthorizationService,
                private toastr: Toastr) {
    }

    reload(filter: orders.ExtSearchFilter | string, reset?: boolean) {
        let self: OrdersListController = this;
        this.loadingAppStatus = LoadingStatus.LOADING;

        if (reset) {
            this.reset();
        }

        if (filter) {
            this.filter = filter;
        }

        let params: any = {
            sort: this.sorting + ',' + (this.sortingAsc ? 'asc' : 'desc'),
            page: this.searchResult.pageNumber - 1,
            size: this.searchResult.pageSize
        };

        let onSuccess = (response: OrderSearchResult) => {
            let sortedOrders: Order[] = response.content;

            sortedOrders.forEach((order: any) => {
                if (order.files && order.files.length > 1) {
                    let urls = '';

                    order.files.forEach((file) => {
                        urls += '<a href="#" class="text-default" title="{{file.fileName}}">' + file.fileName + '</a><br>';
                    });
                    order.fileUrls = urls;
                }
            });

            response.pageNumber++;

            self.searchResult = response;
            self.loadingAppStatus = LoadingStatus.SUCCESS;

        };

        let onError = () => {
            self.loadingAppStatus = LoadingStatus.ERROR;
        };

        if (_.isString(this.filter)) {
            _.extend(params, {query: this.filter});
            this.orderResource.searchQuery(params, onSuccess, onError);
        } else {
            this.orderResource.search(params, this.filter, onSuccess, onError);
        }
    }

    $onInit() {
        this.reset();

        this.hasViewPermission = this.authorizationService.check('OASI_ADMORDER_CARD');

        this.parent.setListController(this);
    }

    reset() {
        this.sorting = Sorting.DOCUMENT_DATE;
        this.sortingAsc = false;
        this.searchResult = {
            $promise: null,
            content: [],
            totalElements: 0,
            totalPages: 0,
            pageNumber: 1,
            pageSize: 10
        };
    }

    isSorted(field: Sorting, currentSorting: Sorting): boolean {
        return field == currentSorting;
    }

    switchSorting(sorting: Sorting) {
        if (this.sorting === sorting) {
            this.sortingAsc = !this.sortingAsc;
        } else {
            this.sorting = sorting;
        }

        this.reload(this.filter);
    }

    getFileLink(id) {
        return this.fileResource.getDownloadUrl(id);
    }

    exportToExcel() {
        let self: OrdersListController = this;
        let deferred: ng.IDeferred<any> = this.$q.defer();

        let params: any = {
            sort: this.sorting + ',' + (this.sortingAsc ? 'asc' : 'desc'),
            page: this.searchResult.pageNumber - 1,
            size: this.searchResult.pageSize
        };

        let onSuccess = (data, headers) => {
            headers = headers();

            const filename ='excel_export.xlsx';
            const contentType = headers['content-type'];

            const linkElement = document.createElement('a');
            try {
                const blob = new Blob([data], { type: contentType+";charset=utf-8" });
                const url = window.URL.createObjectURL(blob);

                linkElement.setAttribute('href', url);
                linkElement.setAttribute("download", filename);

                const clickEvent = new MouseEvent("click", {
                    "view": window,
                    "bubbles": true,
                    "cancelable": false
                });
                linkElement.dispatchEvent(clickEvent);

                deferred.resolve(true);
            } catch (ex) {
                console.log(ex);
            }
        };

        let onError = () => {
            self.toastr.error('Во время экспорта произошла ошибка. Повторите попытку.');
        };

        if (_.isString(this.filter)) {
            _.extend(params, {query: this.filter});
            this.orderResource
                .exportExcelQuery(params, onSuccess, onError);
        } else {
            this.orderResource.exportExcel(params, this.filter, onSuccess, onError);
        }

        return deferred.promise;
    }
}

angular.module('app').component('ordersList', new OrdersListComponent());
