class OrdersComponent {
    public bindings: any;
    public controller: any;
    public templateUrl: string;

    constructor() {
        this.controller = OrdersController;
        this.templateUrl = 'app/components/adm-order/orders/orders.html';
        this.bindings = {};
    }
}

class OrdersController {
    static $inject = [
        '$state',
        '$stateParams',
        'nsiRestService',
        '$uibModal',
        'session'
    ];

    private initialized: boolean = false;

    private viewExtSearch: boolean;

    private query: string;
    private extSearchFilter: orders.ExtSearchFilter;
    private filterController: IOrdersFilterController;
    private listController: IChildOrdersController;
    private isExporting: boolean = false;

    constructor(private $state: any,
                private $stateParams: any,
                private nsiRestService: oasiNsiRest.NsiRestService,
                private $uibModal: any,
                private sessionStorage: oasiSecurity.ISessionStorage) {
    }

    $onInit() {
        this.query = this.$stateParams.query;
        this.viewExtSearch = false;
        if (this.$stateParams.filter) {
            this.extSearchFilter = this.$stateParams.filter;
            this.initialized = true;
        } else {
            this.nsiRestService.get('RD_STATUS').then((statuses: RDStatus[]) => {
                let currentYear = (new Date()).getFullYear();
                this.extSearchFilter = new orders.ExtSearchFilter();
                this.extSearchFilter.docYear = [currentYear, currentYear-1];
                this.extSearchFilter.docStatusCode = _.chain(statuses).filter((status: RDStatus) => {
                    return status.statusAnalytic === 'Действующий';
                }).map((status: RDStatus) => {
                    return status.STATUS_CODE;
                }).value();
                this.initialized = true;
            });
        }
    }

    toggleViewExtSearch() {
        this.viewExtSearch = !this.viewExtSearch;
    }

    setFilterController(filterController: IOrdersFilterController) {
        this.filterController = filterController;
        this.filterController.setFilter(this.extSearchFilter);
    }

    setListController(listController: IChildOrdersController) {
        this.listController = listController;

        this.listController.reload(this.query ? this.query : this.extSearchFilter, true);
    }

    setFilter(filter: orders.ExtSearchFilter) {
        _.extend(this.extSearchFilter, filter);

        if (this.query) {
            this.$state.go('app.adm-order.orders', {filter: this.extSearchFilter, query: null});
        } else {
            this.filterController.setFilter(this.extSearchFilter);
            this.listController.reload(this.extSearchFilter, true);
        }
    }

    clearFilter() {
        _.each(_.keys(this.extSearchFilter), (key) => {
            delete this.extSearchFilter[key];
        });

        if (this.query) {
            this.$state.go('app.adm-order.orders', {filter: this.extSearchFilter, query: null});
        } else {
            this.filterController.setFilter(this.extSearchFilter);
            this.listController.reload(this.extSearchFilter, true);
        }
    }

    exportToExcel() {
        this.isExporting = true;
        this.listController
            .exportToExcel()
            .then(() => {
                this.isExporting = false;
            });
    }

    showAddOrderForm() {
        this.$state.go('app.adm-order.initiate');
    }

    showCreateBpButton() {
        const groups = this.sessionStorage.groups();
        return ((groups.indexOf('OASI_ADMORDER_EDIT') !== -1) &&
                (groups.indexOf('OASI_ADMORDER_KAN') !== -1 ||
                 groups.indexOf('OASI_ADMORDER_OGS') !== -1));
    }

}

interface IOrdersFilterController {
    setFilter(extSearchFilter: orders.ExtSearchFilter): void;
}

interface IChildOrdersController {
    reload(extSearchFilter: orders.ExtSearchFilter | string, reset: boolean): void;

    exportToExcel(): Promise<boolean>;
}

angular.module('app').component('orders', new OrdersComponent());
