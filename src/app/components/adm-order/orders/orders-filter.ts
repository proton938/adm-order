class OrdersFilterComponent implements ng.IComponentOptions {
    public bindings: any;
    public require: any;
    public controller: any;
    public templateUrl: string;

    constructor() {
        this.controller = OrdersFilterController;
        this.templateUrl = 'app/components/adm-order/orders/orders-filter.html';
        this.bindings = {
            applyFilter: '&',
            clearFilter: '&'
        };
        this.require = {
            parent: '^orders'
        };
    }
}

class OrdersFilterController implements IOrdersFilterController {
    static $inject = ['$q', 'nsiResource', 'nsiRestService'];

    private applyFilter: (value: any) => {};
    private clearFilter: () => {};
    private searchFilter: orders.ExtSearchFilter;

    private parent: OrdersController;

    private loadingDictStatus: LoadingStatus = false;

    rdTypes: RDType[];
    rdStatuses: RDStatus[];
    users: oasiNsiRest.UserBean[];
    rdStates: any[];
    selectedState: any;
    departments: Organization[];
    updatingUsersListInProgress: boolean;
    responsibleDepartment: Organization;
    availableYears: any;

    constructor(private $q: ng.IQService,
                private nsiResource: INsiResource,
                private nsiRestService: oasiNsiRest.NsiRestService) {
    }

    $onInit() {
        let currentYear = (new Date()).getFullYear();
        this.availableYears = _.range(currentYear, 2003);
        this.loadDictionaries();
        console.log(this);
    }

    loadDictionaries() {
        let self: OrdersFilterController = this;

        this.loadingDictStatus = LoadingStatus.LOADING;

        this.$q.all([
            this.nsiRestService.get('RD_TYPES'),
            this.nsiRestService.get('RD_STATUS'),
            this.nsiResource.departments().$promise
        ]).then(function (values: any[]) {
            self.loadingDictStatus = LoadingStatus.SUCCESS;

            self.rdTypes = values[0];
            self.rdStatuses = values[1];
            self.rdStates = self.rdStatuses.map(item => item.statusAnalytic)
                .filter((value, index, self) => self.indexOf(value) === index);
            self.departments = values[2];

            self.parent.setFilterController(self);
        });
    }

    statusChangeHandler() {
        this.selectedState = null;
    }

    stateChangeHandler() {
        this.searchFilter.docStatusCode = (this.rdStatuses.filter(status => status.statusAnalytic === this.selectedState) || [])
            .map(status => status.STATUS_CODE);
    }

    setFilter(extSearchFilter:orders.ExtSearchFilter) {
        this.searchFilter = angular.copy(extSearchFilter);
    }

    apply() {
        this.applyFilter({
            value: this.searchFilter
        });
    }

    cancel() {
        this.selectedState = null;
        this.clearFilter();
    }

    searchExecutors(query) {
        const params: oasiNsiRest.ISerachUsersParams = {
            fio: query
        };
        if (this.responsibleDepartment) {
            params.department = [this.responsibleDepartment.description];
        }
        this.updatingUsersListInProgress = true;
        this.nsiRestService.searchUsers(params).then((users: oasiNsiRest.UserBean[]) => {
            this.users = users;
            this.updatingUsersListInProgress = false;
        })
    }

    departmentsChange(dep) {
        this.updatingUsersListInProgress = true;
        this.nsiRestService.ldapUsers(null, dep.description).then((users: oasiNsiRest.UserBean[]) => {
            this.searchFilter.responsibleExecutor = '';
            this.searchFilter.responsibleExecutors = _.map(users, (el) => el.accountName);
            this.updatingUsersListInProgress = false;
        });
    }

    executorsChange(executor) {
        this.responsibleDepartment = {
            name: executor.departmentFullName,
            description: executor.departmentCode
        };
    }
}

angular.module('app').component('ordersFilter', new OrdersFilterComponent());
