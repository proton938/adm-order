class ProcessEditDocComponent {
    public bindings: any;
    public require: any;
    public controller: any;
    public templateUrl: string;

    constructor() {
        this.controller = ProcessEditDocController;
        this.templateUrl = 'app/components/adm-order/order/process/process-editdoc.html';
        this.bindings = {};
        this.require = {
            taskWrapper: '^taskWrapper'
        }
    }
}

class ProcessEditDocController extends AbstractProcessController {
    static $inject = [
        '$q',
        '$state',
        '$stateParams',
        '$window',
        'toastr',
        'orderResource',
        'orderTaskService',
        'activityRestService',
        'nsiRestService',
        'fileResource',
        '$rootScope',
        'ApprovalType',
        'nsiUserService'
    ];

    private loadingStatus: LoadingStatus;
    private taskId: string;
    private taskInfo: ITaskInfo;
    private order: Order;
    private rdType: RDType;

    private rdTypes: RDType[];

    private dzDraftFilesWithoutAttOptions: any;
    private dzDraftFilesWithoutAttCallbacks: any;
    private dzDraftFilesWithoutAttMethods: any;

    private dzDraftFilesAttOptions: any;
    private dzDraftFilesAttCallbacks: any;
    private dzDraftFilesAttMethods: any;;

    private declineInfo: any;

    private sendingToApproval: boolean;
    private sendingToRegistration: boolean;
    private creatingDraftFile: boolean;
    private creatingDraftFilesWithoutAtt: boolean;
    private combiningDraftFile: boolean;


    constructor(public $q: ng.IQService,
                private $state: any,
                private $stateParams: any,
                private $window: any,
                private toastr: Toastr,
                public orderResource: IOrderResource,
                private orderTaskService: IOrderTaskService,
                public activityRestService: oasiBpmRest.ActivityRestService,
                private nsiRestService: oasiNsiRest.NsiRestService,
                private fileResource: IFileResource,
                private $rootScope: IScope,
                private ApprovalType: any,
                public nsiUserService: INsiUserService) {
        super($q, orderResource, nsiUserService, activityRestService);
    }

    $onInit() {
        this.loadingStatus = LoadingStatus.LOADING;
        this.taskId = this.$stateParams.taskId;
        this.getTaskInfo(this.$stateParams.documentID, 'DocEdit', this.$stateParams.taskId).then((taskInfo: ITaskInfo) => {
            this.taskInfo = taskInfo;
            this.order = taskInfo.document;

            const lastCycle = this.order.approvalHistory ? _.last(this.order.approvalHistory.approvalCycle) : null;
            this.declineInfo = this.getDeclineInfo(this.order, lastCycle);

            if (lastCycle) {
                return this.initNewCycle(lastCycle);
            } else {
                return this.$q.resolve();
            }
        }).then((nextApprovalCycle) => {
            if (nextApprovalCycle) {
                this.order.approval = {
                    approvalCycle: nextApprovalCycle
                };
            } else {
                this.order.approval = new OrderApproval(this.order.approvalHistory.approvalCycle.length + 1)
            }
            this.initDropzone();
            return this.loadDictionaries();
        }).then(() => {
            this.rdType = _.find(this.rdTypes, _ => {
                return _.DOC_CODE === this.order.documentTypeCode;
            })
        }).then(() => {
            this.loadingStatus = LoadingStatus.SUCCESS;
        }, () => {
            this.loadingStatus = LoadingStatus.ERROR;
        })
    }

    private initNewCycle(lastCycle: OrderApprovalHistoryList): ng.IPromise<any> {
        return this.$q.all([
            this.nsiRestService.get('RDApprovalDuration'),
            ...lastCycle.agreed.map((agreed) => {
                return this.nsiUserService.user(agreed.agreedBy.accountName);
            })
        ]).then((result) => {
            let approvalTypes: ApprovalType[] = result[0];
            let users: User[] = result.splice(1);
            let usersMap = _.reduce(users, (map, user) => {
                map[user.accountName] = user;
                return map;
            }, {});
            const nextApprovalCycle = angular.copy(lastCycle);
            nextApprovalCycle.approvalCycleNum = '' + (this.order.approvalHistory.approvalCycle.length + 1);
            nextApprovalCycle.agreed = _.map(lastCycle.agreed, agreed => {
                let result = new OrderApprovalListItem();
                let user = usersMap[agreed.agreedBy.accountName];
                let approvalType = _.find(approvalTypes, type => type.approvalTypeCode === agreed.approvalTypeCode && type.department === user.department);
                if (!approvalType) {
                    approvalType = _.find(approvalTypes, type => type.approvalTypeCode === agreed.approvalTypeCode && !type.department);
                }
                let approvalTime = approvalType ? approvalType.duration : null
                _.extend(result, {
                    approvalNum: agreed.approvalNum,
                    agreedBy: agreed.agreedBy,
                    approvalType: agreed.approvalType,
                    approvalTypeCode: agreed.approvalTypeCode,
                    approvalTime: approvalTime
                });
                return result;
            });
            return nextApprovalCycle;
        });
    }

    private getDeclineInfo(order: Order, lastCycle: OrderApprovalHistoryList) {
        const notAgreedOrApproved = lastCycle ? _.find(lastCycle.agreed, (agreedItem: OrderApprovalListItem) => {
            return agreedItem.approvalResult === 'Не согласовано' || agreedItem.approvalResult === 'Не утверждено';
        }) : null;

        const lastSigningPaper = order.SigningOnPaper ? _.last(order.SigningOnPaper) : null;
        if (notAgreedOrApproved) {
            return  {
                user: notAgreedOrApproved.agreedBy.fioFull + ' (' + notAgreedOrApproved.agreedBy.post + ')',
                approvalType: notAgreedOrApproved.approvalType,
                approvalFactDate: notAgreedOrApproved.approvalFactDate,
                approvalNote: notAgreedOrApproved.approvalNote,
                files: notAgreedOrApproved.fileApproval
            }
        } else if (lastSigningPaper) {
            const head = <User>order.head;
            return {
                user: head ? head.fio + ' (' + head.post + ')' : null,
                approvalType: null,
                approvalFactDate: lastSigningPaper.signingFactDate,
                approvalNote: lastSigningPaper.noteSigning.noteText,
                files: lastSigningPaper.File
            }
        }
    }

    loadDictionaries() {
        const deferred = this.$q.defer();
        this.$q.all([
            this.nsiRestService.get('RD_TYPES')
        ]).then((values: any[]) => {
            this.rdTypes = values[0];
            deferred.resolve();
        }).catch(_ => {
            deferred.reject();
        });
        return deferred.promise;
    }

    initDropzone() {
        const dzOptions = {
            autoProcessQueue: true,
            withCredentials: true,
            parallelUploads: 1,
            maxFilesize: 500,
            paramName: "file",
            dictDefaultMessage: 'Загрузить файл. <br/> Допустимые расширения: doc, docx',
            accept: (file, done) => {
                let fileName: string = file.name;
                let match = /.*\.docx?/.exec(fileName.toLowerCase());
                if (match) {
                    done();
                } else {
                    done("Допустимые расширения: doc, docx");
                }
            }
        };

        const dzOptionsAttachment = {
            autoProcessQueue: true,
            withCredentials: true,
            maxFilesize: 500,
            parallelUploads: 1,
            paramName: "file",
            dictDefaultMessage: 'Загрузить файл. <br/> Допустимые расширения: doc, docx, pdf',
            accept: (file, done) => {
                let fileName: string = file.name;
                let match = /.*\.docx?|\.pdf/.exec(fileName.toLowerCase());
                if (match) {
                    done();
                } else {
                    done("Допустимые расширения: doc, docx, pdf");
                }
            }
        };

        this.dzDraftFilesWithoutAttOptions = _.extend(_.cloneDeep(dzOptions), {
            url: this.fileResource.getDraftFilesWithoutAttUploadUrl(this.order.documentID)
        });
        this.dzDraftFilesWithoutAttCallbacks = {
            error: (file, errorMessage) => {
                this.toastr.warning(errorMessage.message ? errorMessage.message : errorMessage, 'Ошибка');
                this.dzDraftFilesWithoutAttMethods.removeFile(file);
            }, success: (dzFile, file) => {
                this.dzDraftFilesWithoutAttMethods.removeFile(dzFile);
                this.order.draftFilesWithoutAtt = file;
            }
        };
        this.dzDraftFilesAttOptions = _.extend(_.cloneDeep(dzOptionsAttachment), {
            url: this.fileResource.getDraftFilesAttUploadUrl(this.order.documentID)
        });
        this.dzDraftFilesAttCallbacks = {
            error: (file, errorMessage) => {
                this.toastr.warning(errorMessage.message ? errorMessage.message : errorMessage, 'Ошибка');
                this.dzDraftFilesAttMethods.removeFile(file);
            }, success: (dzFile, file) => {
                this.dzDraftFilesAttMethods.removeFile(dzFile);
                this.order.attach = file;
            }
        };
    }

    saveAndCreateDraftFile() {
        if (!this.validate()) {
            return;
        }
        this.creatingDraftFile = true;
        this.updateDocument(this.order).then(() => {
            return this.createDraftFile(this.order.documentID);
        }).then((result: OrderDraftFile) => {
            this.creatingDraftFile = false;
            this.order.draftFiles = result;
        }, () => {
            this.creatingDraftFile = false;
        })
    }

    createDraftFile(id: string): ng.IPromise<OrderDraftFile> {
        let deferred: ng.IDeferred<OrderDraftFile> = this.$q.defer();
        this.orderResource.createDraftFile({id: id}, (result: OrderDraftFile) => {
            deferred.resolve(result);
        }, () => {
            deferred.reject();
        });
        return deferred.promise;
    }

    checkDoubleAgreed() {
        let items = this.order.approval.approvalCycle.agreed.slice(0),
            testItem;

        while (items.length) {
            testItem = items.shift();
            if (_.find(items, item => testItem.agreedBy.accountName === item.agreedBy.accountName)) {
                return testItem;
            }
        }
        return false;

    }

    validate() {
        if (!this.order.documentTypeCode || !this.order.documentYear || !this.order.documentContent ||
            !this.order.executor || !this.order.mailList || this.order.executor.length === 0 || this.order.mailList.length === 0) {
            this.toastr.error('Не заполнены обязательные поля');
            return false;
        }
        const double = this.checkDoubleAgreed();
        if (double) {
            this.toastr.error(`В листе согласования повторяется согласующий ${double.agreedBy.fioFull}`);
            return false;
        }
        return true;
    }

    saveDocument() {
        if (!this.validate()) {
            return;
        }
        this.updateDocument(this.order).then(() => {
        })
    }

    saveAndSendToApproval() {
        if (!this.validate()) {
            return;
        }
        if (!this.order.draftFiles) {
            this.toastr.error('Не найден файл проект приказа');
            return;
        }
        this.order.signingElectronically = this.order.signingElectronically.toString() === 'true';
        this.order.PublishingSite = String(this.order.PublishingSite) === 'true';
        if (!this.order.signingElectronically && (!this.order.approval.approvalCycle.agreed || this.order.approval.approvalCycle.agreed.length === 0)) {
            this.toastr.error('Для отправки на согласование необходимо выбрать согласующих');
            return false;
        }
        if (this.order.approval && this.order.approval.approvalCycle && this.order.approval.approvalCycle.agreed) {
            let agreed = this.order.approval.approvalCycle.agreed;
            for (let i = 0; i < agreed.length; i++) {
                if (!agreed[i].approvalTypeCode || !agreed[i].agreedBy) {
                    this.toastr.error('Не заполнен список согласования');
                    return;
                }
            }
        }
        if (this.order.signingElectronically && !this.hasApproval()) {
            this.toastr.error('Не указано утверждающее лицо');
            return;
        }
        if (!this.order.signingElectronically && !_.some(this.order.approval.approvalCycle.agreed, (item) => {
            return item.agreedBy.accountName === (<User>this.order.head).accountName;
        })) {
            this.toastr.error('Необходимо добавить руководителя в лист согласования');
            return;
        }
        this.order.activeApprover = this.order.approval.approvalCycle.agreed[0].agreedBy.fioShort;
        this.sendingToApproval = true;
        this.updateDocument(this.order).then(() => {
            return this.orderTaskService.updateProcess(this.taskId, [{
                "name": "ElApprovalVar",
                "value": true
            },{
                "name": "RDPublishingSite",
                "value": this.order.PublishingSite
            }]);
        }).then(() => {
            this.sendingToApproval = false;
            this.toastr.success('Документ отправлен на согласование.');
            this.$window.location.href = this.$window.location.protocol + '//' + this.$window.location.host + '/oasi/#/app/my-tasks';
        }, () => {
            this.sendingToApproval = false;
        })
    }

    hasApproval() {
        if (!this.order.approval || !this.order.approval.approvalCycle || !this.order.approval.approvalCycle.agreed) {
            return false;
        }
        return _.some(this.order.approval.approvalCycle.agreed, (a) => {
            return a.approvalTypeCode === this.ApprovalType.approval;
        })
    }

    isApplication() {
        return this.rdType.APPLICATION;
    }

    createDraftFilesWithoutAtt() {
        this.creatingDraftFilesWithoutAtt = true;
        this.updateDocument(this.order).then(() => {
            return this.orderResource.createDraftFilesWithoutAtt({id: this.order.documentID}, {}).$promise
        }).then((draftFilesWithoutAtt: OrderDraftFile) => {
            this.order.draftFilesWithoutAtt = draftFilesWithoutAtt;
            this.creatingDraftFilesWithoutAtt = false;
        }, () => {
            this.creatingDraftFilesWithoutAtt = false;
        });
    }

    deleteDraftFilesWithoutAtt() {
        this.orderResource.deleteDraftFilesWithoutAtt({id: this.order.documentID}, {}, () => {
            delete this.order.draftFilesWithoutAtt;
        })
    }

    deleteDraftFilesAtt() {
        this.orderResource.deleteDraftFilesAtt({id: this.order.documentID}, {}, () => {
            delete this.order.attach;
        })
    }

    deleteDraftFiles() {
        this.orderResource.deleteDraftFiles({id: this.order.documentID}, {}, () => {
            delete this.order.draftFiles;
        })
    }

    combineDraftFile() {
        this.combiningDraftFile = true;
        this.orderResource.combineDraftFiles({id: this.order.documentID}, {}, (draftFiles: OrderDraftFile) => {
            this.order.draftFiles = draftFiles;
            this.combiningDraftFile = false;
        }, () => {
            this.combiningDraftFile = false;
        })
    }

}

angular.module('app').component('processEditDoc', new ProcessEditDocComponent());
