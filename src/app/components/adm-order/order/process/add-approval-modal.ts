//import IModalServiceInstance = angular.ui.bootstrap.IModalServiceInstance;

class AddApprovalModalComponent {
    public bindings: any;
    public controller: any;
    public templateUrl: string;

    constructor() {
        this.controller = AddApprovalModalController;
        this.templateUrl = 'app/components/adm-order/order/process/add-approval-modal.html';
        this.bindings = {
            modalInstance: '<',
            resolve: '<'
        };
    }
}

class AddApprovalModalController {
    static $inject = [
        'alertService'
    ];

    private modalInstance: angular.ui.bootstrap.IModalServiceInstance;
    private resolve: any;
    private agreed: OrderApprovalListItem[];
    private documentTypeCode: string;
    private except: string[];
    private dueDate: Date;

    constructor(
        private alertService: oasiWidgets.IAlertService
    ) {
    }

    $onInit() {
        this.agreed = [];
        this.documentTypeCode = this.resolve.documentTypeCode;
        this.except = this.resolve.except;
        this.dueDate = this.resolve.dueDate;
    }

    isValid() {
        return !_.some(this.agreed, _ => {
            return !_.agreedBy;
        })
    }

    add() {
        this.modalInstance.close(this.agreed);
    }

    cancel() {
        if (this.agreed.length > 0) {
            this.alertService.confirm({
                message: 'Добавленные данные не сохранятся. Продолжить без сохранения?',
                type: 'default',
                okButtonText: 'Да',
                windowClass: 'zindex'
            }).then(_ => {
                this.modalInstance.dismiss();
            })
        } else {
            this.modalInstance.dismiss();
        }
    }

}

angular.module('app').component('addApprovalModal', new AddApprovalModalComponent());
