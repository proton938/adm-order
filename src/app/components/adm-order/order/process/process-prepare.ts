import { maxFilesize } from '../../../../consts/consts';

class ProcessPrepareComponent {
    public bindings: any;
    public require: any;
    public controller: any;
    public templateUrl: string;

    constructor() {
        this.controller = ProcessPrepareController;
        this.templateUrl = 'app/components/adm-order/order/process/process-prepare.html';
        this.bindings = {};
        this.require = {
            taskWrapper: '^taskWrapper'
        }
    }
}

class ProcessPrepareController extends AbstractProcessController {
    static $inject = [
        '$q',
        '$scope',
        '$state',
        '$stateParams',
        '$timeout',
        '$window',
        'toastr',
        'orderResource',
        'fileResource',
        'nsiRestService',
        'orderTaskService',
        'activityRestService',
        'nsiUserService',
        '$rootScope',
        'ApprovalType',
        'prAuthorizationService',
        'registerDocumentService',
        'session',
        'nsiResource'
    ];

    private loadingStatus: LoadingStatus;
    private taskId: string;
    private taskInfo: ITaskInfo;
    private order: Order;
    private account: any = {};
    private rdType: RDType;

    private rdTypes: RDType[];

    private ApprovalUmpl: any[];

    private dzDraftFilesWithoutAttOptions: any;
    private dzDraftFilesWithoutAttCallbacks: any;
    private dzDraftFilesWithoutAttMethods: any;

    private dzDraftFilesAttOptions: any;
    private dzDraftFilesAttCallbacks: any;
    private dzDraftFilesAttMethods: any;

    private saving: boolean;
    private sendingToApproval: boolean;
    private sendingToRegistration: boolean;
    private creatingDraftFile: boolean;
    private creatingDraftFilesWithoutAtt: boolean;
    private combiningDraftFile: boolean;

    private hasPreregPermission: boolean;
    private canViewRegButton: boolean;

    constructor(public $q: ng.IQService,
        private $scope: any,
        private $state: any,
        private $stateParams: any,
        private $timeout: any,
        private $window: any,
        private toastr: Toastr,
        public orderResource: IOrderResource,
        private fileResource: IFileResource,
        private nsiRestService: oasiNsiRest.NsiRestService,
        private orderTaskService: IOrderTaskService,
        public activityRestService: oasiBpmRest.ActivityRestService,
        public nsiUserService: INsiUserService,
        private $rootScope: IScope,
        private ApprovalType: any,
        private authorizationService: IAuthorizationService,
        private registerDocumentService: IRegisterDocumentService,
        private sessionStorage: oasiSecurity.ISessionStorage,
        private nsiResource: INsiResource) {
        super($q, orderResource, nsiUserService, activityRestService);
    }

    $onInit() {
        this.loadingStatus = LoadingStatus.LOADING;
        let id: string = this.$stateParams.documentID;
        this.taskId = this.$stateParams.taskId;
        this.hasPreregPermission = this.authorizationService.check('OASI_ADMORDER_PREREGBUTTON');
        this.canViewRegButton = this.sessionStorage.groups().some((group) => group === orders.Permissions.OASI_ADMORDER_REG_WITHOUT_APPROVE);
        this.getTaskInfo(this.$stateParams.documentID, 'PrepareRD', this.$stateParams.taskId).then((taskInfo: ITaskInfo) => {
            this.taskInfo = taskInfo;
            this.order = taskInfo.document;

            this.account.accountName = this.sessionStorage.login();
            this.account.fio = this.sessionStorage.name();
            this.account.post = this.sessionStorage.post();

            this.order.responsibleExecutor = {
                fio: this.sessionStorage.fullName(),
                accountName: this.sessionStorage.login(),
                post: this.sessionStorage.post(),
                email: this.sessionStorage.mail(),
                phoneNumber: this.sessionStorage.telephoneNumber()
            };

            this.initDropzone();
            return this.loadDictionaries();
        }).then(() => {
            this.rdType = _.find(this.rdTypes, _ => {
                return _.DOC_CODE === this.order.documentTypeCode;
            })
            if (this.rdType.approvalUmpl) {
                const approvalUmpl: any = this.ApprovalUmpl.filter(ApprovalUmpl => ApprovalUmpl.DOC_CODE === this.rdType.DOC_CODE);
                if (approvalUmpl && approvalUmpl.length && approvalUmpl[0].children && approvalUmpl[0].children.length) {
                    this.$q.all(
                        approvalUmpl[0].children.map(user => {
                            return this.nsiUserService.user(user.approvalPerson);
                        })
                    ).then((response: User[]) => {
                        const agreed = [];
                        this.order.approval = this.order.approval || new OrderApproval(1);
                        this.order.approval.approvalCycle.agreed = this.order.approval.approvalCycle.agreed || [];
                        response.map(u => {
                            this.order.mailList = this.order.mailList || [];
                            if (!this.order.mailList.some(user => user.accountName === u.accountName)) {
                                this.order.mailList.push(u);
                            }
                            if (!this.order.approval.approvalCycle.agreed.some(agr => agr.agreedBy.accountName === u.accountName)) {
                                const approval = _.find(approvalUmpl[0].children, approvalUser => approvalUser.approvalPerson === u.accountName);
                                let approvalNum = this.order.approval.approvalCycle.agreed.length + 1;
                                const approvalUser: any = {
                                    approvalNum,
                                    approvalType: approval.approvalType === 'assent' ? 'Одобрение (без ЭП)' : 'Согласование (с ЭП)',
                                    approvalTypeCode: approval.approvalType,
                                    agreedBy: { ...u, fioFull: u.fio }
                                };
                                console.log('ApprovalUser: ', approvalUser);
                                agreed.push(approvalUser);
                            }
                        });
                        this.order.approval.approvalCycle.agreed = this.order.approval.approvalCycle.agreed.concat(agreed);
                        // this.order.approval.approvalCycle.agreed = this.order.approval.approvalCycle.agreed.concat(agreed.filter(a => a.approvalTypeCode === 'assent'))
                        //     .concat(agreed.filter(a => a.approvalTypeCode === 'agreed'));
                    });
                }
            }
            return this.nsiResource.departments().$promise;
        })
            .then((departments) => {
                const userDepartmentCode = this.sessionStorage.departmentCode();
                const userResponsibleDep = _.find(departments, ['description', userDepartmentCode]);

                if (userResponsibleDep) {
                    this.order.responsibleDep = {
                        departmentCode: userResponsibleDep.description,
                        departmentFull: userResponsibleDep.name
                    };
                }
            })
            .then(() => {
                this.loadingStatus = LoadingStatus.SUCCESS;
            }, () => {
                this.loadingStatus = LoadingStatus.ERROR;
            })
    }

    loadDictionaries() {
        const deferred = this.$q.defer();
        this.$q.all([
            this.nsiRestService.get('RD_TYPES'),
            this.nsiRestService.get('ApprovalUmpl')
        ]).then((values: any[]) => {
            this.rdTypes = values[0];
            this.ApprovalUmpl = values[1];
            console.log("ApprovalUmpl: ", this.ApprovalUmpl);
            deferred.resolve();
        }).catch(_ => {
            deferred.reject();
        });
        return deferred.promise;
    }

    initDropzone() {
        const dzOptions = {
            autoProcessQueue: true,
            withCredentials: true,
            parallelUploads: 1,
            maxFilesize: 500,
            paramName: "file",
            dictDefaultMessage: 'Загрузить файл. <br/> Допустимые расширения: doc, docx',
            accept: (file, done) => {
                let fileName: string = file.name;
                let match = /.*\.docx?/.exec(fileName.toLowerCase());
                if (match) {
                    done();
                } else {
                    done("Допустимые расширения: doc, docx");
                }
            }
        };

        const dzOptionsAttachment = {
            autoProcessQueue: true,
            withCredentials: true,
            parallelUploads: 1,
            paramName: "file",
            maxFilesize: 500,
            dictDefaultMessage: 'Загрузить файл. <br/> Допустимые расширения: doc, docx, pdf',
            accept: (file, done) => {
                let fileName: string = file.name;
                let match = /.*\.docx?|\.pdf/.exec(fileName.toLowerCase());
                if (match) {
                    done();
                } else {
                    done("Допустимые расширения: doc, docx, pdf");
                }
            }
        };

        this.dzDraftFilesWithoutAttOptions = _.extend(_.cloneDeep(dzOptions), {
            url: this.fileResource.getDraftFilesWithoutAttUploadUrl(this.order.documentID)
        });
        this.dzDraftFilesWithoutAttCallbacks = {
            error: (file, errorMessage) => {
                this.toastr.warning(errorMessage.message ? errorMessage.message : errorMessage, 'Ошибка');
                this.dzDraftFilesWithoutAttMethods.removeFile(file);
            }, success: (dzFile, file) => {
                this.dzDraftFilesWithoutAttMethods.removeFile(dzFile);
                this.order.draftFilesWithoutAtt = file;
            }
        };
        this.dzDraftFilesAttOptions = _.extend(_.cloneDeep(dzOptionsAttachment), {
            url: this.fileResource.getDraftFilesAttUploadUrl(this.order.documentID)
        });
        this.dzDraftFilesAttCallbacks = {
            error: (file, errorMessage) => {
                this.toastr.warning(errorMessage.message ? errorMessage.message : errorMessage, 'Ошибка');
                this.dzDraftFilesAttMethods.removeFile(file);
            }, success: (dzFile, file) => {
                this.dzDraftFilesAttMethods.removeFile(dzFile);
                this.order.attach = file;
            }
        };
    }

    saveAndCreateDraftFile() {
        if (!this.validate()) {
            return;
        }
        this.creatingDraftFile = true;
        this.updateDocument(this.order).then(() => {
            return this.createDraftFile(this.order.documentID);
        }).then((result: OrderDraftFile) => {
            this.creatingDraftFile = false;
            this.order.draftFiles = result;
        }, () => {
            this.creatingDraftFile = false;
        })
    }

    createDraftFile(id: string): ng.IPromise<OrderDraftFile> {
        let deferred: ng.IDeferred<OrderDraftFile> = this.$q.defer();
        this.orderResource.createDraftFile({ id: id }, (result: OrderDraftFile) => {
            deferred.resolve(result);
        }, () => {
            deferred.reject();
        })
        return deferred.promise;
    }

    validate() {
        if (!this.order.documentTypeCode || !this.order.documentYear || !this.order.documentContent ||
            !this.order.executor || !this.order.mailList || this.order.executor.length === 0 || this.order.mailList.length === 0) {
            this.toastr.error('Не заполнены обязательные поля');
            return false;
        }
        if (!this.order.head) {
            this.toastr.error('Не заполнен руководитель, утверждающий документ');
            return false;
        }
        const double = this.checkDoubleAgreed();
        if (double) {
            this.toastr.error(`В листе согласования повторяется согласующий ${double.agreedBy.fioFull}`);
            return false;
        }
        return true;
	}
	
	approvalListIsValid() {
		return this.order.approval.approvalCycle.agreed.some(a => !a.approvalTypeCode || !a.agreedBy)
	}

    checkDoubleAgreed() {
        let items = this.order.approval.approvalCycle.agreed.slice(0),
            testItem;

        while (items.length) {
            testItem = items.shift();
            if (_.find(items, item => testItem.approvalTypeCode === item.approvalTypeCode
                && testItem.agreedBy.accountName === item.agreedBy.accountName)) {
                return testItem;
            }
        }
        return false;

    }

    saveDocument() {
        if (!this.validate()) {
            return;
        }
        this.saving = true;
        this.updateDocument(this.order).then(() => {
            this.saving = false;
        }, () => {
            this.saving = false;
        })
    }

    saveAndSendToApproval() {
        if (!this.validate()) {
            return;
        }

        if (!this.order.draftFiles) {
            this.toastr.error('Не найден файл проект приказа');
            return;
        }
        this.order.signingElectronically = this.order.signingElectronically.toString() == 'true';
        if (!this.order.signingElectronically && (!this.order.approval.approvalCycle.agreed || this.order.approval.approvalCycle.agreed.length === 0)) {
            this.toastr.error('Для отправки на согласование необходимо выбрать согласующих');
            return false;
        }
        if (this.order.approval && this.order.approval.approvalCycle && this.order.approval.approvalCycle.agreed) {
            let agreed = this.order.approval.approvalCycle.agreed;
            for (let i = 0; i < agreed.length; i++) {
                if (!agreed[i].approvalTypeCode || !agreed[i].agreedBy) {
                    this.toastr.error('Не заполнен список согласования');
                    return;
                }
            }
        }
        if (this.order.signingElectronically && !this.hasApproval()) {
            this.toastr.error('Не указано утверждающее лицо');
            return;
        }
        if (!this.order.signingElectronically && !_.some(this.order.approval.approvalCycle.agreed, (item) => {
            return item.agreedBy.accountName === (<User>this.order.head).accountName;
        })) {
            this.toastr.error('Необходимо добавить руководителя в лист согласования');
            return;
        }
        this.order.activeApprover = this.order.approval.approvalCycle.agreed[0].agreedBy.fioShort;
        this.sendingToApproval = true;
        this.updateDocument(this.order).then(() => {
            return this.orderTaskService.updateProcess(this.taskId, [{
                "name": "ElApprovalVar",
                "value": true
            }]);
        }).then(() => {
            this.sendingToApproval = false;
            this.toastr.success('Документ отправлен на согласование.');
            this.$window.location.href = this.$window.location.protocol + '//' + this.$window.location.host + '/oasi/#/app/my-tasks';
        }, () => {
            this.sendingToApproval = false;
        });
    }

    saveAndSendToRegistration() {
        if (!this.validate()) {
            return;
        }
        if (!this.order.draftFiles) {
            this.toastr.error('Не найден файл проект приказа');
            return;
        }
        this.sendingToRegistration = true;
        this.updateDocument(this.order).then(() => {
            return this.orderTaskService.updateProcess(this.taskId, [{
                "name": "ElApprovalVar",
                "value": false
            }]);
        }).then(() => {
            this.sendingToRegistration = false;
            this.toastr.success('Документ отправлен на регистрацию.');
            this.$window.location.href = this.$window.location.protocol + '//' + this.$window.location.host + '/oasi/#/app/my-tasks';
        }, () => {
            this.sendingToRegistration = false;
        })
    }

    hasApproval() {
        if (!this.order.approval || !this.order.approval.approvalCycle || !this.order.approval.approvalCycle.agreed) {
            return false;
        }
        return _.some(this.order.approval.approvalCycle.agreed, (a) => {
            return a.approvalTypeCode === this.ApprovalType.approval;
        })
    }

    isApplication() {
        return this.rdType.APPLICATION;
    }

    isPrereg() {
        return this.rdType.PREREG;
    }

    createDraftFilesWithoutAtt() {
        this.creatingDraftFilesWithoutAtt = true;
        this.updateDocument(this.order).then(() => {
            return this.orderResource.createDraftFilesWithoutAtt({ id: this.order.documentID }, {}).$promise
        }).then((draftFilesWithoutAtt: OrderDraftFile) => {
            this.order.draftFilesWithoutAtt = draftFilesWithoutAtt;
            this.creatingDraftFilesWithoutAtt = false;
        }, () => {
            this.creatingDraftFilesWithoutAtt = false;
        });
    }

    deleteDraftFilesWithoutAtt() {
        this.orderResource.deleteDraftFilesWithoutAtt({ id: this.order.documentID }, {}, () => {
            delete this.order.draftFilesWithoutAtt;
        })
    }

    deleteDraftFilesAtt() {
        this.orderResource.deleteDraftFilesAtt({ id: this.order.documentID }, {}, () => {
            delete this.order.attach;
        })
    }

    deleteDraftFiles() {
        this.orderResource.deleteDraftFiles({ id: this.order.documentID }, {}, () => {
            delete this.order.draftFiles;
        })
    }

    combineDraftFile() {
        this.combiningDraftFile = true;
        this.orderResource.combineDraftFiles({ id: this.order.documentID }, {}, (draftFiles: OrderDraftFile) => {
            this.order.draftFiles = draftFiles;
            this.combiningDraftFile = false;
        }, () => {
            this.combiningDraftFile = false;
        })
    }

}

angular.module('app').component('processPrepare', new ProcessPrepareComponent());
