import IScope = angular.IScope;
class ProcessComponent {
    public bindings: any;
    public controller: any;
    public templateUrl: string;

    constructor() {
        this.controller = ProcessController;
        this.templateUrl = 'app/components/adm-order/order/process/process.html';
        this.bindings = {};
    }
}

class AbstractProcessController {

    private taskWrapper: any;

    constructor(
    	public $q: ng.IQService,
    	public orderResource: IOrderResource,
        public nsiUserService: INsiUserService,
    	public activityRestService: oasiBpmRest.ActivityRestService
    ) {

    }
    
    getTaskInfo(id: string, formKey: string, taskId: number): ng.IPromise<ITaskInfo> {
    	let deferred: ng.IDeferred<ITaskInfo> = this.$q.defer();
        let taskInfo: ITaskInfo = {
        	document: null,
        	task: this.taskWrapper.task,
        	taskVariables: null,
        	assigneeUser: null,
        	taskParams: {
	            formKey: formKey,
	            documentId: id,
	            taskId: taskId
	        }
        };
    	this.loadDocument(id).then((document) => {
    		taskInfo.document = document;
    		return this.activityRestService.getTaskVariables(taskId);
    	}).then((taskVariables) => {
    		taskInfo.taskVariables = taskVariables;
    		return this.nsiUserService.user(taskInfo.task.assignee);
    	}).then((user) => {
    		taskInfo.assigneeUser = user;
    		deferred.resolve(taskInfo);
    	});
    	return deferred.promise;
    }

    loadDocument(id: string) {
        let deferred: ng.IDeferred<Order> = this.$q.defer();

        this.orderResource.getById({id: id}, (data: Order) => {
            deferred.resolve(data);
        }, () => {
        	deferred.reject();
        });

        return deferred.promise;
    }
    
    updateDocument(document: Order): ng.IPromise<boolean> {
        let id: string = document.documentID;
        let deferred: ng.IDeferred<boolean> = this.$q.defer();

        this.orderResource.update({id: id}, {
            document
        }, () => {
            deferred.resolve(true);
        }, () => {
        	deferred.reject();
        });

        return deferred.promise;
    }

    patchDocument(id: string, diff: any[]): ng.IPromise<boolean> {
        let deferred: ng.IDeferred<boolean> = this.$q.defer();

        this.orderResource.patch({id: id}, diff, () => {
            deferred.resolve(true);
        }, () => {
        	deferred.reject();
        });

        return deferred.promise;
    }

    approve(id: string, taskId: string, approve: boolean, params: any): ng.IPromise<OrderApprovalListItem> {
        return this.orderResource.approve({id: id, taskId: taskId, approve: approve}, params).$promise;
    }
    
    prepareSign(id: string, params: any): ng.IPromise<FileType> {
        let deferred: ng.IDeferred<FileType> = this.$q.defer();

        this.orderResource.prepareSign({id: id}, params, (result) => {
            deferred.resolve(result);
        }, () => {
        	deferred.reject();
        });

        return deferred.promise;
    }
    
    closeCycle(id: string): ng.IPromise<any> {
        let deferred: ng.IDeferred<any> = this.$q.defer();

        this.orderResource.closeCycle({id: id}, null, (result) => {
            deferred.resolve(result);
        }, () => {
        	deferred.reject();
        });

        return deferred.promise;
    }    

}

class ITaskParams {
    formKey: string;
    documentId: string;
    taskId: number;
}

class ITaskInfo {
    document: any;
    task: oasiBpmRest.ITask;
    taskVariables: oasiBpmRest.ITaskVariable[];
    assigneeUser: any;
    taskParams: ITaskParams;
}

class ProcessController {
    static $inject = [
    	'$q',
    	'$stateParams',
    	'session'
	];

    private taskId: string;
    private login: string;
    private fio: string;

    constructor(
    	private $q: ng.IQService,
    	private $stateParams: any,
    	private session: oasiSecurity.ISessionStorage
    ) {
        this.taskId = this.$stateParams.taskId;
        this.login = this.session.login();
        this.fio = this.session.fullName();
    }

    $onInit() {    	
    }

    goToMyTasks() {
        location.assign('/oasi/#/app/my-tasks');
    }

}

angular.module('app').component('process', new ProcessComponent());