
class ProcessScanComponent {
    public bindings: any;
    public require: any;
    public controller: any;
    public templateUrl: string;
    public testVar: string;

    constructor() {
        this.controller = ProcessScanController;
        this.templateUrl = 'app/components/adm-order/order/process/process-scan.html';
        this.bindings = {};
        this.require = {
            taskWrapper: '^taskWrapper'
        }
    }
}

class ProcessScanController extends AbstractProcessController {
    static $inject = [
    	'$q',
    	'$filter',
    	'$timeout',
    	'$state',
    	'$scope',
    	'$stateParams',
        '$window',
    	'toastr',
    	'orderResource',
    	'reportResource',
    	'orderTaskService',
    	'activityRestService',
    	'nsiUserService',
    	'fileResource',
        'fileHttpService',
        'mggtService',
    	'prSession',
    	'$rootScope',
    	'ApprovalType'
	];

    private loadingStatus: LoadingStatus;
    private taskId: string;
    private taskInfo: ITaskInfo;
 	private order: Order;

 	private dropzone: Dropzone;
 	private barcode: number;

 	private attached: boolean = false;
 	private submitting: boolean;

    constructor(
		public $q: ng.IQService,
		private $filter: any,
		private $timeout: any,
        private $state: any,
        private $scope: any,
        private $stateParams: any,
        private $window: any,
        private toastr: Toastr,
        public orderResource: IOrderResource,
        private reportResource: IReportResource,
        private orderTaskService: IOrderTaskService,
        public activityRestService: oasiBpmRest.ActivityRestService,
        public nsiUserService: INsiUserService,
        private fileResource: IFileResource,
        private fileHttpService: oasiFileRest.FileHttpService,
        private mggtService: MggtService,
        private sessionStorage: ISessionStorage,
        private $rootScope: IScope,
        private ApprovalType: any
    ) {
		super($q, orderResource, nsiUserService, activityRestService);
    }

    testVar: any = 'test';
    $onInit() {
        this.loadingStatus = LoadingStatus.LOADING;
        let id: string = this.$stateParams.documentID;
        this.taskId = this.$stateParams.taskId;
        this.getTaskInfo(this.$stateParams.documentID, 'PrintAndSignDoc', this.$stateParams.taskId).then((taskInfo: ITaskInfo) => {
        	this.taskInfo = taskInfo;
        	this.order = taskInfo.document;
            console.log(this);
        	this.initDropzone();
            this.testMail(this.order);
            this.sendMails(id);
        	return this.loadDictionaries();
        }).then(() => {
        	this.loadingStatus = LoadingStatus.SUCCESS;
        }, () => {
        	this.loadingStatus = LoadingStatus.ERROR;
        })
    }

    testMail(document: Order): ng.IPromise<boolean> {
        let id: string = document.documentID;
        let deferred: ng.IDeferred<boolean> = this.$q.defer();
        this.orderResource.mail({id: id}, {
            document
        }, () => {
            this.toastr.success("Успешно");
            deferred.resolve(true);
        }, () => {
            this.toastr.warning("Некорректно заполнен список рассылки");
            deferred.reject();
        });
        return deferred.promise;
    }

    private loadingAppStatus: LoadingStatus;
    sendMails(documentID) {
        this.loadingAppStatus = LoadingStatus.LOADING;
        this.orderResource
            .mail({
                id: documentID,
            })
            .$promise
            .then(() => {
                this.$rootScope.$emit('updateCounters');
                this.toastr.success("Рассылка успешно осуществлена");
                //this.reload();
            })
            .catch(() => {
                this.toastr.warning("Рассылка завершилась с ошибкой");
                //this.reload();
            });
    }


    loadDictionaries() {
        return true;
    }



    deleteFiles(): ng.IPromise<any> {
        const deferred = this.$q.defer();
        if (this.order.files && this.order.files.length > 0) {
            this.$q.all(
                _.map(this.order.files, (file) => {
                    return this.fileHttpService.deleteFile(file.fileID);
                })
            ).then(() => {
                this.order.files = [];
                deferred.resolve();
            }).catch(() => {
                deferred.reject();
            })
        } else {
            deferred.resolve();
        }
        return deferred.promise;
    }

    initDropzone() {
        this.$timeout(() => {
        	let self = this;
            let options: any = {
                autoProcessQueue: true,
                withCredentials: true,
                parallelUploads: 1,
                paramName: "file",
                url: this.fileResource.getUploadUrl(this.order.documentID),
                accept: function(file, done) {
                    self.deleteFiles().then(() => {
                        done();
                    }).catch(() => {
                        done("Не удалось удалить предыдуший файл.");
                    })
                },
                init: function() {
                    this.on("error", function(file, errorMessage) {
                        self.toastr.warning(errorMessage.message ? errorMessage.message : errorMessage, 'Ошибка');
                        this.removeFile(file);
                    });
                    this.on("success", function(file, result) {
                    	self.$scope.$apply(() => {
	                    	self.order.files = self.order.files || [];
	                    	self.order.files.push(result);
                            self.attached = true;
                    	});
                    	this.removeFile(file);
                    });
                }
            };

            this.dropzone = new Dropzone("#file-dropzone", options);
        });
    }

    execute() {
        this.submitting = true;

        let canceledDocument = this.order.canceledDocument;

        this.orderResource.createIsogdXml({
            id: this.order.documentID
        }).$promise.then(file => {
            this.order.isogdXmlGuid = file.fileID;
            return this.updateDocument(this.order);
        })
            .then(() => {
                return this.orderTaskService.updateProcess(this.taskId, [{
                    "name": "RDPublishingSite",
                    "value": this.order.PublishingSite
                }]);
            })
            .then(() => {
                if (!canceledDocument || !canceledDocument.canceledDocumentID) {
                    return true;
                }
                return this.orderResource.sendToPSO({id: canceledDocument.canceledDocumentID, includeFile: false});
            }).then(() => {
            return this.mggtService.sendOrder(this.order);
            }).then(() => {
                if (this.order.systemCode === 'gpzu' && this.order.systemParentID
                    && canceledDocument && canceledDocument.canceledDocumentNum && canceledDocument.canceledDocumentDate) {
                    return this.orderTaskService.getProcessDefinition('rd_ToIsogd_ID').then((process: oasiBpmRest.IProcessDefinition) => {
                        let props = [{
                            "name": "EntityIdVar",
                            "value": this.order.documentID
                        },{
                            "name": "EntityDescriptionVar",
                            "value": this.order.documentTypeValue + " " + this.order.documentContent
                        }];
                        return this.orderTaskService.initProcess(process.id, props);
                    });
                }
            })
            .then(() => {
                this.submitting = false;
                this.$window.location.href = this.$window.location.protocol + '//' + this.$window.location.host + '/oasi/#/app/my-tasks';
            }, () => {
                this.submitting = false;
            })
    }

    deleteFile(fileInfo: any) {
        let fileID = fileInfo.idFile;
        this.fileHttpService.deleteFile(fileID).then(() => {
            _.remove(this.order.files, file => {
                return file.fileID === fileID;
            });
            this.updateDocument(this.order);
        });
    }

    updateDocument(document: Order): ng.IPromise<boolean> {
        let id: string = document.documentID;
        let deferred: ng.IDeferred<boolean> = this.$q.defer();

        this.orderResource.update({id: id}, {
            document
        }, () => {
            deferred.resolve(true);
        });

        return deferred.promise;
    }

}

angular.module('app').component('processScan', new ProcessScanComponent());
