class ProcessPrintAndSignComponent {
    public bindings: any;
    public require: any;
    public controller: any;
    public templateUrl: string;

    constructor() {
        this.controller = ProcessPrintAndSignController;
        this.templateUrl = 'app/components/adm-order/order/process/process-printandsign.html';
        this.bindings = {};
        this.require = {
            taskWrapper: '^taskWrapper'
        }
    }
}

class ProcessPrintAndSignController extends AbstractProcessController {
    static $inject = [
        '$q',
        '$timeout',
        '$state',
        '$stateParams',
        '$scope',
        '$window',
        'toastr',
        'orderResource',
        'reportResource',
        'orderTaskService',
        'activityRestService',
        'cancelDocumentService',
        'fileResource',
        'session',
        'nsiUserService'
    ];

    private loadingStatus: LoadingStatus;
    private taskId: string;
    private taskInfo: ITaskInfo;
    private order: Order;

    private dropzone: Dropzone;

    private signingFactDate: Date;
    private noteText: string;

    private sendingToRegistration: boolean;
    private sendingBackToWork: boolean;

    constructor(public $q: ng.IQService,
                private $timeout: any,
                private $state: any,
                private $stateParams: any,
                private $scope: any,
                private $window: any,
                private toastr: Toastr,
                public orderResource: IOrderResource,
                private reportResource: IReportResource,
                private orderTaskService: IOrderTaskService,
                public activityRestService: oasiBpmRest.ActivityRestService,
                private cancelDocumentService: ICancelDocumentService,
                private fileResource: IFileResource,
                private sessionStorage: oasiSecurity.ISessionStorage,
                public nsiUserService: INsiUserService) {
        super($q, orderResource, nsiUserService, activityRestService);
    }

    $onInit() {
        this.loadingStatus = LoadingStatus.LOADING;
        let id: string = this.$stateParams.documentID;
        this.taskId = this.$stateParams.taskId;
        this.signingFactDate = new Date();
        this.getTaskInfo(this.$stateParams.documentID, 'PrintAndSignDoc', this.$stateParams.taskId).then((taskInfo: ITaskInfo) => {
            this.taskInfo = taskInfo;
            this.order = taskInfo.document;

            /*this.initDropzone();*/

            this.loadingStatus = LoadingStatus.SUCCESS;
        }, () => {
            this.loadingStatus = LoadingStatus.ERROR;
        })
    }

    initDropzone() {
        this.$timeout(() => {
            let self = this;
            let options: any = {
                autoProcessQueue: true,
                withCredentials: true,
                parallelUploads: 1,
                paramName: "file",
                url: this.fileResource.getPaperFileUploadUrl(this.order.documentID, this.taskId),
                maxFiles: 1,
                init: function() {
                    this.on("error", function(file, errorMessage) {
                        self.toastr.warning(errorMessage.message ? errorMessage.message : errorMessage, 'Ошибка');
                        this.removeFile(file);
                    });
                    this.on("maxfilesexceeded", function (file) {
                        self.toastr.warning('Можно загрузить всего один файл.', 'Ошибка');
                        this.removeFile(file);
                    });
                    this.on("success", function(file, result) {
                        self.$scope.$apply(() => {
                            let SigninigOnPaper = self.getSigninigOnPaper();
                            SigninigOnPaper.File = SigninigOnPaper.File || [];
                            SigninigOnPaper.File.push(result);
                        });
                        this.removeFile(file);
                    });
                }
            };

            this.dropzone = new Dropzone("#file-dropzone", options);
        });
    }

    getSigninigOnPaper() {
        if (!this.order.SigningOnPaper) {
            this.order.SigningOnPaper = [];
        }
        let result = _.find(this.order.SigningOnPaper, (s) => {
            return s.taskId === this.taskId;
        });
        if (!result) {
            result = new OrderSigningOnPaper(this.taskId);
            this.order.SigningOnPaper.push(result);
        }
        return result;
    }

    signed() {
        let signinigOnPaper = this.getSigninigOnPaper();
        signinigOnPaper.signingResult = true;
        signinigOnPaper.signingFactDate = this.signingFactDate;
        signinigOnPaper.noteSigning = {
            noteText: this.noteText,
            noteAuthor: this.sessionStorage.login(),
            noteAuthorFIO: this.sessionStorage.name(),
            noteDate: new Date()
        };
        this.sendingToRegistration = true;

        let canceledDocument = this.order.canceledDocument;
        this.cancelDocumentService.checkCanceledDocument(canceledDocument).then(() => {
            return this.updateDocument(this.order);
        }).then(() => {
            return this.cancelDocumentService.cancelDocument(canceledDocument, this.order.documentDate);
        }).then(() => {
            return this.orderTaskService.updateProcess(this.taskId, [{
                "name": "ApprovedByBossVar",
                "value": true
            }]);
        }).then(() => {
            this.sendingToRegistration = false;
            this.toastr.success('Документ отправлен на сканирование.');
            this.$window.location.href = this.$window.location.protocol + '//' + this.$window.location.host + '/oasi/#/app/my-tasks';
        }, () => {
            this.sendingToRegistration = false;
        })
    }

    sendBackToWork() {
        if (!this.noteText) {
            this.toastr.error('Не указан комментарий');
            return;
        }
        let signinigOnPaper = this.getSigninigOnPaper();
        signinigOnPaper.signingResult = false;
        signinigOnPaper.signingFactDate = this.signingFactDate;
        signinigOnPaper.noteSigning = {
            noteText: this.noteText,
            noteAuthor: this.sessionStorage.login(),
            noteAuthorFIO: this.sessionStorage.name(),
            noteDate: new Date()
        };
        this.sendingBackToWork = true;
        this.updateDocument(this.order).then(() => {
            return this.closeCycle(this.order.documentID);
        }).then(() => {
            return this.orderTaskService.updateProcess(this.taskId, [{
                "name": "ApprovedByBossVar",
                "value": false
            }]);
        }).then(() => {
            this.sendingBackToWork = false;
            this.toastr.success('Документ отправлен на доработку.');
            this.$window.location.href = this.$window.location.protocol + '//' + this.$window.location.host + '/oasi/#/app/my-tasks';
        }, () => {
            this.sendingBackToWork = false;
        })
    }

    getApprovalReportUrl() {
        return this.reportResource.getApprovalReportUrl(this.order.documentID);
    }

    deletePaperFile(fileID: string) {
        this.fileResource.deletePaperFile({
            id: this.order.documentID,
            taskId: this.taskId,
            fileGuid: fileID
        }, {}, () => {
            _.remove(this.getSigninigOnPaper().File, f => {
                return f.fileID === fileID;
            })
        });
    }

}

angular.module('app').component('processPrintAndSign', new ProcessPrintAndSignComponent());
