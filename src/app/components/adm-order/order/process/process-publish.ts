class ProcessPublishComponent {
    public bindings: any;
    public require: any;
    public controller: any;
    public templateUrl: string;

    constructor() {
        this.controller = ProcessPublishController;
        this.templateUrl = 'app/components/adm-order/order/process/process-publish.html';
        this.bindings = {};
        this.require = {
            taskWrapper: '^taskWrapper'
        }
    }
}

class ProcessPublishController extends AbstractProcessController {

    static $inject = [
        '$q',
        '$state',
        '$scope',
        '$stateParams',
        '$window',
        'toastr',
        'orderResource',
        'orderTaskService',
        'activityRestService',
        'nsiUserService'
    ];

    private loadingStatus: LoadingStatus;
    private taskId: string;
    private taskInfo: ITaskInfo;
    private order: Order;

    private submitting: boolean;

    constructor(
        public $q: ng.IQService,
        private $state: any,
        private $scope: any,
        private $stateParams: any,
        private $window: any,
        private toastr: Toastr,
        public orderResource: IOrderResource,
        private orderTaskService: IOrderTaskService,
        public activityRestService: oasiBpmRest.ActivityRestService,
        public nsiUserService: INsiUserService
    ) {
        super($q, orderResource, nsiUserService, activityRestService);
    }

    $onInit() {

        this.loadingStatus = LoadingStatus.LOADING;
        this.taskId = this.$stateParams.taskId;

        this.getTaskInfo(this.$stateParams.documentID, 'PublishRD', this.$stateParams.taskId).then((taskInfo: ITaskInfo) => {

            this.taskInfo = taskInfo;
            this.order = taskInfo.document;

        }).then(() => {
            this.loadingStatus = LoadingStatus.SUCCESS;
        }, () => {
            this.loadingStatus = LoadingStatus.ERROR;
        });

    }

    execute() {
        this.submitting = true;
        this.orderTaskService.updateProcess(this.taskId, [{
            "name": "ElApprovalVar",
            "value": true
        }]).then(() => {
                this.submitting = false;
                this.$window.location.href = this.$window.location.protocol + '//' + this.$window.location.host + '/oasi/#/app/my-tasks';
            }, () => {
                this.submitting = false;
            });
    }

}

angular.module('app').component('processPublish', new ProcessPublishComponent());