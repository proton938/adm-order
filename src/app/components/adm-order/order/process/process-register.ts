class ProcessRegisterComponent {
    public bindings: any;
    public require: any;
    public controller: any;
    public templateUrl: string;

    constructor() {
        this.controller = ProcessRegisterController;
        this.templateUrl = 'app/components/adm-order/order/process/process-register.html';
        this.bindings = {};
        this.require = {
            taskWrapper: '^taskWrapper'
        }
    }
}

class ProcessRegisterController extends AbstractProcessController {
    static $inject = [
        '$q',
        '$filter',
        '$timeout',
        '$state',
        '$stateParams',
        '$window',
        'toastr',
        'orderResource',
        'reportResource',
        'orderTaskService',
        'activityRestService',
        'alertService',
        'redirectToTaskService',
        'cancelDocumentService',
        'registerDocumentService',
        'fileResource',
        'session',
        '$rootScope',
        'ApprovalType',
        'nsiUserService'
    ];

    private loadingStatus: LoadingStatus;
    private taskId: string;
    private taskInfo: ITaskInfo;
    private order: Order;

    private registrars: User[];

    private sending: boolean;

    constructor(public $q: ng.IQService,
                private $filter: any,
                private $timeout: any,
                private $state: any,
                private $stateParams: any,
                private $window: any,
                private toastr: Toastr,
                public orderResource: IOrderResource,
                private reportResource: IReportResource,
                private orderTaskService: IOrderTaskService,
                public activityRestService: oasiBpmRest.ActivityRestService,
                private alertService: oasiWidgets.IAlertService,
                private redirectToTaskService: IRedirectToTaskService,
                private cancelDocumentService: ICancelDocumentService,
                private registerDocumentService: IRegisterDocumentService,
                private fileResource: IFileResource,
                private sessionStorage: oasiSecurity.ISessionStorage,
                private $rootScope: IScope,
                private ApprovalType: any,
                public nsiUserService: INsiUserService) {
        super($q, orderResource, nsiUserService, activityRestService);
    }

    $onInit() {
        this.loadingStatus = LoadingStatus.LOADING;
        let id: string = this.$stateParams.documentID;
        this.taskId = this.$stateParams.taskId;
        this.getTaskInfo(this.$stateParams.documentID, 'PrintAndSignDoc', this.$stateParams.taskId).then((taskInfo: ITaskInfo) => {
            this.taskInfo = taskInfo;
            this.order = taskInfo.document;
            return this.loadDictionaries();
        }).then(() => {
            this.order.registar = _.find(this.registrars, (r) => {
                return r.accountName === this.sessionStorage.login();
            });
            this.loadingStatus = LoadingStatus.SUCCESS;
        }, () => {
            this.loadingStatus = LoadingStatus.ERROR;
        })
    }


    loadDictionaries() {
        return this.$q.all([
            this.nsiUserService.registrars()
        ]).then((values: any[]) => {
            this.registrars = values[0];
        });
    }

    updateDocument(document: Order): ng.IPromise<boolean> {
        let id: string = document.documentID;
        let deferred: ng.IDeferred<boolean> = this.$q.defer();

        this.orderResource.update({id: id}, {
            document
        }, () => {
            this.$rootScope.$emit('updateCounters');
            deferred.resolve(true);
        });

        return deferred.promise;
    }

    generateDocumentNumber() {
        return this.registerDocumentService.registerDocument(this.order).then(() => {
            _.extend(this.order, {
                documentStatusCode: 'REGISTER',
                documentStatusValue: 'Зарегистрирован',
                documentStatusColor: 'primary'
            });
        })
    }

    generateBarcode(order: Order): ng.IPromise<any> {
        if (order.barcode) {
            return this.$q.resolve();
        } else {
            return this.orderResource.generateBarcode().$promise.then((response: OrderBarcodeResponse) => {
                order.barcode = response.barcode;
                order.barcodeText = response.barcodeText;
                return this.$q.resolve();
            });
        }
    }

    registerDocument() {
        this.sending = true;
        this.registerDocumentService.checkLinkedDocuments(this.order).then(() => {
            return this.registerDocumentService.fillDocumentDate(this.order);
        }).then(() => {
            return this.cancelDocumentService.checkCanceledDocument(this.order.canceledDocument);
        }).then(() => {
            return this.generateDocumentNumber();
        }).then(() => {
            return this.generateBarcode(this.order);
        }).then(() => {
            return this.updateDocument(this.order);
        }).then(() => {
            return this.alertService.message({
                message: 'Документ успешно зарегистрирован, данные сохранены'
            }).catch(() => {
                return this.$q.resolve();
            });
        }).then(() => {
            return this.cancelDocumentService.cancelDocument(this.order.canceledDocument, this.order.documentDate);
        }).then(() => {
            return this.orderResource.register({id: this.order.documentID}).$promise;
        }).then(() => {
            return this.orderTaskService.updateProcess(this.taskId, [{
                "name": "EntityDescriptionVar",
                "value": this.order.documentTypeValue.split(' ')[0] + " № " + this.order.documentNumber + " " + this.order.documentContent
            }])
        }).then(() => {
            this.sending = false;
            this.$window.location.href = this.$window.location.protocol + '//' + this.$window.location.host + '/oasi/#/app/my-tasks';
        }).catch((err) => {
            if (err.documentID && err.documentTypeValue) {
                this.redirectToTaskService.redirectToTask({ linked: {documentID: err.documentID, documentTypeValue: err.documentTypeValue}});
            }
            this.sending = false;
        });
}

}

angular.module('app').component('processRegister', new ProcessRegisterComponent());
