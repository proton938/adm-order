class ProcessToisogdComponent {
    public bindings: any;
    public require: any;
    public controller: any;
    public templateUrl: string;

    constructor() {
        this.controller = ProcessToisogdController;
        this.templateUrl = 'app/components/adm-order/order/process/process-toisogd.html';
        this.bindings = {};
        this.require = {
            taskWrapper: '^taskWrapper'
        }
    }
}

class ProcessToisogdController extends AbstractProcessController {
    static $inject = [
    	'$q', 
    	'$filter',
    	'$timeout',
    	'$state',
    	'$scope',  
    	'$stateParams',
        '$window',
    	'toastr',
    	'orderResource',
    	'reportResource',
    	'orderTaskService', 
    	'activityRestService',
    	'nsiUserService'
	];
	
    private loadingStatus: LoadingStatus;
    private taskId: string;
    private taskInfo: ITaskInfo;
 	private order: Order;

 	private submitting: boolean;

    constructor(
		public $q: ng.IQService,
		private $filter: any,
		private $timeout: any,
        private $state: any,
        private $scope: any,
        private $stateParams: any,
        private $window: any,
        private toastr: Toastr,
        public orderResource: IOrderResource,
        private reportResource: IReportResource,
        private orderTaskService: IOrderTaskService,
        public activityRestService: oasiBpmRest.ActivityRestService,
        public nsiUserService: INsiUserService
    ) {
		super($q, orderResource, nsiUserService, activityRestService);
    }

    $onInit() {
        this.loadingStatus = LoadingStatus.LOADING;
        let id: string = this.$stateParams.documentID;
        this.taskId = this.$stateParams.taskId;
        this.getTaskInfo(this.$stateParams.documentID, 'ToIsogdOnceMore', this.$stateParams.taskId).then((taskInfo: ITaskInfo) => {
            this.taskInfo = taskInfo;
        	this.order = taskInfo.document;
        }).then(() => {
        	this.loadingStatus = LoadingStatus.SUCCESS;
        }, () => {
        	this.loadingStatus = LoadingStatus.ERROR;
        })
    }

    execute() {
        this.submitting = true;

        this.orderResource.createIsogdXml({
            id: this.order.documentID
        }).$promise.then(file => {
            this.order.isogdXmlGuid = file.fileID;
            return this.updateDocument(this.order);
        }).then(() => {
            return this.orderTaskService.updateProcess(this.taskId, [{
                "name": "EntityIdVar",
                "value": this.order.documentID
            }])
        }).then(() => {
                this.submitting = false;
                this.$window.location.href = this.$window.location.protocol + '//' + this.$window.location.host + '/oasi/#/app/my-tasks';
            }, () => {
                this.submitting = false;
        })
    }

}

angular.module('app').component('processToisogd', new ProcessToisogdComponent());
