class OrderComponent {
    public bindings: any;
    public controller: any;
    public templateUrl: string;

    constructor() {
        this.controller = OrderController;
        this.templateUrl = 'app/components/adm-order/order/order.html';
        this.bindings = {};
    }
}

class OrderController {
    static $inject = ['$q', '$state', '$stateParams', 'orderResource', '$rootScope', 'nsiRestService', 'session'];

    private loadingAppStatus: LoadingStatus;

    private orderPromise: ng.IPromise<Order>;
    private order: Order;
    private rdStatuses: any;
    private editingOrder: boolean;

    constructor(private $q: ng.IQService,
                private $state: any,
                private $stateParams: any,
                private orderResource: IOrderResource,
                private $rootScope: IScope,
                private nsiRestService: oasiNsiRest.NsiRestService,
                private session: oasiSecurity.ISessionStorage) {

    }

    $onInit() {
        let id: string = this.$stateParams.id;

        this.editingOrder = this.$stateParams.editingOrder;
        this.nsiRestService.get('RD_STATUS').then(result => {
            this.rdStatuses = _.reduce(result, (obj, status: RDStatus) => {
                obj[status.STATUS_CODE] = status.statusAnalytic;
                return obj;
            }, {});
        });

        if (!id) {
            this.orderPromise = this.newOrder();
            this.editingOrder = true;
        } else {
            this.orderPromise = this.loadOrder(id);
        }
    }

    newOrder() {
        let deferred: ng.IDeferred<Order> = this.$q.defer();

        this.order = {
            documentID: "",
            bpmProcess: null,
            documentTypeCode: "",
            documentTypeValue: "",
            documentYear: (new Date()).getFullYear(), // current year
            documentNumber: null,
            documentDate: null,
            documentExpireDate: null,
            canceledDocument: null,
            documentStatusCode: "NEW",
            documentStatusValue: "Новый",
            documentStatusColor: "warning",
            documentContent: "",
            address: "",
            approvedDocumentNumber: "",
            createDateTime: "",
            createAuthor: null,
            barcode: null,
            folderID: "",
            files: [],
            head: null,
            executor: [],
            registar: null,
            mailList: [],
            mailDateTime: "",
            psoInputID: null,
            psoOutputID: [],
            draftFiles: null,
            signingElectronically: false,
            approval: null,
            approvalHistory: null,
            SigningOnPaper: null,
            draftFilesWithoutAtt: null,
            attach: null,
            documentThemeCode: null,
            documentThemeValue: null,
            additionalMaterials: [],
            draftFilesRegInfo: null
        };

        this.loadingAppStatus = LoadingStatus.SUCCESS;

        deferred.resolve(this.order);

        return deferred.promise;
    }

    copyOrder() {
        let deferred: ng.IDeferred<Order> = this.$q.defer();

        let fieldsToCopy = _.pick(this.order, [
            'documentTypeCode',
            'documentTypeValue',
            'documentYear',
            'documentContent',
            'head',
            'executor',
            'registar',
            'mailList'
        ]);
        let newOrder: Order = _.extend(new Order(), {
            documentID: "",
            documentTypeCode: "",
            documentTypeValue: "",
            documentYear: (new Date()).getFullYear(), // current year
            documentNumber: null,
            documentDate: "",
            documentExpireDate: "",
            canceledDocument: null,
            documentStatusCode: "NEW",
            documentStatusValue: "Новый",
            documentStatusColor: "warning",
            documentContent: "",
            address: "",
            approvedDocumentNumber: "",
            createDateTime: "",
            createAuthor: null,
            barcode: null,
            folderID: "",
            files: [],
            head: null,
            executor: [],
            registar: null,
            mailList: [],
            mailDateTime: "",
            psoInputID: null,
            psoOutputID: []
        }, fieldsToCopy);

        this.order = newOrder;

        this.loadingAppStatus = LoadingStatus.SUCCESS;

        deferred.resolve(this.order);

        return deferred.promise;
    }

    loadOrder(id: string) {
        let self: OrderController = this;
        this.loadingAppStatus = LoadingStatus.LOADING;
        let deferred: ng.IDeferred<Order> = this.$q.defer();

        self.orderResource.getById({id: id}, (data: Order) => {
            self.loadingAppStatus = LoadingStatus.SUCCESS;

            self.order = data;

            deferred.resolve(self.order);
        });

        return deferred.promise;
    }

    createRelated() {
        this.$state.go('app.adm-order.initiate', {
            linkedDocumentID: this.order.documentID,
            linkedDocumentType: this.order.documentTypeValue
        });
    }

    private createDocument(document: Order): ng.IPromise<string> {
        let deferred: ng.IDeferred<string> = this.$q.defer();

        this.orderResource.create(
            {
                document
            },
            (data: OrderCreateResult) => {
                this.$rootScope.$emit('updateCounters');
                deferred.resolve(data.id);
            });

        return deferred.promise;
    }

    private updateDocument(document: Order): ng.IPromise<boolean> {
        let id: string = document.documentID;
        let deferred: ng.IDeferred<boolean> = this.$q.defer();

        this.orderResource.update({id: id}, {
            document
        }, () => {
            this.$rootScope.$emit('updateCounters');
            deferred.resolve(true);
        });

        return deferred.promise;
    }

    saveOrder(document: Order): ng.IPromise<string> {
        let self: OrderController = this;
        let deferred: ng.IDeferred<string> = this.$q.defer();

        if (document.documentID) {
            this.updateDocument(document)
                .then(function () {
                    self.$state.reload();
                });
        } else {
            this.createDocument(document)
                .then(function (id: string) {
                    self.$state.go('app.adm-order.order', {
                        id: id,
                        editingOrder: false
                    });
                });
        }

        return deferred.promise;
    }

    editOrder(edit: boolean) {
        this.editingOrder = edit;
    }

    cancelEditOrder() {
        this.editOrder(false);
    }

    createCopy() {
        this.orderPromise = this.copyOrder();
        this.editOrder(true);
    }

    hasAdminPermission() {
        return this.session.hasPermission('OASI_ADMORDER_ADMIN');
    }
}

angular.module('app').component('order', new OrderComponent());
