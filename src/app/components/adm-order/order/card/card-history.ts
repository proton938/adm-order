class CardHistoryController {
    static $inject = [
        'toastr',
        '$q',
        'prAuthorizationService',
        '$stateParams',
        'orderResource'
    ];

    private orderPromise: ng.IPromise<Order>;
    private order: Order;
    private history: oasiDocumentLogs.HistoryModel;

    private loadingStatus: LoadingStatus = LoadingStatus.LOADING;

    constructor(
        private toastr: Toastr,
        private $q: ng.IQService,
        private authorizationService: IAuthorizationService,
        private $stateParams: ng.ui.IStateParamsService,
        private orderResource: IOrderResource) {
    }

    $onInit() {
        this.orderPromise.then((order: Order) => {
            this.order = order;
            return this.orderResource.log({ id: this.order.documentID }).$promise;
        }).then(data => {
            let logs: oasiDocumentLogs.HistoryLogModel[] = [];
            data.forEach(r => {
                let date = new Date(r.dateEdit);
                let ms = date.getTime();
                if (r.jsonPatch) {
                    r.jsonPatch.forEach(a => {
                        logs.push(new oasiDocumentLogs.HistoryLogModel(ms, r.userName, a.op, a.path, a.value));
                    });
                } else {
                    logs.push(new oasiDocumentLogs.HistoryLogModel(ms, r.userName, 'add', '/document', r.jsonNew));
                }
            });
            this.history = {
                logs: logs,
                allLogs: logs,
                sortings: oasiDocumentLogs.HistoryLogSorting.fillSortings(),
                filter: new oasiDocumentLogs.HistoryLogFilter(logs),
                pagination: new oasiDocumentLogs.HistoryLogPagination(logs.length)
            };
            this.loadingStatus = LoadingStatus.SUCCESS;
        }).catch(error => {
            console.log(error);
        });
    }

}

angular.module('app').component('cardHistory', {
    controller: CardHistoryController,
    templateUrl: 'app/components/adm-order/order/card/card-history.html',
    bindings: {
        orderPromise: "<",
    }
});
