class CardEditComponent {
    public bindings: any;
    public controller: any;
    public templateUrl: string;

    constructor() {
        this.controller = CardEditController;
        this.templateUrl = 'app/components/adm-order/order/card/card-edit.html';
        this.bindings = {
            orderPromise: "<",
            saveOrder: '&',
            cancel: '&'
        };
    }
}

class CardEditController {
    static $inject = [
        '$timeout', '$state', '$scope',
        '$q', 'orderResource', 'nsiRestService',
        'alertService',
        '$filter', 'toastr', 'prAuthorizationService', 'cancelDocumentService', 'registerDocumentService', 'prSession',
        'nsiUserService', 'nsiResource'];

    private loadingDictStatus: LoadingStatus;

    private orderPromise: ng.IPromise<Order>;
    private order: Order;
    private saveOrder: (data: any) => ng.IPromise<string>;
    private cancel: () => {};

    private isCanceledDocumentDisabled: boolean;

    private orderTypes: RDType[];
    private executives: User[];
    private executors: User[];
    private registrars: User[];
    private subscribers: User[];
    private emailGroups: EmailGroup[];
    private availableYears: number[];
    private themes: RDTheme[];
    private availableThemes: RDTheme[] = [];
    private updatedTypes: string[] = [];

    private selectedEmailGroups: EmailGroup[];

    private availableCanceledDocuments: CancelType[];
    private availableUpdatedDocuments: UpdateType[];

    constructor(private $timeout: any,
                private $state: any,
                private $scope: any,
                private $q: ng.IQService,
                private orderResource: IOrderResource,
                private nsiRestService: oasiNsiRest.NsiRestService,
                private alerService: oasiWidgets.IAlertService,
                private $filter: any,
                private toastr: any,
                private authorizationService: IAuthorizationService,
                private cancelDocumentService: ICancelDocumentService,
                private registerDocumentService: IRegisterDocumentService,
                private prSession: ISessionStorage,
                private nsiUserService: INsiUserService,
                private nsiResource: INsiResource) {
    }

    $onInit() {
        if (!this.authorizationService.check('OASI_ADMORDER_FORM')) {
            this.loadingDictStatus = LoadingStatus.ERROR;
            return;
        }

        let self: CardEditController = this;

        self.availableYears = _.range(2016, 2028);

        self.orderPromise.then(function (order: Order) {
            self.order = angular.copy(order);
            
            if (self.order.responsibleDep) {
                self.order.responsibleDep = self.convertDepartmentFormatFromDictionary(self.order.responsibleDep);
            }

            if (!_.isArray(self.order.head) && self.order.head) {
                let user: User = <User>self.order.head;
                self.order.head = [user];
            }

            if (!_.isArray(self.order.registar) && self.order.registar) {
                let user: User = <User>self.order.registar;
                self.order.registar = [user];
            }
            self.isCanceledDocumentDisabled = order.documentStatusCode !== 'NEW';

            self.loadDictionaries();
        });
    }

    loadDictionaries() {
        let self: CardEditController = this;
        this.loadingDictStatus = LoadingStatus.LOADING;

        this.$q.all([
            this.nsiRestService.get('RD_TYPES'),
            this.nsiRestService.get('emailGroups'),
            this.nsiUserService.executors(),
            this.nsiUserService.registrars(),
            this.nsiUserService.subscribers(),
            this.nsiRestService.get('RD_THEME'),
            this.nsiRestService.get('RD_STATUS'),
            this.nsiResource.departments().$promise
        ]).then((values: any[]) => {
            self.orderTypes = values[0].filter(type => this.prSession.rdTypes().indexOf(type.DOC_CODE) > -1);
            self.emailGroups = values[1];
            self.executors = values[2];
            self.registrars = values[3];
            self.subscribers = values[4];
            self.themes = values[5];
            self.updatedTypes = values[6]
                .filter(status => status.statusAnalytic === 'Действующий')
                .map(status => status.STATUS_CODE);
            self.departments = values[7];

            self.documentTypeChanged();
            self.loadingDictStatus = LoadingStatus.SUCCESS;
        });
    }

    loadDependentOnOrderType() {
        const docAccessAction = _.find(this.orderTypes, type => type.DOC_CODE === this.order.documentTypeCode).DOC_ACCESS_ACTION;
        this.nsiUserService.executives(docAccessAction).then((executives: User[]) => {
            this.executives = executives;
        });
    }

    documentTypeChanged() {
        if (this.order.documentTypeCode) {
            this.loadDependentOnOrderType();
            this.availableThemes = _.filter(this.themes, th => {
                return th.DOC_CODE === this.order.documentTypeCode;
            })
        } else {
            this.availableThemes = [];
        }
    }

    themeChanged() {
        if (this.order.documentThemeCode) {
            this.order.documentThemeValue = _.find(this.availableThemes, th => {
                return th.THEME_CODE === this.order.documentThemeCode;
            }).THEME_NAME;
        } else {
            delete this.order.documentThemeValue;
        }
    }

    isThemeDisabled() {
        return this.order.approvedDocumentNumber || this.order.previousDocument;
    }

    searchCanceledDocuments(query) {
        let self: CardEditController = this;

        this.orderResource
            .searchQuery({name: query, page: 0, size: 20})
            .$promise
            .then((response) => {
                this.availableCanceledDocuments = _.map(response.content, (order: Order) => {
                    return {
                        canceledDocumentID: order.documentID,
                        canceledDocumentNum: order.documentNumber,
                        canceledDocumentDate: order.documentDate,
                        canceledDocumentType: order.documentTypeValue,
                        canceledDocumentTypeCode: order.documentTypeCode,
                    }
                });
            });
    }

    searchUpdatedDocuments(query) {
        this.orderResource
            .search({page: 0, size: 20}, {docName: query, docStatusCode: this.updatedTypes}, (response) => {
                this.availableUpdatedDocuments = (response.content || []).map(order => {
                    return {
                        updatedDocumentID: order.documentID,
                        updatedDocumentNum: order.documentNumber,
                        updatedDocumentDate: order.documentDate,
                        updatedDocumentType: order.documentTypeValue,
                        updatedDocumentTypeCode: order.documentTypeCode
                    };
                });
            });
    }

    validate() {
        if (!this.order.documentTypeCode || !this.order.documentYear || !this.order.documentContent ||
            !this.order.executor || !this.order.mailList || this.order.executor.length === 0 || this.order.mailList.length === 0) {
            this.toastr.error('Не заполнены обязательные поля');
            return false;
        }
        return true;
    }

    save() {
        if (!this.validate()) {
            return;
        }

        let self: CardEditController = this;

        let head: any = <any> self.order.head;
        if (_.isArray(head) && _.size(<User[]>head)) {
            let user = _.first(<User[]>head);
            delete user.email;
            self.order.head = user;
        } else {
            delete self.order.head;
        }

        let registar: any = <any> self.order.registar;
        if (_.isArray(registar) && _.size(<User[]>registar)) {
            let user = _.first(<User[]>registar);
            delete user.email;
            self.order.registar = user;
        } else {
            delete self.order.registar;
        }

        _.each(self.order.executor, (user: User) => {
            delete user.email;
        });

        self.order.responsibleDep = this.convertDepartmentFormatToDictionary(self.order.responsibleDep);

        this.registerDocumentService.fillDocumentDate(self.order).then(() => {
            self.order.documentExpireDate = this.$filter('date')(self.order.documentExpireDate, "yyyy-MM-dd");
            self.saveOrder({document: self.order});
        })
    }

    /*
        две следуюшие функции для конвертации
        были добавленны из-за несовпадения формата данных 
        приходящих с сервера и принимающих сервером
    */
    convertDepartmentFormatToDictionary(department) {
        return {
            departmentFull: department.name,
            departmentShort: '',
            departmentCode: department.description
        };
    }

    convertDepartmentFormatFromDictionary(department) {
        return {
            name: department.departmentFull,
            description: department.departmentCode
        };
    }

    cancelEdit() {
        this.cancel();
    }

    isRequiredFieldsFilled() {
        if (!this.order.documentTypeCode) {
            return false;
        }

        if (!this.order.documentYear) {
            return false;
        }

        if (!this.order.documentContent || !this.order.documentContent.trim()) {
            return false;
        }

        return true;
    }

    generateDocumentNumber() {
        return this.registerDocumentService.registerDocument(this.order).then(() => {
            _.extend(this.order, {
                documentStatusCode: 'REGISTER',
                documentStatusValue: 'Зарегистрирован',
                documentStatusColor: 'primary'
            });
            return this.$q.resolve();
        })
    }

    registerDocument() {
        if (!this.isRequiredFieldsFilled()) {
            return this.alerService.message({
                message: 'Заполните все обязательные поля, отмеченные звездочкой'
            });
        }

        this.registerDocumentService.fillDocumentDate(this.order).then(() => {
            return this.cancelDocumentService.checkCanceledDocument(this.order.canceledDocument);
        }).then(() => {
            return this.generateDocumentNumber();
        }).then(() => {
            return this.save();
        }).then(() => {
            return this.cancelDocumentService.cancelDocument(this.order.canceledDocument, this.order.documentDate);
        }).then(() => {
            this.alerService.message({
                message: 'Документ успешно зарегистрирован, данные сохранены'
            });
        })
    }

    findUser(users: User[], login: string) {
        return _.find(users, (user) => {
            return user.accountName === login;
        })
    }

    addEmailGroup(emailGroup: EmailGroup) {
        this.order.mailList = this.order.mailList || [];
        let usersToAdd = _.chain(emailGroup.users).map((login) => {
            return this.findUser(this.subscribers, login);
        }).filter((user) => {
            return user;
        }).filter((user) => {
            return !this.findUser(this.order.mailList, user.accountName);
        }).value();
        _.each(usersToAdd, (user) => {
            this.order.mailList.push(user);
        });
    }

}

angular.module('app').component('cardEdit', new CardEditComponent());
angular.module('app').filter('userFilter', () => {
    function checkUser(user: User, criterion: string) {
        if (!criterion) {
            return true;
        }
        let tokens = criterion.split(" ");
        return !_.chain(tokens).map((token) => {
            let res = -1;
            if (user.fio) {
                res = user.fio.toLowerCase().indexOf(token.toLowerCase());
            }
            return res;
        }).some((index) => {
            return index < 0;
        }).value();
    }

    return (users: User[], criterion: string) => {
        return _.filter(users, (user) => {
            return checkUser(user, criterion);
        })
    }
})