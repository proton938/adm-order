class CardApprovalsController {
    static $inject = [
    	'toastr', 
    	'$q', 
    	'prAuthorizationService',
    	'$stateParams'
	];
	
    private orderPromise: ng.IPromise<Order>;
    private order: Order;	

   private loadingStatus: LoadingStatus = LoadingStatus.LOADING;

    constructor(
        private toastr: Toastr,
        private $q: ng.IQService,
        private authorizationService: IAuthorizationService,
        private $stateParams: ng.ui.IStateParamsService) {
    }

    $onInit() {
        this.orderPromise.then((order: Order) => {
            this.order = order;
        	this.loadingStatus = LoadingStatus.SUCCESS;
        });            
    }

}

angular.module('app').component('cardApprovals', {
    controller: CardApprovalsController,
    templateUrl: 'app/components/adm-order/order/card/card-approvals.html',
    bindings: {
        orderPromise: "<",
    }    
});