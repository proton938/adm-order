class CardProcessController {
    static $inject = [
    	'$log',
    	'$state',
    	'prAuthorizationService',
    	'activityProcessHistoryManager',
    	'toastr',
        '$stateParams'
	];
	
	private orderPromise: ng.IPromise<Order>;
	private order: Order;	
	
    private init: oasiBpmHistory.IInfoAboutProcessInit;

    private taskInfo: any;
    private loadingStatus: LoadingStatus = LoadingStatus.LOADING;

    constructor(
    	private $log: any,
    	private $state: any,
        private authorizationService: IAuthorizationService,
        private activityProcessHistoryManager: oasiBpmHistory.IProcessHistoryManager,
        private toastr: Toastr,
        private $stateParams: any
    ) {
    }

    $onInit() {
    	// Кнопка временно отключена
    	//this.canAddProcess = this.authorizationService.check('OASI_ADMORDER_ADDPROCESS');
    	//this.canAddProcess = false;
    	//this.canDeleteProcess = this.authorizationService.check('OASI_ADMORDER_DELPROCESS');
        this.orderPromise.then((order: Order) => {
            this.order = order;
            
			let categories: oasiBpmHistory.IProcessCategory[] = [];
	        categories.push({
	            name: "Распорядительные документы",
	            key: "rd_",
	            compareType: oasiBpmHistory.CompareType.startsWith
	        }); 
	        this.activityProcessHistoryManager.init("rd", categories, this.order.documentID, "EntityIdVar").then(result => {
	            this.init = result;
	            this.loadingStatus = LoadingStatus.SUCCESS;
	        }).catch(error => {
	            this.$log.error(error);
	            this.loadingStatus = LoadingStatus.ERROR;
	        });           
        });  
        
    }
    finishTask(taskId: number, vars: oasiBpmRest.ITaskVariable[], reqNum: string) {
        // this.myBlockUI.start("Завершение задачи ...");
        this.activityProcessHistoryManager.finishTask(taskId, vars).then(response => {
            // this.myBlockUI.stop();
            this.toastr.success('Задача успешно завершена!');
            this.$state.go('app.adm-order.order', { id: this.$stateParams.id }, { reload: true });
        }).catch(error => {
            // this.myBlockUI.stop();
            this.toastr.error("Ошибка при завершении задачи");
            this.$log.error(error);
        });
    }
    formFinishTask(taskId: string): ng.IPromise<oasiBpmHistory.IAddProcessVariable[]> {
        return this.activityProcessHistoryManager.formFinishTask(taskId);
    }
    formStartProcess(processId: string): ng.IPromise<oasiBpmHistory.IAddProcessVariable[]> {
        return this.activityProcessHistoryManager.formStartProcess(processId);
    }
    openDiagram(id: string, type: string, name: string) {
        let image = this.activityProcessHistoryManager.getDiagram(id, type);
        let url = this.$state.href('diagram', { title: name, image: image }, {absolute: true});
        window.open(url,'_blank');
    }
    startProcess(id,name,vars,reqNum) {
         // this.myBlockUI.start(`Запуск процесса "${name}" ...`);
        this.activityProcessHistoryManager.startProcess(id, <oasiBpmRest.ITaskVariable[]>vars).then(response => {
            // this.myBlockUI.stop();
            this.toastr.success('Процесс успешно запущен!');
            this.$state.go('app.adm-order.order', { id: reqNum }, { reload: true });
        }).catch(error => {
            // this.myBlockUI.stop();
            this.toastr.error("Ошибка при запуске процесса");
            this.$log.error(error);
        });
    }
    processEditable() {
        return this.authorizationService.check('OASI_ADMORDER_EDIT_PROCESS');
    }
    deleteProcess(id,name,reqNum) {
        // this.myBlockUI.start(`Удаление процесса "${name}" ...`);
        this.activityProcessHistoryManager.deleteProcess(id).then(response => {
            // this.myBlockUI.stop();
            this.toastr.success('Процесс успешно удален!');
            this.$state.go('app.adm-order.order', { id: this.$stateParams.id }, { reload: true });
        }).catch(error => {
            // this.myBlockUI.stop();
            this.toastr.error("Ошибка при удалении процесса");
            this.$log.error(error);
        });
    }
}

angular.module('app').component('cardProcess', {
    controller: CardProcessController,
    templateUrl: 'app/components/adm-order/order/card/card-process.html',
    bindings: {
        orderPromise: "<",
    }    
});