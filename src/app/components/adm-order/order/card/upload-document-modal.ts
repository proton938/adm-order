class UploadDocumentModalComponent implements ng.IComponentOptions {
    public bindings: any;
    public controller: any;
    public templateUrl: string;

    constructor() {
        this.controller = UploadDocumentModalController;
        this.templateUrl = 'app/components/adm-order/order/card/upload-document-modal.html';
        this.bindings = {
            resolve: '<',
            close: '&',
            dismiss: '&'
        };
    }
}

class UploadDocumentModalController implements ng.ui.bootstrap.IModalServiceInstance {
    static $inject = ['$timeout', '$q', '$scope', 'toastr', 'fileResource'];

    public close:(result?: any) => {};
    public dismiss:(reason?: any) => {};
    public result: ng.IPromise<any>;
    public opened: ng.IPromise<any>;
    public rendered: ng.IPromise<any>;
    public closed: ng.IPromise<any>;

    private resolve: any;
    private dropzone: Dropzone;

    constructor(private $timeout: any,
                private $q: ng.IQService,
                private $scope: any,
                private toastr: any,
                private fileResource: IFileResource) {
    }

    save() {
        this.close();
    }

    cancel() {
        this.dismiss();
    }

    $onInit() {
        let self: UploadDocumentModalController = this;

        this.$timeout(() => {
            let options: any = {
                autoProcessQueue: true,
                withCredentials: true,
                parallelUploads: 1,
                paramName: "file",
                acceptedFiles: ".pdf,.doc,.docx",
                url: self.fileResource.getUploadUrl(self.resolve.documentID),
                maxFiles: 1,
                init: function() {
                    this.on("error", function(file, errorMessage) {
                        self.toastr.warning(errorMessage.message ? errorMessage.message : errorMessage, 'Ошибка');
                        this.removeFile(file);
                    });
                    this.on("maxfilesexceeded", function(file, errorMessage) {
                        self.toastr.warning('Можно загрузить всего один файл.', 'Ошибка');
                        this.removeFile(file);
                    });
                    this.on("success", function() {
                        self.save();
                    });
                }
            };

            self.dropzone = new Dropzone("#upload-scan-dropzone", options);
        });
    }

}

angular.module('app').component('uploadDocumentModal', new UploadDocumentModalComponent());
