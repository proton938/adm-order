class CardViewComponent {
    public bindings: any;
    public controller: any;
    public templateUrl: string;

    constructor() {
        this.controller = CardViewController;
        this.templateUrl = 'app/components/adm-order/order/card/card-view.html';
        this.bindings = {
            orderPromise: "<",
            edit: '&'
        };
    }
}

class CardViewController {
    static $inject = [
    	'$q', 
    	'$stateParams', 
    	'$uibModal', 
    	'fileResource', 
    	'orderResource',
    	'$state', 
    	'alertService', 
    	'prAuthorizationService',
        'extDocumentService',
    	'$rootScope', 
    	'toastr',
        'nsiRestService',
        'activityProcessHistoryManager',
        'activityRestService'
	];

    private loadingOrderStatus: LoadingStatus;

    private orderPromise: ng.IPromise<Order>;
    private order: Order;
    private orderType: RDType;
    private isSendDisabled: boolean;
    private isUploadDisabled: boolean;
    private modal: any;
    private cancelingDocument: Order;
    private extDocLink: string;

	private hasAccessRight: boolean = true;
    private isUpdating: boolean = false;
    private isSending: boolean = false;
    private isDeleted: boolean = false;
    
    private canDeleteFile: boolean = false;
    private canDelete: boolean = false;

    public edit: (val: any) => {};

    constructor(private $q: ng.IQService,
                private $stateParams: any,
                private $uibModal: any,
                private fileResource: IFileResource,
                private orderResource: IOrderResource,
                private $state: any,
                private alertService: oasiWidgets.IAlertService,
                private authorizationService: IAuthorizationService,
                private extDocumentService: IExtDocumentService,
                private $rootScope: IScope,
                private toastr: Toastr,
                private nsiRestService: oasiNsiRest.NsiRestService,
                private activityProcessHistoryManager: oasiBpmHistory.IProcessHistoryManager,
                private activityRestService: oasiBpmRest.ActivityRestService) {
    }

    $onInit() {
        this.loadingOrderStatus = LoadingStatus.LOADING;

        this.orderPromise.then((order: Order) => {
            this.order = order;

            if (_.isEmpty(this.order.canceledDocument)) {
                delete this.order.canceledDocument;
            }

            this.isSendDisabled = order.documentStatusCode !== 'REGISTERSCAN';

            this.isUploadDisabled = !order.documentID || !order.documentNumber;

            this.isDeleted = order.documentStatusCode === "DELETED";

            this.canDeleteFile = this.authorizationService.check('OASI_ADMORDER_FILE_DELETEBUTTON');
            return this.nsiRestService.get('RD_TYPES');
        }).then((orderTypes: RDType[]) => {
            this.orderType = _.find(orderTypes, (type: RDType) => type.DOC_CODE === this.order.documentTypeCode);

            if (this.orderType.DOC_ACCESS_EI) {
                this.hasAccessRight = this.authorizationService.check('OASI_ADMORDER_CARD_EI');
            }
            return this.loadCancelingDocument();
        }).then(()=> {
            return this.activityRestService.queryTasks({
                active: true,
                processInstanceVariables: [{
                    name: "EntityIdVar",
                    value: this.order.documentID,
                    operation: "equals",
                    type: "string"
                }]
            })
        }).then((tasksStatus) => {
            let editDoc  = _.some(tasksStatus.data, x => {
                console.log(x);
                return x.formKey ==="EditDoc";
            });

            this.canDelete = this.order.documentStatusCode === "PROJECT" || (this.order.documentStatusCode === "APPROVAL" && editDoc);
            return this.extDocumentService.getLink(this.order);
        }).then((link) => {
            this.extDocLink = link;
            return true;
        }).then(() => {
            this.loadingOrderStatus = LoadingStatus.SUCCESS;
        }).catch(() => {
            this.loadingOrderStatus = LoadingStatus.ERROR;
        })
    }

    loadCancelingDocument(): ng.IPromise<any> {
        if (this.order.documentStatusCode === OrderStatus.CANCELED) {
            const deferred = this.$q.defer();
            this.orderResource.getCancelingDocumentId({id: this.order.documentID}).$promise.then((idWrapper: IdWrapper) => {
                return idWrapper && idWrapper.id ? this.orderResource.getById({id: idWrapper.id}).$promise : this.$q.resolve(null);
            }).then((order) => {
                this.cancelingDocument = order;
                return true;
            }).then(() => {
                deferred.resolve();
            }).catch(() => {
                deferred.reject();
            });
            return deferred.promise;
        } else {
            return this.$q.resolve();
        }

    }

    editCard() {
        this.edit({val: true});
    }

    deleteCard() {
        let self: CardViewController = this;

        self.isUpdating = true;

        self.alertService
            .confirm({
                message: 'После удаления документ будет невозможно востановить',
                okButtonText: 'Удалить',
                type: 'danger'
            })
            .then(() => {
                _.extend(self.order, {
                    documentStatusCode: 'DELETED',
                    documentStatusValue: 'Удален',
                    documentStatusColor: 'danger'
                });

                return self.updateDocument(self.order);
            })
            .then(() => {
                return self.activityProcessHistoryManager.deleteProcess(self.order.bpmProcess.bpmProcessId);
            })
            .then(()=>{
                this.toastr.success("Документ удален");
                this.$state.reload();
            }).catch(() => {
                this.isUpdating = false;
            });
    }

    updateDocument(document: Order): ng.IPromise<boolean> {
        let id: string = document.documentID;
        let deferred: ng.IDeferred<boolean> = this.$q.defer();

        this.orderResource.update({id: id}, {
            document
        }, () => {
            this.$rootScope.$emit('updateCounters');
            deferred.resolve(true);
        });

        return deferred.promise;
    }

    openUploadDocument(ignoreExistedFiles: boolean = false) {
        let self: CardViewController = this;

        if (!ignoreExistedFiles && _.size(self.order.files)) {
            self.alertService
                .confirm({
                    message: 'Для добавления нового файла, сначала необходимо удалить уже существующий файл',
                    okButtonText: 'Удалить',
                    type: 'danger'
                })
                .then(() => {
                    this.orderResource
                        .deleteFile({
                            id: this.order.documentID,
                            fileGuid: self.order.files[0].fileID
                        })
                        .$promise
                        .then(() => {
                            self.$rootScope.$emit('updateCounters');
                            self.openUploadDocument(true);
                        })
                });

            return;
        }

        this.$uibModal
            .open({
                component: 'upload-document-modal',
                resolve: {
                    documentID: () => {
                        return self.order.documentID
                    },
                },
                size: 'lg'
            })
            .result
            .then(() => {
                self.$rootScope.$emit('updateCounters');
                self.$state.reload();
            });
    }

    openServiceInformation() {
        let self: CardViewController = this;

        let controller = function ($uibModalInstance, $scope) {
            $scope.order = self.order;
            $scope.cancel = function () {
                $uibModalInstance.dismiss();
            };

            return {};
        };

        controller.$inject = ['$uibModalInstance', '$scope'];

        self.modal = this.$uibModal.open({
            templateUrl: 'app/components/adm-order/order/card/service-information-modal.html',
            controller: controller,
            size: 'lg'
        });
    }

    sendMails() {
        this.isUpdating = true;
        this.isSending = true;

        this.orderResource
            .mail({
                id: this.order.documentID,
            })
            .$promise
            .then(() => {
                this.$rootScope.$emit('updateCounters');
                this.toastr.success("Рассылка успешно осуществлена");
                this.$state.reload();
            })
            .catch(() => {
                this.toastr.warning("Рассылка завершилась с ошибкой");
                this.$state.reload();
            }).catch(() => {
                this.isUpdating = false;
                this.isSending = false;
        });
    }

    deleteDocument(fileInfo: any) {
        let self: CardViewController = this;

        self.isUpdating = true;

        this.orderResource .deleteFile({
            id: this.order.documentID,
            fileGuid: fileInfo.idFile
        }).$promise.then(() => {
            this.$rootScope.$emit('updateCounters');
            self.$state.reload();
            this.isUpdating = false;
        }, () => {
            this.isUpdating = false;
        });

    }
}

angular.module('app').component('cardView', new CardViewComponent());