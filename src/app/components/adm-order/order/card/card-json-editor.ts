const docDatesToDateString = [
    'documentDate',
    'documentExpireDate'
];

const docDatesToDateTimeStringWithoutHourOffset = [
    'createDateTime',
    'mailDateTime'
];

class CardJsonEditorController {
    static $inject = [
        '$state',
        'toastr',
        '$q',
        'prAuthorizationService',
        '$stateParams',
        'orderResource'
    ];

    private orderPromise: ng.IPromise<Order>;
    private order: Order;
    private orderPrev: any;
    private orderNew: any;
    private jsonEditorOptions: {
        mode: string
    };

    private loadingStatus: LoadingStatus = LoadingStatus.LOADING;

    constructor(
        private $state: ng.ui.IStateService,
        private toastr: Toastr,
        private $q: ng.IQService,
        private authorizationService: IAuthorizationService,
        private $stateParams: ng.ui.IStateParamsService,
        private orderResource: IOrderResource) {
    }

    $onInit() {
        this.orderPromise.then((order: Order) => {
            this.order = order;
            this.orderPrev = angular.copy(this.order);
            this.preprocessDates(this.orderPrev);
            this.orderNew = angular.copy(this.orderPrev);
            this.jsonEditorOptions = {
                mode: 'tree'
            };
            this.loadingStatus = LoadingStatus.SUCCESS;
        }).catch(error => {
            console.log(error);
            this.loadingStatus = LoadingStatus.ERROR;
        });
    }

    save() {
        if (angular.equals(this.orderPrev, this.orderNew)) {
            this.toastr.warning('Нет изменений в JSON.');
            return;
        }
        this.orderResource.update({id: this.order.documentID}, { document: this.orderNew }).$promise.then(() => {
            this.toastr.success('Документ сохранен.');
            this.$state.reload();
        }, () => {
            this.toastr.error('Ошибка при сохранении документа.');
        });
    }

    toDateTimeString(date, hasHourOffset) {
        const dd = date.getDate() > 9 ? date.getDate() : '0' + date.getDate();
        const month = date.getMonth() + 1;
        const MM = month > 9 ? month : '0' + month;

        return `${date.getFullYear()}-${MM}-${dd}T${date.toLocaleTimeString('en-GB', {timeZone: 'UTC'}).split(' ')[0]}${hasHourOffset ? 'Z' : ''}`;
    }

    toDateString(date) {
        const dd = date.getDate() > 9 ? date.getDate() : '0' + date.getDate();
        const month = date.getMonth() + 1;
        const MM = month > 9 ? month : '0' + month;

        return `${date.getFullYear()}-${MM}-${dd}`;
    }

    preprocessDates(obj) {
        for (let key in obj) {
            if (obj.hasOwnProperty(key)) {
                if (_.isDate(obj[key])) {
                    if (docDatesToDateString.indexOf(key) !== -1) {
                        obj[key] = this.toDateString(obj[key]);
                    }
                    else if (docDatesToDateTimeStringWithoutHourOffset.indexOf(key) !== -1) {
                        obj[key] = this.toDateTimeString(obj[key], false);
                    }
                    else {
                        obj[key] = this.toDateTimeString(obj[key], true);
                    }
                }
                else if (typeof obj[key] === 'object') {
                    this.preprocessDates(obj[key]);
                }
            }
        }
    }
}

angular.module('app').component('cardJsonEditor', {
    controller: CardJsonEditorController,
    templateUrl: 'app/components/adm-order/order/card/card-json-editor.html',
    bindings: {
        orderPromise: "<",
    }
});
