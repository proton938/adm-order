class CardRelatedController {
    static $inject = [
    	'toastr', 
    	'$q', 
    	'prAuthorizationService',
    	'$stateParams',
        'nsiRestService',
        'orderResource'
	];
	
    private orderPromise: ng.IPromise<Order>;
    private order: Order;
    private relatedDocuments: any = [{
        documentDate: new Date(),
        documentNumber: 23,
        documentStatusCode: 'CANCELED',
        documentTypeValue: 'Приказ председателя Москомархитектуры',
        documentContent: 'Об утверждении границ чего-то там длинные текст короче тупо потестить чтобы две и более строчек адекватненько смотрелись'
    }];
    private rdStatus: any;
    private rdTypes: any;
    private rdLinkTypes: any;
    private docAccess: any;
    private fileAccess: any;

   private loadingStatus: LoadingStatus = LoadingStatus.LOADING;

    constructor(
        private toastr: Toastr,
        private $q: ng.IQService,
        private authorizationService: IAuthorizationService,
        private $stateParams: ng.ui.IStateParamsService,
        private nsiRestService: oasiNsiRest.NsiRestService,
        private orderResource: IOrderResource) {
    }

    $onInit() {
        this.loadDictionaries().then(() => {
            this.orderPromise.then((order: Order) => {
                this.order = order;
                return this.getRelatedDocuments();
            }).then(orders => {
                this.relatedDocuments = orders.sort((a, b) => {
                    if (a.documentDate > b.documentDate) return 1;
                    else if (a.documentDate < b.documentDate) return 0;
                    return Number(a.documentNumber > b.documentNumber);
                });
                this.loadingStatus = LoadingStatus.SUCCESS;
            });
        });
    }

    getRelatedDocuments(): ng.IPromise<Order[]> {
        return this.orderResource.getRelated({id: this.order.documentID}).$promise.then((ids) => {
            let related = [];
            for (let doc in ids) {
                if (ids.hasOwnProperty(doc) && doc[0] !== '$') {
                    related.push(doc);
                }
            }
            return this.$q.all(
                _.map(related, id => {
                    return this.orderResource.getById({id: id}).$promise.then((res: any) => {
                        res.relationType = this.rdLinkTypes[ids[id]];
                        return res;
                    })
                })
            )
        })
    }

    loadDictionaries() {
        return this.$q.all([
            this.nsiRestService.get('RD_STATUS'),
            this.nsiRestService.get('RD_TYPES'),
            this.nsiRestService.get('RDLinkType')
        ]).then((values: any[]) => {
            this.rdStatus = _.reduce(values[0], (obj, status: RDStatus) => {
                obj[status.STATUS_CODE] = status.statusAnalytic;
                return obj;
            }, {});
            this.rdTypes = values[1];
            this.docAccess = _.reduce(values[1], (obj, type: RDType) => {
                obj[type.DOC_CODE] = type.DOC_ACCESS_ACTION;
                return obj;
            }, {});
            this.fileAccess = _.reduce(values[1], (obj, type: RDType) => {
                obj[type.DOC_CODE] = type.DOC_ACCESS_EI;
                return obj;
            }, {});
            this.rdLinkTypes = _.reduce(values[2], (obj, type: any) => {
                obj[type.LinkTypeCode] = type.LinkTypeName;
                return obj;
            }, {});
        });
    }

}

angular.module('app').component('cardRelated', {
    controller: CardRelatedController,
    templateUrl: 'app/components/adm-order/order/card/card-related.html',
    bindings: {
        orderPromise: "<",
    }    
});