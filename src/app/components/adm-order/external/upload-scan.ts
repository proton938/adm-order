class UploadScanComponent implements ng.IComponentOptions {
    public bindings: any;
    public controller: any;
    public templateUrl: string;

    constructor() {
        this.controller = UploadScanController;
        this.templateUrl = 'app/components/adm-order/external/upload-scan.html';
        this.bindings = {};
    }
}

class UploadScanController {
    static $inject = ['$timeout', '$q', '$scope', '$stateParams', '$window', 'toastr', 'fileResource'];

    private dropzone: Dropzone;

    constructor(private $timeout: any,
                private $q: ng.IQService,
                private $scope: any,
                private $stateParams: any,
                private $window: any,
                private toastr: any,
                private fileResource: IFileResource) {
    }

    $onInit() {
        let self: UploadScanController = this;

        this.$timeout(() => {
            let options: any = {
                autoProcessQueue: true,
                withCredentials: true,
                parallelUploads: 1,
                paramName: "file",
                acceptedFiles: ".pdf,.doc,.docx",
                url: self.fileResource.getUploadUrl(self.$stateParams.documentID),
                maxFiles: 1,
                init: function() {
                    this.on("error", function(file, errorMessage) {
                        self.toastr.warning(errorMessage.message ? errorMessage.message : errorMessage, 'Ошибка');
                        this.removeFile(file);
                    });
                    this.on("maxfilesexceeded", function(file, errorMessage) {
                        self.toastr.warning('Можно загрузить всего один файл.', 'Ошибка');
                        this.removeFile(file);
                    });
                    this.on("success", function() {
                        self.$window.location.href = self.$window.location.protocol + '//' + self.$window.location.host + '/oasi/#/app/showcase/admorder?activeTab=scanning';
                    });
                }
            };

            self.dropzone = new Dropzone("#upload-scan-dropzone", options);
        });
    }

}

angular.module('app').component('uploadScan', new UploadScanComponent());
