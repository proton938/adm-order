class AttachOriginalsComponent implements ng.IComponentOptions {
    public bindings: any;
    public controller: any;
    public templateUrl: string;

    constructor() {
        this.controller = AttachOriginalsController;
        this.templateUrl = 'app/components/adm-order/external/attach-originals.html';
        this.bindings = {
        };
    }
}

class AttachOriginalsController {
    static $inject = ['$timeout', '$window'];

    constructor(private $timeout: any, private $window: any) {
    }

    $onInit() {
    }

    done() {
        this.$timeout(() => {
            this.$window.location.href = this.$window.location.protocol + '//' + this.$window.location.host + '/oasi/#/app/showcase/admorder?activeTab=scanning';
        }, 4000)
    }

}

angular.module('app').component('attachOriginals', new AttachOriginalsComponent());
