class OrderInitiateComponent implements ng.IComponentOptions {
    public bindings: any;
    public controller: any;
    public templateUrl: string;

    constructor() {
        this.controller = OrderInitiateController;
        this.templateUrl = 'app/components/adm-order/external/order-initiate.html';
        this.bindings = {};
    }
}

class OrderInitiateController {
    static $inject = [
        '$q',
        '$state',
        '$window',
        'prSession',
        'session',
        'orderResource',
        'orderTaskService',
        'activityRestService',
        'nsiRestService'
    ];

    private loadingDictStatus: LoadingStatus;
    private orderTypes: RDType[];
    private themes: RDTheme[];
    private availableYears: number[];
    private availableThemes: RDTheme[] = [];

    private saving: boolean = false;
    private order: Order;

    constructor(private $q: ng.IQService,
                private $state: any,
                private $window: any,
                private prSession: ISessionStorage,
                private session: oasiSecurity.ISessionStorage,
                private orderResource: IOrderResource,
                private orderTaskService: IOrderTaskService,
                private activityRestService: oasiBpmRest.ActivityRestService,
                private nsiRestService: oasiNsiRest.NsiRestService) {
    }

    $onInit() {
        this.order = new Order();
        this.order.documentYear = new Date().getFullYear();
        if (this.$state.params.linkedDocumentID || this.$state.params.linkedDocumentType) {
            this.order.linkedDocument = [{
                linkedDocumentID: this.$state.params.linkedDocumentID,
                linkedDocumentType: this.$state.params.linkedDocumentType
            }];
        }
        this.loadDictionaries();
    }

    loadDictionaries() {
        this.availableYears = _.range(2016, 2028);
        this.loadingDictStatus = LoadingStatus.LOADING;
        this.$q.all([
            this.nsiRestService.get('RD_TYPES'),
            this.nsiRestService.get('RD_THEME')
        ]).then((values: any[]) => {
            this.loadingDictStatus = LoadingStatus.SUCCESS;
            this.orderTypes = values[0].filter(type => this.prSession.rdTypes().indexOf(type.DOC_CODE) > -1);
            this.themes = values[1];
            this.documentTypeChanged();
        });
    }

    save() {
        this.saving = true;
        this.orderResource.create({document: this.order}, (data: OrderCreateResult) => {
            this.orderResource.getById({id: data.id}, (order: Order) => {
                this.orderTaskService.getProcessDefinition('rd_coordinationOrders_ID')
                    .then((process: oasiBpmRest.IProcessDefinition) => {
                        return this.orderTaskService.initProcess(process.id, this.getInitProcessProperties(order));
                    })
                    .then((id) => {
                        order.bpmProcess = new OrderProcess();
                        order.bpmProcess.bpmProcessId = id.toString();
                        return this.updateOrder(order)
                            .then(() => {
                                return this.getTaskId(id);
                            })
                    })
                    .then((taskId) => {
                        this.saving = false;
                        this.$state.go('app.adm-order.process.PrepareRD', {
                            documentID: order.documentID,
                            taskId: taskId
                        });
                    });
            });
        });
    }

    updateOrder(order: Order) {
        let deferred = this.$q.defer();
        this.orderResource.update({id: order.documentID}, {
            document: order
        }, deferred.resolve, deferred.reject);
        return deferred.promise;
    }

    getTaskId(processId: any) {
        let deferred = this.$q.defer();
        this.activityRestService.getTasks({
            processInstanceId: processId
        }).then((result: any) => {
            deferred.resolve(result.data[0].id)
        },  deferred.reject);
        return deferred.promise;
    }

    documentTypeChanged() {
        this.order.documentThemeCode = '';
        if (this.order.documentTypeCode) {
            this.availableThemes = _.filter(this.themes, th => {
                return th.DOC_CODE === this.order.documentTypeCode;
            })
        } else {
            this.availableThemes = [];
        }
    }

    themeChanged() {
        if (this.order.documentThemeCode) {
            this.order.documentThemeValue = _.find(this.availableThemes, th => {
                return th.THEME_CODE === this.order.documentThemeCode;
            }).THEME_NAME;
        } else {
            delete this.order.documentThemeValue;
        }
    }

    getInitProcessProperties(order: Order): oasiBpmRest.ITaskVariable[] {
        const rdType = _.find(this.orderTypes, _ => {
            return _.DOC_CODE === order.documentTypeCode;
        });
        return [{
            "name": "EntityIdVar",
            "value": order.documentID
        },{
            "name": "EntityDescriptionVar",
            "value": order.documentTypeValue + " " + order.documentContent
        }, {
            "name": "ContractorVar",
            "value": this.session.login()
        }, {
            "name": "reg_group_var",
            "value": rdType.DOC_ACCESS_ACTION
        }, {
            "name": "scan_group_var",
            "value": rdType.DOC_ACCESS_ACTION
        }];
    }

    cancel() {
        this.$window.history.back();
    }
}

angular.module('app').component('orderInitiate', new OrderInitiateComponent());
