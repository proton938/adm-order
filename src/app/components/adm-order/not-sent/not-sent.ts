class NotSentComponent {
    public bindings: any;
    public require: any;
    public controller: any;
    public templateUrl: string;

    constructor() {
        this.controller = NotSentController;
        this.templateUrl = 'app/components/adm-order/not-sent/not-sent.html';
        this.bindings = {};
        // this.require = {
        //     parent: '^orders'
        // };
    }
}

class NotSentController {
    static $inject = [];
}

angular.module('app').component('notSent', new NotSentComponent());
