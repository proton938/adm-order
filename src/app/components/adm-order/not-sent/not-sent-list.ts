// import Sorting = orders.Sorting;

class NotSentListComponent {
    public bindings: any;
    public require: any;
    public controller: any;
    public templateUrl: string;

    constructor() {
        this.controller = NotSentListController;
        this.templateUrl = 'app/components/adm-order/not-sent/not-sent-list.html';
        this.bindings = {};
        this.require = {
            parent: '^notSent'
        };
    }
}

class NotSentListController {
    static $inject = ['orderResource', 'orderByFilter', '$q', 'toastr',
        '$rootScope', 'prAuthorizationService', 'fileResource', 'nsiResource'];

    private sorting: Sorting;
    private sortingAsc: boolean;

    private loadingAppStatus: LoadingStatus;
    private searchResult: OrderSearchResult;
    private hasViewPermission: boolean;

    constructor(private orderResource: IOrderResource,
                private orderBy: ng.IFilterOrderBy,
                private $q: ng.IQService,
                private toastr: Toastr,
                private $rootScope: IScope,
                private authorizationService: IAuthorizationService,
                private fileResource: IFileResource,
                private nsiResource: INsiResource) {
    }

    reload() {
        let self: NotSentListController = this;

        let params: any = {
            sort: this.sorting + ',' + (this.sortingAsc ? 'asc' : 'desc'),
            page: this.searchResult.pageNumber - 1,
            size: this.searchResult.pageSize
        };

        self.orderResource.notSent(params, function (response: OrderSearchResult) {
            let sortedOrders: Order[] = response.content;

            sortedOrders.forEach((order: any) => {
                if (order.files && order.files.length > 1) {
                    let urls = '';

                    order.files.forEach((file) => {
                        urls += '<a href="#" class="text-default" title="{{file.fileName}}">' + file.fileName + '</a><br>';
                    });
                    order.fileUrls = urls;
                }
            });

            response.pageNumber++;

            self.searchResult = response;
            self.loadingAppStatus = LoadingStatus.SUCCESS;
        }, function (error) {
            self.loadingAppStatus = LoadingStatus.ERROR;
        });
    }

    $onInit() {
        this.sorting = Sorting.DOCUMENT_DATE;
        this.sortingAsc = false;

        this.searchResult = {
            $promise: null,
            content: [],
            totalElements: 0,
            totalPages: 0,
            pageNumber: 1,
            pageSize: 10
        };
        this.hasViewPermission = this.authorizationService.check('OASI_ADMORDER_CARD');
    }

    isSorted(field: Sorting, currentSorting: Sorting): boolean {
        return field == currentSorting;
    }

    switchSorting(sorting: Sorting) {
        if (this.sorting === sorting) {
            this.sortingAsc = !this.sortingAsc;
        } else {
            this.sorting = sorting;
        }

        this.reload();
    }

    sendMails(documentID) {
        this.loadingAppStatus = LoadingStatus.LOADING;
        this.orderResource
            .mail({
                id: documentID,
            })
            .$promise
            .then(() => {
                this.$rootScope.$emit('updateCounters');
                this.toastr.success("Рассылка успешно осуществлена");
                this.reload();
            })
            .catch(() => {
                this.toastr.warning("Рассылка завершилась с ошибкой");
                this.reload();
            });
    }

    getFileLink(id) {
        return this.fileResource.getDownloadUrl(id);
    }
}

angular.module('app').component('notSentList', new NotSentListComponent());
