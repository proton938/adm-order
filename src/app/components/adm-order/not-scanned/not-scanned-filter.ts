class NotScannedFilterComponent implements ng.IComponentOptions {
    public bindings: any;
    public require: any;
    public controller: any;
    public templateUrl: string;

    constructor() {
        this.controller = NotScannedFilterController;
        this.templateUrl = 'app/components/adm-order/not-scanned/not-scanned-filter.html';
        this.bindings = {
            applyFilter: '&',
            clearFilter: '&'
        };
        this.require = {
            parent: '^notScanned'
        };
    }
}

class NotScannedFilterController implements INotScannedOrdersFilterController {
    static $inject = ['$q'];

    private applyFilter: (value: any) => {};
    private clearFilter: () => {};
    private searchFilter: orders.ExtSearchMiniFilter;

    private parent: NotScannedController;


    constructor(private $q: ng.IQService) {
    }

    $onInit() {
        this.parent.setFilterController(this);
    }

    setFilter(extSearchFilter:orders.ExtSearchMiniFilter) {
        this.searchFilter = angular.copy(extSearchFilter);
    }

    apply() {
        this.applyFilter({
            value: this.searchFilter
        });
    }

    cancel() {
        this.clearFilter();
    }
}

angular.module('app').component('notScannedFilter', new NotScannedFilterComponent());
