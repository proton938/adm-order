class NotScannedAttachModalComponent implements ng.IComponentOptions {
    public bindings: any;
    public controller: any;
    public templateUrl: string;

    constructor() {
        this.controller = NotScannedAttachModalController;
        this.templateUrl = 'app/components/adm-order/not-scanned/not-scanned-attach-modal.html';
        this.bindings = {
            resolve: '<',
            close: '&',
            dismiss: '&'
        };
    }
}

class NotScannedAttachModalController implements ng.ui.bootstrap.IModalServiceInstance {
    static $inject = [];

    public close: (result?: any) => {};
    public dismiss: (reason?: any) => {};
    public result: ng.IPromise<any>;
    public opened: ng.IPromise<any>;
    public rendered: ng.IPromise<any>;
    public closed: ng.IPromise<any>;

    private uploaded: boolean = false;

    constructor() {
    }

    save() {
    }

    cancel() {
        this.dismiss();
    }

    done() {
        this.uploaded = true;
    }
    
    $onInit() {
    }

}

angular.module('app').component('notScannedAttachModal', new NotScannedAttachModalComponent());
