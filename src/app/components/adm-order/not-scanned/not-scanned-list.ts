// import Sorting = orders.Sorting;

class NotScannedListComponent {
    public bindings: any;
    public require: any;
    public controller: any;
    public templateUrl: string;

    constructor() {
        this.controller = NotScannedListController;
        this.templateUrl = 'app/components/adm-order/not-scanned/not-scanned-list.html';
        this.bindings = {};
        this.require = {
            parent: '^notScanned'
        };
    }
}

class NotScannedListController {
    static $inject = ['orderResource', 'orderByFilter',
        '$q', '$uibModal', 'toastr',
        '$rootScope', 'prAuthorizationService'];

    private sorting: Sorting;
    private sortingAsc: boolean;

    private loadingAppStatus: LoadingStatus;
    private searchResult: OrderSearchResult;

    private filter: orders.ExtSearchMiniFilter;

    private parent: NotScannedController;
    private hasViewPermission: boolean;

    constructor(private orderResource: IOrderResource,
                private orderBy: ng.IFilterOrderBy,
                private $q: ng.IQService,
                private $uibModal: any,
                private toastr: Toastr,
                private $rootScope: IScope,
                private authorizationService: IAuthorizationService) {
    }

    reload(filter: orders.ExtSearchMiniFilter) {
        let self: NotScannedListController = this;
        this.loadingAppStatus = LoadingStatus.LOADING;

        if (filter) {
            this.filter = filter;
        }

        let params: any = {
            sort: this.sorting + ',' + (this.sortingAsc ? 'asc' : 'desc'),
            page: this.searchResult.pageNumber - 1,
            size: this.searchResult.pageSize
        };

        self.orderResource.notScanned(params, this.filter,
            function (response: OrderSearchResult) {
                response.pageNumber++;

                self.searchResult = response;
                self.loadingAppStatus = LoadingStatus.SUCCESS;

            }, function (error) {
                self.loadingAppStatus = LoadingStatus.ERROR;
            });
    }

    $onInit() {
        this.sorting = Sorting.DOCUMENT_DATE;
        this.sortingAsc = false;
        this.searchResult = {
            $promise: null,
            content: [],
            totalElements: 0,
            totalPages: 0,
            pageNumber: 1,
            pageSize: 10
        };

        this.hasViewPermission = this.authorizationService.check('OASI_ADMORDER_CARD');
        this.parent.setListController(this);
    }

    openUploadDocument(documentID: number) {
        this.$uibModal
            .open({
                component: 'upload-document-modal',
                resolve: {
                    documentID: () => {
                        return documentID
                    },
                },
                size: 'lg'
            })
            .result
            .then(() => {
                this.$rootScope.$emit('updateCounters');
                this.toastr.success('Данные сохранены');
                this.reload(this.filter);
            });
    }

    isSorted(field: Sorting, currentSorting: Sorting): boolean {
        return field == currentSorting;
    }

    switchSorting(sorting: Sorting) {
        if (this.sorting === sorting) {
            this.sortingAsc = !this.sortingAsc;
        } else {
            this.sorting = sorting;
        }

        this.reload(this.filter);
    }
}

angular.module('app').component('notScannedList', new NotScannedListComponent());
