class NotScannedComponent {
    public bindings: any;
    public require: any;
    public controller: any;
    public templateUrl: string;

    constructor() {
        this.controller = NotScannedController;
        this.templateUrl = 'app/components/adm-order/not-scanned/not-scanned.html';
        this.bindings = {};
        // this.require = {
        //     parent: '^orders'
        // };
    }
}

class NotScannedController {
    static $inject = ['$rootScope', '$uibModal'];

    private viewExtSearch: boolean;

    private extSearchFilter: orders.ExtSearchMiniFilter;
    private filterController: INotScannedOrdersFilterController;
    private listController: IChildNotScannedOrdersController;

    constructor(private $rootScope: any, private $uibModal: any) {
    }    
    
    $onInit() {
        this.viewExtSearch = false;
        this.extSearchFilter = new orders.ExtSearchMiniFilter();
    }

    toggleViewExtSearch() {
        this.viewExtSearch = !this.viewExtSearch;
    }

    setFilterController(filterController: INotScannedOrdersFilterController) {
        this.filterController = filterController;
        this.filterController.setFilter(this.extSearchFilter);
    }

    setListController(listController: IChildNotScannedOrdersController) {
        this.listController = listController;
        this.listController.reload(this.extSearchFilter);
    }

    setFilter(filter: orders.ExtSearchMiniFilter) {
        _.extend(this.extSearchFilter, filter);

        console.log(this.extSearchFilter);

        this.filterController.setFilter(this.extSearchFilter);
        this.listController.reload(this.extSearchFilter);
    }

    clearFilter() {
        _.each(_.keys(this.extSearchFilter), (key) => {
            delete this.extSearchFilter[key];
        });
        this.filterController.setFilter(this.extSearchFilter);
        this.listController.reload(this.extSearchFilter);
    }

    attachOriginals() {
        let reload = () => {
            this.$rootScope.$emit('updateCounters');
            this.listController.reload(this.extSearchFilter);            
        }
        this.$uibModal.open({
            component: 'not-scanned-attach-modal',
            size: 'lg'
        }).result.then(reload, reload);
    }

}

interface INotScannedOrdersFilterController {
    setFilter(extSearchFilter: orders.ExtSearchMiniFilter): void;
}

interface IChildNotScannedOrdersController {
    reload(extSearchFilter?: orders.ExtSearchMiniFilter): void;
}


angular.module('app').component('notScanned', new NotScannedComponent());
