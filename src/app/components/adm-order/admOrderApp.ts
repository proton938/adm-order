class AdmOrderAppComponent {
    public bindings: any;
    public controller: any;
    public templateUrl: string;

    constructor() {
        this.controller = AdmOrderAppController;
        this.templateUrl = 'app/components/adm-order/admOrderApp.html';
        this.bindings = {};
    }
}

class AdmOrderAppController {
}

angular.module('app').component('admOrderApp', new AdmOrderAppComponent());