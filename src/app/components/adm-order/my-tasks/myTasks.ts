class MyTasksController {
    static $inject = [
        'toastr', 
        'session',
        '$rootScope',
        'myTasksService',
        'orderResource',
        '$uibModal', 
        '$state', 
        '$q'
    ];
    
    private tasks: IOrderTask[] = [];
    private numberOfTasks: number;
    private tasksLoadStatus: LoadingStatus = LoadingStatus.LOADING;
    
    sortName: string = 'dueDate';
    sortReverse: boolean = false;
    currentDate: any = new Date();
    currentDateTime: any;    
    
    viewType: ViewType;

    constructor(
        private toastr: Toastr,
        private session: oasiSecurity.ISessionStorage,
        private $rootScope: ng.IRootScopeService,
        private myTasksService: MyTasksService,
        private orderResource: IOrderResource,
        private $uibModal: angular.ui.bootstrap.IModalService,
        private $state: ng.ui.IStateService,
        private $q: ng.IQService    
    ) {
        this.viewType = new ViewType('LIST',
            [new ViewValue('LIST', 'списком', 'fa fa-list'), new ViewValue('GROUP', 'по задачам', 'fa fa-indent')]);
    }

    $onInit() {
        this.currentDateTime = this.currentDate.getTime();
        this.$rootScope['title'] = "Мои задачи";  
        
        let allTasks: IOrderTask[] = [];
        this.myTasksService.getAllTaskWithDelegated(this.session.login()).then(tasks => {
            this.tasks = tasks;
            this.numberOfTasks = this.tasks.length;
            this.$rootScope.$broadcast('updateMyTasksCount', {
            	count: this.numberOfTasks
            })
            if (this.tasks.length !== 0) {
            	let promises = [];
            	_.forEach(this.tasks, t => {
            		let documentId = this.getDocumentId(t);
            		let promise = this.orderResource.getById({id: documentId}).$promise;
            		promises.push(promise);
            		promise.then((order) => {
            			let task = _.find(this.tasks, t => {
            				return this.getDocumentId(t) === order.documentID;
            			})
            			task.documentTypeValue = order.documentTypeValue;
            			task.documentContent = order.documentContent;
            		})
            	})
            	this.$q.all(promises).then(() => {
            		this.tasksLoadStatus = LoadingStatus.SUCCESS;
            	}, () => {
            		this.tasksLoadStatus = LoadingStatus.SUCCESS;
            	})
            } else {
            	this.tasksLoadStatus = LoadingStatus.SUCCESS;
            }
        }).catch(error => {
            this.tasksLoadStatus = LoadingStatus.ERROR;
            console.error(error);
        });       
        
    }
    
    private goToTask(task: IOrderTask) {
        if (task.formKey) {
        	let stateName = 'app.adm-order.process.' + task.formKey;
            this.$state.go(stateName, {
            	formKey: task.formKey,
                documentID: this.getDocumentId(task),
                taskId: task.id
            });
        } else {
            this.toastr.error('Задача является автоматической!');
        }
    }  
    
	  sortBy(propName: string, ascOrder: boolean) {
	    let _ascOrder = ascOrder ? 'asc' : 'desc';
	    this.tasks = _.orderBy(this.tasks, [propName], [_ascOrder]);
	  }         
    
    getDocumentId(task: IOrderTask): string {
		if (task.variables && _.isArray(task.variables)) {
			let variables: any[] = <any[]> task.variables;
			let idVar = _.find(variables, variable => {
				return variable.name && variable.name === "EntityIdVar";
			})
			return idVar ? idVar.value : null;
		} else {
			return null;
		}    
    }

}

class MyTasksComponent {
    public bindings: any;
    public controller: any;
    public templateUrl: string;

    constructor() {
        this.controller = MyTasksController;
        this.templateUrl = 'app/components/adm-order/my-tasks/myTasks.html';
        this.bindings = {};
    }
}

angular.module('app').component('myTasks', new MyTasksComponent());
