class TaskCardController {
    static $inject = [];

    task: oasiBpmRest.ITask;
    selectHandler: any;
    currentDateTime: any;

    constructor() {
        this.currentDateTime = (new Date()).getTime();
    }

    openTask(task: oasiBpmRest.ITask) {
        this.selectHandler({value:task});
    }

    convertToTime(_date: any) {
        let date = new Date(_date);
        return date.getTime();
    }
}

class TaskCardComponent {
    public bindings: any;
    public controller: any;
    public templateUrl: string;

    constructor() {
        this.controller = TaskCardController;
        this.templateUrl = 'app/components/adm-order/my-tasks/task-card/taskCard.html';
        this.bindings = {
            task: "<",
            selectHandler: '&'
        };
    }
}

angular.module('app').component('taskCard', new TaskCardComponent());