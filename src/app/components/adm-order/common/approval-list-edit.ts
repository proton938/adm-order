class ApprovalListEditComponent {
    public bindings: any;
    public controller: any;
    public templateUrl: string;

    constructor() {
        this.controller = ApprovalListEditController;
        this.templateUrl = 'app/components/adm-order/common/approval-list-edit.html';
        this.bindings = {
        	order: '<',
        	taskInfo: '<',
			enableDoubleAgree: '<'
        };
    }
}

class ApprovalListEditController {
    static $inject = [
    	'$scope',
    	'ApprovalType',
		'nsiRestService',
		'nsiUserService'
	];

	private order: Order;
	private taskInfo: ITaskInfo;
	private enableDoubleAgree: boolean;

    private users: { [key: string] : User} = {};

	private approversFullList: OrderApprover[];
	private executivesFullList: OrderApprover[];
 	private approvers: OrderApprover[];
 	private executives: OrderApprover[];
 	private origApprovalTypes: ApprovalType[];
	private approvalTypes: ApprovalType[];
	private fullApprovalTypesWithTime: ApprovalType[];

    constructor(
    	private $scope: any,
    	private ApprovalType: any,
        private nsiRestService: oasiNsiRest.NsiRestService,
		private nsiUserService: INsiUserService
    ) {
    }

    $onInit() {
    	this.nsiRestService.get('RDApprovalDuration').then((approvalTypes: ApprovalType[]) => {
    		this.fullApprovalTypesWithTime = approvalTypes;
    		this.origApprovalTypes = [];
    		for (let i = 0; i < this.fullApprovalTypesWithTime.length; i++) {
    			if (!_.find(this.origApprovalTypes, type => type.approvalTypeCode === this.fullApprovalTypesWithTime[i].approvalTypeCode)) {
    				this.origApprovalTypes.push(this.fullApprovalTypesWithTime[i]);
				}
			}

    		this.nsiRestService.get('RD_TYPES').then((docTypes: RDType[]) => {
                const docAccessAction = _.find(docTypes, type => type.DOC_CODE === this.order.documentTypeCode).DOC_ACCESS_ACTION;
                this.nsiUserService.executives(docAccessAction).then((executives: User[]) => {
                	executives.forEach(user => this.users[user.accountName] = user);
                    this.executivesFullList = _.map(executives, (e) => {
                        return this.getOrderApprover(e);
                    });
                    if (this.enableDoubleAgree) {
                    	this.executives = this.executivesFullList;
					}
					else {
                        this.updateApproversAndExecutives();
					}
                });
                this.nsiUserService.approvers(docAccessAction).then((approvers: User[]) => {
                    approvers.forEach(user => this.users[user.accountName] = user);
                    this.approversFullList = _.map(approvers, (a) => {
                        return this.getOrderApprover(a);
                    });
                    if (this.enableDoubleAgree) {
                        this.approvers = this.approversFullList;
                    }
                    else {
                        this.updateApproversAndExecutives();
                    }
                });
            });

	    	this.order.approval = this.order.approval || new OrderApproval(1);
			_.each(this.order.approval.approvalCycle.agreed, (item, index) => {
				if (!item.approvalTime) {
			    	this.personSetTime(index);
		    	}
			});
			this.$scope.$watch(() => {
				return this.order.signingElectronically;
			}, () => {
				this.approvalTypes = _.clone(this.origApprovalTypes);
	    		if (!this.order.signingElectronically) {
	    			_.remove(this.approvalTypes, at => {
	    				return at.approvalTypeCode === this.ApprovalType.approval;
	    			});
				}
	    		this.sortApprovalsAndSetNumeration();
			});
            this.$scope.$watch(() => {
                return this.order.head;
            }, () => {
                this.sortApprovalsAndSetNumeration();
            })
    	});
    }

    updateApproversAndExecutives() {
    	if (this.enableDoubleAgree) {
            return;
		}
        if (this.approversFullList) {
        	this.approvers = _.filter(this.approversFullList, a => {
                return !_.find(this.order.approval.approvalCycle.agreed, (agreed) => {
                    return agreed.agreedBy && agreed.agreedBy.accountName === a.accountName;
                });
            });
        }

        if (this.executivesFullList) {
            this.executives = _.filter(this.executivesFullList, a => {
                return !_.find(this.order.approval.approvalCycle.agreed, (agreed) => {
                    return agreed.agreedBy && agreed.agreedBy.accountName === a.accountName;
                });
            });
        }
	}

    getOrderApprover(user: User): OrderApprover {
    	return {
    		post: user.post,
    		fioFull: user.fio,
    		accountName: user.accountName,
    		fioShort: user.fioShort,
    		iofShort: null,
    		phone: user.phoneNumber
    	}
    }

    setNumeration() {
    	_.each(this.order.approval.approvalCycle.agreed, (item, index) => {
    		item.approvalNum = "" + (index + 1);
    	})
    }

    sortApprovalsAndSetNumeration() {
        // const signingElectronically = this.isSigningElectronically();
        // const head = <User> this.order.head;
    	// this.order.approval.approvalCycle.agreed = _.sortBy(this.order.approval.approvalCycle.agreed, (a) => {
    	// 	if (a.approvalTypeCode === this.ApprovalType.approval) {
    	// 		return 1;
    	// 	} else if (!signingElectronically && head && a.agreedBy && a.agreedBy.accountName === head.accountName) {
		// 		return 2;
		// 	} else {
    	// 		return 0;
    	// 	}
    	// })
		this.setNumeration();
    }

    isSigningElectronically() {
    	return this.order.signingElectronically && this.order.signingElectronically.toString() === 'true';
	}

    swapApprovals(ind1: number, ind2: number) {
    	let agreed = this.order.approval.approvalCycle.agreed;
    	let app = agreed[ind1];
    	agreed[ind1] = agreed[ind2];
    	agreed[ind2] = app;
    	this.setNumeration();
    }

    delApproval(ind: number) {
    	this.order.approval.approvalCycle.agreed.splice(ind, 1);
        this.updateApproversAndExecutives();
    	this.setNumeration();
    	this.setHead();
    }

    initApprovalList() {
    	let historyLength = this.order.approvalHistory && this.order.approvalHistory.approvalCycle ?
    		this.order.approvalHistory.approvalCycle.length : 0;
		this.order.approval = new OrderApproval(historyLength + 1);
    }

    canAdd() {
    	let approval = this.order.approval;
    	if (approval && approval.approvalCycle && approval.approvalCycle.agreed) {
    		return !_.some(approval.approvalCycle.agreed, agreed => {
    			return !agreed.agreedBy || !agreed.approvalTypeCode
    		})
    	} else {
    		return true;
    	}
    }

    addApproval() {
    	if (!this.order.approval) {
    		this.initApprovalList()
    	}
    	let agreed = new OrderApprovalListItem();
    	let approvalType = _.find(this.approvalTypes, (at) => {
    		if (!this.order.systemCode) {
                return at.approvalTypeCode === this.ApprovalType.assent;
			}
            return at.approvalTypeCode === this.ApprovalType.agreed;
    	});
    	agreed.approvalType = approvalType.approvalType;
    	agreed.approvalTypeCode = approvalType.approvalTypeCode;
        this.order.approval.approvalCycle.agreed = this.order.approval.approvalCycle.agreed || [];
    	this.order.approval.approvalCycle.agreed.push(agreed);
    	this.sortApprovalsAndSetNumeration();
    }

    approvalTypeChanged(ind: number) {
    	if (!this.order.approval.approvalCycle.agreed[ind]) return;
    	let approvalType = _.find(this.approvalTypes, (at) => {
    		return at.approvalTypeCode === this.order.approval.approvalCycle.agreed[ind].approvalTypeCode
    	});
    	this.order.approval.approvalCycle.agreed[ind].approvalType = approvalType && approvalType.approvalType;
    	this.personSetTime(ind);
    	this.sortApprovalsAndSetNumeration();
    	this.setHead();
    }

    approvalChanged() {
    	this.updateApproversAndExecutives();
    	this.sortApprovalsAndSetNumeration();
    	this.setHead();
    }

    personSetTime(index) {
    	const agreed = this.order.approval.approvalCycle.agreed[index];
    	if (!agreed.agreedBy) return;
        let user = this.users[agreed.agreedBy.accountName];
        let department = user ? user.department : null;
    	let approvalType = _.find(this.fullApprovalTypesWithTime, type => type.approvalTypeCode === agreed.approvalTypeCode && type.department === department);
    	if (!approvalType) {
			approvalType = _.find(this.fullApprovalTypesWithTime, type => type.approvalTypeCode === agreed.approvalTypeCode
			&& !type.department);
		}
		this.order.approval.approvalCycle.agreed[index].approvalTime = approvalType && approvalType.duration;
	}

    isHead(ind: number) {
    	return ind === this.getHeadIndex();
    }

    getHeadIndex() {
        if (!this.order.approval || !this.order.approval.approvalCycle || !this.order.approval.approvalCycle.agreed) {
            return null;
        }
        const signingElectronically = this.isSigningElectronically();
        const head = <User> this.order.head;
        return _.findIndex(this.order.approval.approvalCycle.agreed, (a) => {
        	if (signingElectronically) {
                return a.approvalTypeCode === this.ApprovalType.approval;
			} else {
        		const accountName = a.agreedBy ? a.agreedBy.accountName : null;
                return head && accountName === head.accountName;
			}
        });
	}

    setHead() {
        const signingElectronically = this.isSigningElectronically();
    	if (signingElectronically) {
            const approver = _.find(this.order.approval.approvalCycle.agreed, (a) => {
                return a.approvalTypeCode === this.ApprovalType.approval;
            });
            if (approver) {
                let user = this.users[approver.agreedBy.accountName];
                let department = user ? user.department : null;
                this.order.head = ApprovalListEditController.getUser(approver.agreedBy, department);
			} else {
                this.order.head = null;
			}
        }
    }

    static getUser(approver: OrderApprover, department: string): User {
    	if (!approver) {
    		return null;
		}
    	return {
    		post: approver.post,
    		fio: approver.fioFull,
    		accountName: approver.accountName,
    		email: null,
    		phoneNumber: approver.phone,
			iofShort: approver.iofShort,
			department: department
    	}
    }

}

angular.module('app').component('approvalListEdit', new ApprovalListEditComponent())
.filter('approvalTypes', ['ApprovalType', (ApprovalType: any) => {
	return function(options: _.List<ApprovalType>, agreed: OrderApprovalListItem[], current: OrderApprovalListItem) {
		let hasApproval = _.some(agreed, (a) => {
			return a.approvalTypeCode === ApprovalType.approval;
		});
		return !hasApproval || current.approvalTypeCode === ApprovalType.approval ? options : _.filter(options, (o) => {
			return o.approvalTypeCode !== ApprovalType.approval;
		});
  	};
}])
.filter('approverFilter', () => {
    function checkApprover(approver: OrderApprover, criterion: string) {
        if (!criterion) {
            return true;
        }
        let tokens = criterion.split(" ");
        return !_.chain(tokens).map((token) => {
            return approver.fioFull.toLowerCase().indexOf(token.toLowerCase());
        }).some((index) => {
            return index < 0;
        }).value();
    }
    return (approvers: OrderApprover[], criterion: string) => {
        return _.filter(approvers, (approver) => {
            return checkApprover(approver, criterion);
        })
    }
});
