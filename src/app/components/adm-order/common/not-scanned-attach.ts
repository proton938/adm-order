class NotScannedAttachComponent implements ng.IComponentOptions {
    public bindings: any;
    public controller: any;
    public templateUrl: string;

    constructor() {
        this.controller = NotScannedAttachController;
        this.templateUrl = 'app/components/adm-order/common/not-scanned-attach.html';
        this.bindings = {
            done: '&'
        };
    }
}

class NotScannedAttachController {
    static $inject = ['$q', '$timeout', 'toastr', 'orderResource', 'fileResource', 'activityRestService', 'session', 'orderTaskService'];

    private done: () => void;
    private dropzone: Dropzone;

    constructor(private $q: ng.IQService, private $timeout: any, private toastr: Toastr, private orderResource: IOrderResource, private fileResource: IFileResource,
                private activityRestService: oasiBpmRest.ActivityRestService, private session: oasiSecurity.ISessionStorage, private orderTaskService: IOrderTaskService) {
    }

    $onInit() {
        let self: NotScannedAttachController = this;
        this.$timeout(() => {
            let options: any = {
                withCredentials: true,
                parallelUploads: 1,
                paramName: "file",
                url: "null",
                accept: (file, done) => {
                    let fileName: string = file.name;
                    let match = /[A-Za-z]*(\d*).*/.exec(fileName);
                    if (match[1]) {
                        let barcode: string = match[1];
                        this.orderResource.notScanned({
                            barcode: barcode
                        }, (data) => {
                            if (data.content && data.content.length > 0) {
                                file.documentID = data.content[0].documentID;
                                done();
                            } else {
                                done("Карточка со штрихкодом " + barcode + " не найдена");
                            }
                        })                        
                    } else {
                        done("Имя файла не соответствует шаблону");
                    }                    
                },
                init: function() {
                    this.on("processing", function(file) {
                        this.options.url = self.fileResource.getUploadUrl(file.documentID);
                    });
                    this.on("error", function(file, errorMessage) {
                        file.promise = self.$q.resolve();
                        self.toastr.warning(file.name + ": " + (errorMessage.message ? errorMessage.message : errorMessage), 'Ошибка');
                    });
                    this.on("success", function(file) {
                        const documentID = file.documentID;
                        file.promise = self.activityRestService.queryTasks({
                            processInstanceVariables: [{
                                name: "EntityIdVar",
                                value: documentID,
                                operation: "equals",
                                type: "string"
                            }]
                        }).then((tasksStatus: oasiBpmRest.IActivityArray<oasiBpmRest.ITask>) => {
                            let scanningTask = _.find(tasksStatus.data, (x: oasiBpmRest.ITask) => {
                                return x.formKey ==="ScanRD";
                            });
                            if (scanningTask) {
                                return self.orderResource.getById({id: documentID}).$promise.then((order) => {
                                    return self.activityRestService.setTaskAssignee(scanningTask.id, self.session.login()).then(() => {
                                        return self.activityRestService.finishTask(parseInt(scanningTask.id), [{
                                            "name": "RDPublishingSite",
                                            "value": order.PublishingSite
                                        }]).then(() => {
                                            if (order.systemCode === 'gpzu' && order.systemParentID
                                                && order.canceledDocument && order.canceledDocument.canceledDocumentNum && order.canceledDocument.canceledDocumentDate) {
                                                return self.orderResource.createIsogdXml({
                                                    id: order.documentID
                                                }).$promise.then(file => {
                                                    order.isogdXmlGuid = file.fileID;
                                                    return self.orderResource.update({id: order.documentID}, { document: order });
                                                }).then(() => {
                                                    return self.orderTaskService.getProcessDefinition('rd_ToIsogd_ID').then((process: oasiBpmRest.IProcessDefinition) => {
                                                        let props = [{
                                                            "name": "EntityIdVar",
                                                            "value": order.documentID
                                                        },{
                                                            "name": "EntityDescriptionVar",
                                                            "value": order.documentTypeValue + " " + order.documentContent
                                                        }];
                                                        return self.orderTaskService.initProcess(process.id, props);
                                                    });
                                                });
                                            }
                                        });
                                    })
                                })
                            }
                        }).catch(() => {
                            self.toastr.warning("Не удалось закрыть задачу для РД " + documentID, 'Ошибка');
                        })
                    });
                    this.on("queuecomplete", function(file, errorMessage) {
                        let callback = () => {
                            let successNum = _.filter(this.files, (file: any) => {
                                return file.status === 'success';
                            }).length;
                            let failedNum = _.filter(this.files, (file: any) => {
                                return file.status === 'error';
                            }).length;
                            let showResult = failedNum === 0 ?  self.toastr.success : self.toastr.warning;
                            showResult("Обработано файлов: успешно - " + successNum + ", неуспешно - " + failedNum, "Результат");
                            self.done();
                        };
                        self.$q.all(this.files.map(f => f.promise)).then(callback, callback);
                    });
                }
            };
            this.dropzone = new Dropzone("#attach-dropzone", options);
        });
    }

}

angular.module('app').component('notScannedAttach', new NotScannedAttachComponent());
