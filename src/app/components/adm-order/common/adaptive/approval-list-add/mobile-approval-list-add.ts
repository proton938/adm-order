class MobileApprovalListAddComponent {
    public bindings: any;
    public controller: any;
    public templateUrl: string;

    constructor() {
        this.controller = MobileApprovalListAddController;
        this.templateUrl = 'app/components/adm-order/common/adaptive/approval-list-add/mobile-approval-list-add.html';
        this.bindings = {
          agreed: '<',
          documentTypeCode: '<',
          except: '<',
			    dueDate: '<',
          systemCode: '<'
        };
    }
}

class MobileApprovalListAddController {
    static $inject = [
    	'$scope',
    	'ApprovalType',
		'nsiRestService',
		'nsiResource',
		'nsiUserService'
	];

	private agreed: AddedOrderApprovalListItem[];
	private documentTypeCode: string;
	private docAccessAction: string;
	private except: string[];
	private systemCode: string;

    private approvers: OrderApprover[] = [];
	private approvalTypes: ApprovalType[] = [];
	private dueDate: Date;

    constructor(
    	private $scope: any,
    	private ApprovalType: any,
		private nsiRestService: oasiNsiRest.NsiRestService,
		private nsiResource: INsiResource,
		private nsiUserService: INsiUserService
    ) {
    }

    $onInit() {
        this.nsiRestService.get('RD_TYPES').then((types: RDType[]) => {
            this.docAccessAction = _.find(types, type => type.DOC_CODE === this.documentTypeCode).DOC_ACCESS_ACTION;
            return this.nsiRestService.get('RDApprovalDuration');
        }).then((approvalTypes: ApprovalType[]) => {
			for (let i = 0; i < approvalTypes.length; i++) {
				if (!_.find(this.approvalTypes, type => type.approvalTypeCode === approvalTypes[i].approvalTypeCode)) {
					this.approvalTypes.push(approvalTypes[i]);
				}
			}
            _.remove(this.approvalTypes, at => {
                return at.approvalTypeCode === this.ApprovalType.approval;
            });
            return this.nsiUserService.approvers(this.docAccessAction)
        }).then((approvers: User[]) => {
            this.approvers = _.chain(approvers).filter(a => {
                return _.indexOf(this.except, a.accountName) < 0;
            }).map(a => {
                return this.getOrderApprover(a);
            }).value();
        });
    }

    getOrderApprover(user: User) {
    	return {
    		post: user.post,
    		fioFull: user.fio,
    		accountName: user.accountName,
    		fioShort: user.fioShort,
    		iofShort: user.iofShort,
    		phone: user.phoneNumber
    	}
    }

    setNumeration() {
    	_.each(this.agreed, (item, index) => {
    		item.approvalNum = "" + (index + 1);
    	})
    }

    swapApprovals(ind1: number, ind2: number) {
    	let app = this.agreed[ind1];
        this.agreed[ind1] = this.agreed[ind2];
        this.agreed[ind2] = app;
    	this.setNumeration();
    }

    delApproval(ind: number) {
    	this.agreed.splice(ind, 1);
    	this.setNumeration();
    }

    addApproval() {
    	let agreed = new AddedOrderApprovalListItem();
    	let approvalType = _.find(this.approvalTypes, (at) => {
            if (!this.systemCode) {
                return at.approvalTypeCode === this.ApprovalType.assent;
            }
            return at.approvalTypeCode === this.ApprovalType.agreed;
    	});
    	agreed.approvalType = approvalType.approvalType;
    	agreed.approvalTypeCode = approvalType.approvalTypeCode;
		this.nsiResource.toLocalDateTime({zonedDateTime: this.dueDate}).$promise.then(({value}) => {
			return this.nsiResource.calcDuration({localDateTime: value}).$promise;
		}).then(({value}) => {
			return this.nsiResource.divideTimeBy({duration: value, divideBy: this.agreed.length + 1}).$promise;
		}).then(({value}) => {
			agreed.approvalTime = value;
			this.agreed.push(agreed);
			this.setNumeration();
		});
    }

    approvalTypeChanged(ind: number) {
    	let approvalType = _.find(this.approvalTypes, (at) => {
    		return at.approvalTypeCode === this.agreed[ind].approvalTypeCode
    	});
    	this.agreed[ind].approvalType = approvalType.approvalType;
    	this.setNumeration();
    }
}

angular.module('app').component('mobileApprovalListAdd', new MobileApprovalListAddComponent());
