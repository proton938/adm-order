
class MobileApprovalListComponent {
    public bindings: any;
    public controller: any;
    public templateUrl: string;

    constructor() {
        this.controller = MobileApprovalListController;
        this.templateUrl = 'app/components/adm-order/common/adaptive/approval-list/mobile-approval-list.html';
        this.bindings = {
        	list: '<'
        };
    }
}

class MobileApprovalListController {
    static $inject = [
    	'fileResource',
		'nsiRestService'
	];

	private list: OrderApprovalList;

	private approvalTypes: { [key: string]: ApprovalType }

    constructor(
    	private fileResource: IFileResource,
        private nsiRestService: oasiNsiRest.NsiRestService
    ) {
    }

    $onInit() {
		this.nsiRestService.get('Approval').then((approvalTypes: ApprovalType[]) => {
			this.approvalTypes = {};
			_.each(approvalTypes, at => {
				this.approvalTypes[at.approvalTypeCode] = at;
			})
		});
    }

    getFileLink(id) {
        return this.fileResource.getDownloadUrl(id);
    }

    getApprovalResultColor(approvalTypeCode: string, approvalResult: string) {
    	if (this.approvalTypes) {
	    	let approvalType = this.approvalTypes[approvalTypeCode];
	    	if (approvalResult === approvalType.buttonYes) {
	    		return 'label-' + approvalType.buttonYesColor;
	    	} else if (approvalResult === approvalType.buttonNo) {
	    		return 'label-' + approvalType.buttonNoColor;
	    	}
    	}
    }

    addedHasNote(item){
       return _.find(item, i => {return i.addedNote ? i.addedNote !== '' : false;});
	}
}

angular.module('app').component('mobileApprovalList', new MobileApprovalListComponent());
