class DesktopApprovalListComponent {
    public bindings: any;
    public controller: any;
    public templateUrl: string;

    constructor() {
        this.controller = DesktopApprovalListController;
        this.templateUrl = 'app/components/adm-order/common/adaptive/approval-list/desktop-approval-list.html';
        this.bindings = {
        	list: '<'
        };
    }
}

class DesktopApprovalListController {
    static $inject = [
    	'fileResource',
		'nsiRestService',
		'orderTaskService'
	];

	private list: OrderApprovalList;

	private approvalTypes: { [key: string]: ApprovalType }

    constructor(
    	private fileResource: IFileResource,
        private nsiRestService: oasiNsiRest.NsiRestService,
        private orderTaskService: IOrderTaskService
    ) {
    }

    $onInit() {
        this.orderTaskService.getApprovalDict().then((approvalTypes: ApprovalType[]) => {
			this.approvalTypes = {};
			_.each(approvalTypes, at => {
				this.approvalTypes[at.approvalTypeCode] = at;
			})
		});
    }

    getFileLink(id) {
        return this.fileResource.getDownloadUrl(id);
    }

    getApprovalResultColor(approvalTypeCode: string, approvalResult: string) {
    	if (this.approvalTypes) {
	    	let approvalType = this.approvalTypes[approvalTypeCode];
	    	if (approvalResult === approvalType.buttonYes) {
	    		return 'label-' + approvalType.buttonYesColor;
	    	} else if (approvalResult === approvalType.buttonNo) {
	    		return 'label-' + approvalType.buttonNoColor;
	    	}
    	}
    }

}

angular.module('app').component('desktopApprovalList', new DesktopApprovalListComponent());
