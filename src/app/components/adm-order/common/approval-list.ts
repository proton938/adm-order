class ApprovalListComponent {
    public bindings: any;
    public controller: any;
    public template: string;

    constructor() {
        this.controller = ApprovalListController;
        this.template = '<desktop-approval-list list="$ctrl.list" class="hidden-xs hidden-sm"></desktop-approval-list>' +
                        '<mobile-approval-list list="$ctrl.list" class="visible-sm visible-xs"><mobile-approval-list>';
        this.bindings = {
        	list: '<'
        };
    }
}

class ApprovalListController {
  static $inject = [
  	'$localStorage'
  ];

	private list: OrderApprovalList;

  constructor(private $localStorage: any) {
  }
}

angular.module('app').component('approvalList', new ApprovalListComponent());
