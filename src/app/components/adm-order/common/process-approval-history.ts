class ProcessApprovalHistoryComponent {
    public bindings: any;
    public controller: any;
    public templateUrl: string;

    constructor() {
        this.controller = ProcessApprovalHistoryController;
        this.templateUrl = 'app/components/adm-order/common/process-approval-history.html';
        this.bindings = {
        	history: '<'
        };
    }
}

class ProcessApprovalHistoryController {
    static $inject = [
	];

    constructor(
    ) {
    }

    $onInit() {
    }
    
}

angular.module('app').component('processApprovalHistory', new ProcessApprovalHistoryComponent());