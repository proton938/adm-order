import { NsiSearchData } from "../../../../../bower_components/oasi-nsi-rest/dist/@types/oasi-nsi-rest/index";

class ProcessPrepareEditComponent {
	public bindings: any;
	public controller: any;
	public templateUrl: string;

	constructor() {
		this.controller = ProcessPrepareEditController;
		this.templateUrl = 'app/components/adm-order/common/process-prepare-edit.html';
		this.bindings = {
			order: '<',
			preregFields: '<',
			account: '<',
			isPrereg: '<',
			hasPreregPermission: '<'
		};
	}
}

class ProcessPrepareEditController {
	static $inject = [
		'$q',
		'toastr',
		'orderResource',
		'fileResource',
		'ApprovalType',
		'nsiRestService',
		'prSession',
		'registerDocumentService',
		'nsiUserService'
	];

	private order: Order;
	private orderType: RDType;

	private orderTypes: RDType[];
	private availableYears: number[];
	private availableCanceledDocuments: CancelType[];
	private availableUpdatedDocuments: UpdateType[];
	private executives: User[];
	private executors: User[];
	private emailGroups: EmailGroup[];
	private subscribers: User[];
	private documentTypes: string[] = [];

	private dzAdditionalMaterialsOptions: any;
	private dzAdditionalMaterialsCallbacks: any;
	private dzAdditionalMaterialsMethods: any;
	private fullApprovalTypesWithTime: ApprovalType[];

	constructor(
		private $q: any,
		private toastr: Toastr,
		private orderResource: IOrderResource,
		private fileResource: IFileResource,
		private ApprovalType: any,
		private nsiRestService: oasiNsiRest.NsiRestService,
		private prSession: ISessionStorage,
		private registerDocumentService: IRegisterDocumentService,
		private nsiUserService: INsiUserService
	) {
	}

	$onInit() {
		this.availableYears = _.range(2016, 2028);

		//setting initial values for comboboxes (showing until dictionaries is loaded)
		this.orderTypes = [{
			DOC_CODE: this.order.documentTypeCode,
			DOC_NAME: this.order.documentTypeValue,
			DOC_ACCESS_EI: null,
			DOC_SING_ELECTRON: false,
			PUBLISHING: true
		}];
		this.executors = this.order.executor;
		this.subscribers = this.order.mailList;

		this.initDropzone();
		this.loadDictionaries();
	}

	loadDictionaries() {
		this.$q.all([
			this.nsiRestService.get('RDApprovalDuration'),
			this.nsiRestService.get('RD_TYPES')
		]).then((values: any[]) => {
			this.fullApprovalTypesWithTime = values[0];
			this.orderTypes = values[1].filter(type => this.prSession.rdTypes().indexOf(type.DOC_CODE) > -1);

			this.orderType = _.find(this.orderTypes, (ot: RDType) => {
				return ot.DOC_CODE === this.order.documentTypeCode;
			});

			this.nsiUserService.executives(this.orderType.DOC_ACCESS_ACTION).then((value: User[]) => {
				this.executives = value;
			});

			if (!this.order.hasOwnProperty('signingElectronically')) {
				this.order.signingElectronically = this.orderType.DOC_SING_ELECTRON.toString() == 'true';
			}

			if (!this.order.hasOwnProperty('PublishingSite')) {
				this.order.PublishingSite = String(this.orderType.PUBLISHING) === 'true';
			}

		});

		this.nsiUserService.executors().then((value: User[]) => {
			this.executors = value;
		});

		let emailGroupsParams: NsiSearchData = {
			nickAttr: 'sendSystem',
			nickAttrLink: 'code',
			values: ['rd']
		};

		this.nsiRestService.getBy('emailGroups', emailGroupsParams).then((value: any) => {
			this.emailGroups = value;
		});

		this.nsiUserService.subscribers().then((value: User[]) => {
			this.subscribers = value;
		});

		this.nsiRestService.get('RD_STATUS').then((result: RDStatus[]) => {
			this.documentTypes = result
				.filter(status => status.statusAnalytic === 'Действующий')
				.map(status => status.STATUS_CODE);
		});
	}

	initDropzone() {
		this.dzAdditionalMaterialsOptions = {
			autoProcessQueue: true,
			withCredentials: true,
			parallelUploads: 1,
			maxFilesize: 500,
			paramName: "file",
			url: this.fileResource.getAdditionalMaterialUploadUrl(this.order.documentID),
			dictDefaultMessage: 'Загрузить файл. <br/> Допустимые расширения: doc, docx, xls, xslx, pdf',
			accept: (file, done) => {
				let fileName: string = file.name;
				let match = /.*(\.docx?|\.xlsx?|\.pdf)/.exec(fileName.toLowerCase());
				if (match) {
					done();
				} else {
					done("Допустимые расширения: doc, docx, xls, xslx, pdf");
				}
			}
		};
		this.dzAdditionalMaterialsCallbacks = {
			error: (file, errorMessage) => {
				this.toastr.warning(errorMessage.message ? errorMessage.message : errorMessage, 'Ошибка');
				this.dzAdditionalMaterialsMethods.removeFile(file);
			}, success: (dzFile, file) => {
				this.dzAdditionalMaterialsMethods.removeFile(dzFile);
				this.order.additionalMaterials = this.order.additionalMaterials || [];
				this.order.additionalMaterials.push(file);
			}
		};
	}

	findUser(users: User[], login: string) {
		return _.find(users, (user) => {
			return user.accountName === login;
		})
	}

	changeSelect() {
		let approvalNum = this.order.approval.approvalCycle.agreed.length + 1;
		if (_.isArray(this.order.head)) {
			this.order.head.map(head => {
				const agreed: any = {
					approvalNum,
					approvalType: this.orderType.DOC_SING_ELECTRON ? (this.order.signingElectronically ? 'Утверждение' : null) : 'Согласование (с ЭП)',
					approvalTypeCode: this.orderType.DOC_SING_ELECTRON ? (this.order.signingElectronically ? 'approval' : null) : 'agreed',
					agreedBy: { ...head, fioFull: head.fio }
				};
				agreed.approvalTime = this.personSetTime(agreed);
				this.order.approval.approvalCycle.agreed.push(agreed);
			});
		} else {
			const agreed: any = {
				approvalNum,
				approvalType: this.orderType.DOC_SING_ELECTRON ? (this.order.signingElectronically ? 'Утверждение' : null) : 'Согласование (с ЭП)',
				approvalTypeCode: this.orderType.DOC_SING_ELECTRON ? (this.order.signingElectronically ? 'approval' : null) : 'agreed',
				agreedBy: { ...this.order.head, fioFull: this.order.head.fio }
			};
			agreed.approvalTime = this.personSetTime(agreed);
			this.order.approval.approvalCycle.agreed.push(agreed);
		}
	}

	personSetTime(agreed) {
		if (!agreed.agreedBy) return;
		const user = agreed.agreedBy;
		let department = user ? user.department : null;
		let approvalType = _.find(this.fullApprovalTypesWithTime, type => type.approvalTypeCode === agreed.approvalTypeCode && type.department === department);
		if (!approvalType) {
			approvalType = _.find(this.fullApprovalTypesWithTime, type => type.approvalTypeCode === agreed.approvalTypeCode
				&& !type.department);
		}
		return approvalType && approvalType.duration;
	}

	addEmailGroup(emailGroup: EmailGroup) {
		this.order.mailList = this.order.mailList || [];
		let usersToAdd: User[] = _.chain(emailGroup.users).map((login) => {
			return this.findUser(this.subscribers, login);
		}).filter((user) => {
			return user;
		}).filter((user) => {
			return !this.findUser(this.order.mailList, user.accountName);
		}).value();
		_.each(usersToAdd, (user) => {
			this.order.mailList.push(user);
		});
	}

	searchDocuments(query) {
		this.orderResource.search({ page: 0, size: 20 }, { docName: query, docStatusCode: this.documentTypes }, (response) => {
			this.availableCanceledDocuments = _.map(response.content, (order: Order) => {
				return {
					canceledDocumentID: order.documentID,
					canceledDocumentNum: order.documentNumber,
					canceledDocumentDate: order.documentDate,
					canceledDocumentType: order.documentTypeValue,
					canceledDocumentTypeCode: order.documentTypeCode
				}
			});

			this.availableUpdatedDocuments = _.map(response.content, (order: Order) => {
				return {
					updatedDocumentID: order.documentID,
					updatedDocumentNum: order.documentNumber,
					updatedDocumentDate: order.documentDate,
					updatedDocumentType: order.documentTypeValue,
					updatedDocumentTypeCode: order.documentTypeCode
				}
			});

		})
	}

	hasApproval() {
		if (!this.order.approval || !this.order.approval.approvalCycle || !this.order.approval.approvalCycle.agreed) {
			return false;
		}
		return _.some(this.order.approval.approvalCycle.agreed, (a) => {
			return a.approvalTypeCode === this.ApprovalType.approval;
		})
	}

	getApproval() {
		if (!this.order.approval || !this.order.approval.approvalCycle || !this.order.approval.approvalCycle.agreed) {
			return null;
		}
		return _.find(this.order.approval.approvalCycle.agreed, (a) => {
			return a.approvalTypeCode === this.ApprovalType.approval;
		})
	}

	deleteAdditionalMaterial(fileInfo: any) {
		let fileID = fileInfo.idFile;
		this.fileResource.deleteAdditionalMaterial({
			id: this.order.documentID,
			fileGuid: fileID
		}, {}, () => {
			_.remove(this.order.additionalMaterials, f => {
				return f.fileID === fileID;
			})
		});
	}

	generateDocumentNumber() {
		return this.registerDocumentService.registerDocument(this.order);
	}

}

angular.module('app').component('processPrepareEdit', new ProcessPrepareEditComponent());
