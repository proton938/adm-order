namespace orders {
    export class ExtSearchFilter {
        docNumber: string;
        barcode: string;
        approvedDocNumber: string;
        docContent: string;
        docDate: DateRange;
        docTypeCode: RDType[] = [];
        docStatusCode: string[] = [];
        responsibleExecutor: string;
        responsibleExecutors: string[];
        docYear: number[];
    }

    export class ExtSearchMiniFilter {
        docNumber: string;
        barcode: string;
    }

    export class DateRange {
        startDate: Date;
        endDate: Date;
    }
}