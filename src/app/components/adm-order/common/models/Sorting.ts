namespace orders {
    export class Sorting {
        static HAS_FILE: string = "docScanned";
        static IS_SENT: string = "docSent";
        static DOCUMENT_TYPE: string = "docType";
        static DOCUMENT_DATE: string = "docDate";
        static DOCUMENT_NUMBER: string = "docNumber";
        static DOCUMENT_STATUS: string = "docStatus";
        static RESPONSIBLE_EXECUTORS: string = "responsibleExecutors";
        static DOCUMENT_YEAR: string = "docYear";
    }

    angular.module("app").constant("Sorting", {
        HAS_FILE: Sorting.HAS_FILE,
        IS_SENT: Sorting.IS_SENT,
        DOCUMENT_TYPE: Sorting.DOCUMENT_TYPE,
        DOCUMENT_DATE: Sorting.DOCUMENT_DATE,
        DOCUMENT_NUMBER: Sorting.DOCUMENT_NUMBER,
        DOCUMENT_STATUS: Sorting.DOCUMENT_STATUS,
        RESPONSIBLE_EXECUTORS: Sorting.RESPONSIBLE_EXECUTORS,
        DOCUMENT_YEAR: Sorting.DOCUMENT_YEAR
    });
}