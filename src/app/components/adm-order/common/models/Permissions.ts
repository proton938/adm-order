namespace orders {
    export class Permissions {
        static OASI_ADMORDER_VIEW_ALL: string = "OASI_ADMORDER_VIEW_ALL";
        static OASI_ADMORDER_VIEW_SCAN: string = "OASI_ADMORDER_VIEW_SCAN";
        static OASI_ADMORDER_VIEW_MAIL: string = "OASI_ADMORDER_VIEW_MAIL";
        static OASI_ADMORDER_CARD: string = "OASI_ADMORDER_CARD";
        static OASI_ADMORDER_FORM: string = "OASI_ADMORDER_FORM";
        static OASI_ADMORDER_ADDBUTTON: string = "OASI_ADMORDER_ADDBUTTON";
        static OASI_ADMORDER_EDITBUTTON: string = "OASI_ADMORDER_EDITBUTTON";
        static OASI_ADMORDER_SCANBUTTON: string = "OASI_ADMORDER_SCANBUTTON";
        static OASI_ADMORDER_MAILBUTTON: string = "OASI_ADMORDER_MAILBUTTON";
        static OASI_ADMORDER_PREREGBUTTON: string = "OASI_ADMORDER_PREREGBUTTON";
        static OASI_ADMORDER_REG_WITHOUT_APPROVE: string = "OASI_ADMORDER_REG_WITHOUT_APPROVE";
    }

    angular.module("app").constant("Permissions", {
        OASI_ADMORDER_VIEW_ALL: Permissions.OASI_ADMORDER_VIEW_ALL,
        OASI_ADMORDER_VIEW_SCAN: Permissions.OASI_ADMORDER_VIEW_SCAN,
        OASI_ADMORDER_VIEW_MAIL: Permissions.OASI_ADMORDER_VIEW_MAIL,
        OASI_ADMORDER_CARD: Permissions.OASI_ADMORDER_CARD,
        OASI_ADMORDER_FORM: Permissions.OASI_ADMORDER_FORM,
        OASI_ADMORDER_ADDBUTTON: Permissions.OASI_ADMORDER_ADDBUTTON,
        OASI_ADMORDER_EDITBUTTON: Permissions.OASI_ADMORDER_EDITBUTTON,
        OASI_ADMORDER_SCANBUTTON: Permissions.OASI_ADMORDER_SCANBUTTON,
        OASI_ADMORDER_MAILBUTTON: Permissions.OASI_ADMORDER_MAILBUTTON,
        OASI_ADMORDER_PREREGBUTTON: Permissions.OASI_ADMORDER_PREREGBUTTON,
        OASI_ADMORDER_REG_WITHOUT_APPROVE: Permissions.OASI_ADMORDER_REG_WITHOUT_APPROVE,
    });
}
