class ApprovalListAddComponent {
    public bindings: any;
    public controller: any;
    public templateUrl: string;

    constructor() {
        this.controller = ApprovalListAddController;
        this.templateUrl = 'app/components/adm-order/common/approval-list-add.html';
        this.bindings = {
            agreed: '<',
            documentTypeCode: '<',
			      except: '<',
			      dueDate: '<',
            systemCode: '<'
        };
    }
}

class ApprovalListAddController {
  static $inject = [
    '$localStorage'
	];

	private agreed: AddedOrderApprovalListItem[];
	private documentTypeCode: string;
	private except: string[];
  private dueDate: Date;
	private systemCode: string;

  constructor(private $localStorage: any) {
  }
}

angular.module('app').component('approvalListAdd', new ApprovalListAddComponent());
