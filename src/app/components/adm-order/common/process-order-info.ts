class ProcessOrderInfoComponent {
    public bindings: any;
    public controller: any;
    public templateUrl: string;

    constructor() {
        this.controller = ProcessOrderInfoController;
        this.templateUrl = 'app/components/adm-order/common/process-order-info.html';
        this.bindings = {
        	order: '<',
        	taskInfo: '<',
            showDocumentText: '<',
            showDocumentComment: '<'
        };
    }
}

class ProcessOrderInfoController {
    static $inject = [
	];
	
	private taskDescription: string = "";	
	private isCollapsed: boolean = true;

    constructor(
    ) {
    }

    $onInit() {
    }
    
}

angular.module('app').component('processOrderInfo', new ProcessOrderInfoComponent());
