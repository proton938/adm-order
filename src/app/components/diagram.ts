class DiagramController {
    static $inject = ['$rootScope', '$stateParams'];

    constructor(private $rootScope: ng.IRootScopeService, private $stateParams: ng.ui.IStateParamsService) {
    }

    $onInit() {
        this.$rootScope['title'] = "Диаграмма процесса \"" + this.$stateParams["title"] + "\"";
    }
}

class DiagramComponent {
    controller = DiagramController;
    templateUrl = 'app/components/diagram.html';
}

angular.module('app').component('diagram', new DiagramComponent());