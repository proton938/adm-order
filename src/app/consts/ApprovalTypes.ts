angular.module("app").constant("ApprovalType", {
    approval: 'approval',
    agreed: 'agreed',
    assent: 'assent'
});