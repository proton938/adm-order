interface IOrderTaskService {
	getProcessDefinition(key: string): ng.IPromise<oasiBpmRest.IProcessDefinition>;
	initProcess(processDefinitionId: string, variables: oasiBpmRest.ITaskVariable[]): ng.IPromise<number>;
	updateProcess(taskId: string, vars: oasiBpmRest.ITaskVariable[]): ng.IPromise<string>;
    getApprovalDict(): ng.IPromise<ApprovalType[]>;
}

class OrderTaskService implements IOrderTaskService {
    static $inject = [
    	'$q',
    	'activityRestService',
        'nsiRestService'
	];
    private approvalTypes: { [key: string]: ApprovalType };

    constructor(
    	private $q: ng.IQService,
		private activityRestService: oasiBpmRest.ActivityRestService,
        private nsiRestService: oasiNsiRest.NsiRestService
    ) {
        this.nsiRestService.get('Approval').then((approvalTypes: ApprovalType[]) => {
            this.approvalTypes = {};
            _.each(approvalTypes, at => {
                this.approvalTypes[at.approvalTypeCode] = at;
            });
        })
    }


    getProcessDefinition(key: string): ng.IPromise<oasiBpmRest.IProcessDefinition> {
        let defer: ng.IDeferred<any> = this.$q.defer();

        this.activityRestService.getProcessDefinitions({
            key: key,
            latest: true
        }).then((response) => {
            let processes = response.data;
            if (processes.length <= 0) {
                defer.reject("Процесс " + key + " не найден");
            } else {
                defer.resolve(processes[0]);
            }
        }).catch((error) => {
            defer.reject(error);
        });

        return defer.promise;
    }
    
    initProcess(processDefinitionId: string, variables: oasiBpmRest.ITaskVariable[]): ng.IPromise<number> {
        return this.activityRestService.initProcess({
            processDefinitionId: processDefinitionId,
            variables: variables
        });
    }
    
    updateProcess(taskId: string, vars: oasiBpmRest.ITaskVariable[]): ng.IPromise<string> {
        let defer: ng.IDeferred<any> = this.$q.defer();

        this.activityRestService.finishTask(parseInt(taskId), vars).then(id => {
             defer.resolve(id);
        }).catch((error) => {
            defer.reject(error);
        });
    
        return defer.promise;
    }

    getApprovalDict(): ng.IPromise<ApprovalType[]> {
        let defer: ng.IDeferred<any> = this.$q.defer();
        if(!this.approvalTypes) {
            this.nsiRestService.get('Approval').then((approvalTypes: ApprovalType[]) => {
                this.approvalTypes = {};
                _.each(approvalTypes, at => {
                    this.approvalTypes[at.approvalTypeCode] = at;
                });
                defer.resolve(this.approvalTypes);
            }).catch((error) => {
                defer.reject(error);
            });
        } else {
            defer.resolve(this.approvalTypes);
        }


        return defer.promise;
    }
}

angular.module('app').service('orderTaskService', OrderTaskService);