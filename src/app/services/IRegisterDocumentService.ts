interface IRegisterDocumentService {
    fillDocumentDate(order: Order): ng.IPromise<void>;
    registerDocument(order: Order): ng.IPromise<void>;
    checkLinkedDocuments(order: Order): ng.IPromise<void>;
}

class RegisterDocumentService implements IRegisterDocumentService {
    static $inject = ['$q', 'orderResource', 'workingDateService', 'alertService'];

    constructor(private $q: ng.IQService,
                private orderResource: IOrderResource,
                private workingDateService: IWorkingDateService,
                private alertService: oasiWidgets.IAlertService) {
    }

    fillDocumentDate(order: Order): ng.IPromise<void> {
        if (order.documentDate) {
            if (order.documentDate.getFullYear() !== order.documentYear) {
                return this.alertService.message({
                    message: 'Дата документа не соответствует году документа'
                }).catch(() => {
                    return this.$q.reject();
                });
            } else {
                return this.$q.resolve();
            }
        } else {
            return this.workingDateService.getClosestWorkingDay().then(date => {
                order.documentDate = date;
                return this.$q.resolve();
            })
        }
    }

    checkLinkedDocuments(order: Order): ng.IPromise<void> {
        if (order.linkedDocument && order.linkedDocument.length > 0) {
            return this.$q.all(order.linkedDocument.map(doc => this.orderResource.getById({id: doc.linkedDocumentID}).$promise))
                .then(linkedDocs => {
                    const notRegistered = _.find(linkedDocs, (doc: Order) => !doc.documentNumber || !doc.documentDate);
                    if (notRegistered) {
                       return this.$q.reject(notRegistered);
                    }
                });
        }
        else {
            return this.$q.resolve();
        }
    }

    registerDocument(order: Order): ng.IPromise<void> {
        return this.orderResource.generateNumber({
            type: order.documentTypeCode,
            year: order.documentYear
        }).$promise.then((response: OrderGenerateNumberResult) => {
            order.documentNumber = response.documentNumber;
            return this.fillDocumentDate(order);
        });
    }

}

angular.module('app').service('registerDocumentService', RegisterDocumentService);