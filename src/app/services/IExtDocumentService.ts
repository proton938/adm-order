interface IExtDocumentService {
    getLink(order: Order): ng.IPromise<string>;
}

class ExtDocumentService implements IExtDocumentService {
    static $inject = ['$q', 'solrResource'];

    constructor(private $q: ng.IQService, private solrResource: ISolrResource) {
    }

    getLink(order: Order) {
        if (order.approvedDocumentNumber) {
            const deferred = this.$q.defer<string>();
            this.$q.all([
                this.getDocLink('GPZU','docNumberGpzu', order.approvedDocumentNumber),
                this.getDocLink('PZZ','docNumberPzz', order.approvedDocumentNumber),
                this.getDocLink('PPT','regNumberPPT', order.approvedDocumentNumber)
            ]).then((results: string[]) => {
                results = _.filter(results, str => str);
                deferred.resolve(results.length > 0 ? results[0] : null);
            });
            return deferred.promise;
        }
        return this.$q.resolve(null)
    }

    getDocLink(type: string, numberField: string, number: string) {
        const deferred = this.$q.defer<string>();
        this.solrResource.searchExt({}, {
            fields: [
                {name: numberField, value: number}
            ], type: type
        }).$promise.then((result: SolrSearchResult) => {
            const docs = result.response.docs;
            if (docs && docs.length > 0) {
                deferred.resolve(docs[0].link);
            } else {
                deferred.resolve(null);
            }
        }).catch(() => {
            deferred.reject();
        });
        return deferred.promise;
    }

}

angular.module('app').service('extDocumentService', ExtDocumentService);
