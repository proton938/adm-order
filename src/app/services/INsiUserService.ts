interface INsiUserService {
    user(login: string): ng.IPromise<User>;
    approvers(docAccessAction: string, department?: string[]): ng.IPromise<User[]>;
    executors(): ng.IPromise<User[]>;
    registrars(): ng.IPromise<User[]>;
    subscribers(): ng.IPromise<User[]>;
    executives(docAccessAction: string): ng.IPromise<User[]>;
}

function nsiUserToUser(user: oasiNsiRest.UserBean): User {
    return {
        accountName: user.accountName,
        fio: user.displayName,
        post: user.post,
        email: user.mail,
        phoneNumber: user.telephoneNumber,
        iofShort: user.iofShort,
        fioShort: user.fioShort,
        department: user.departmentFullName
    }
}

class NsiUserService implements INsiUserService {
    static $inject = [
        '$q',
        'nsiRestService'
    ];

    constructor(
        private $q: ng.IQService,
        private nsiRestService: oasiNsiRest.NsiRestService
    ) {
    }

    user(login: string): ng.IPromise<User> {
        return this.nsiRestService.ldapUser(login)
            .then((user) => {
                return nsiUserToUser(user);
            });
    }

    approvers(docAccessAction: string, department?: string[]): ng.IPromise<User[]> {
        let params: any = {
            group: [ 'OASI_ADMORDER_APPROVE', docAccessAction ]
        };
        if (department) params.department = department;
        return this.nsiRestService.searchUsers(params)
            .then(users => {
                return users.map(nsiUserToUser);
            });
    }

    executives(docAccessAction: string): ng.IPromise<User[]> {
        return this.nsiRestService.searchUsers({ group: [ 'OASI_ADMORDER_HEAD', docAccessAction ]})
            .then((users: oasiNsiRest.UserBean[]) => {
                return users.map(nsiUserToUser);
            });
    }

    executors(): ng.IPromise<User[]> {
        return this.nsiRestService.searchUsers({ group: [ 'OASI_ADMORDER_EDIT' ]})
            .then((users: oasiNsiRest.UserBean[]) => {
                return users.map(nsiUserToUser);
            });
    }

    registrars(): ng.IPromise<User[]> {
        return this.nsiRestService.searchUsers({ group: [ 'OASI_ADMORDER_REGISTER' ]})
            .then((users: oasiNsiRest.UserBean[]) => {
                return users.map(nsiUserToUser);
            });
    }

    subscribers(): ng.IPromise<User[]> {
        return this.nsiRestService.ldapUsers()
            .then((users: oasiNsiRest.UserBean[]) => {
                return users.filter(user => !!user.mail).map(nsiUserToUser);
            });
    }

}

angular.module('app').service('nsiUserService', NsiUserService);