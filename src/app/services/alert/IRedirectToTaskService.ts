interface IRedirectToTaskService {
    redirectToTask(settings: RedirectToTaskSettings): ng.IPromise<any>;
}

class RedirectToTaskSettings {
    linked: {
        documentID: string;
        documentTypeValue: string;
    }
}

class RedirectToTaskService implements IRedirectToTaskService {
    static $inject = ['$uibModal'];

    constructor(private $uibModal: any) {
    }

    redirectToTask(settings: RedirectToTaskSettings): ng.IPromise<any> {

        let controller = function($uibModalInstance, $scope) {

            $scope.linked = settings.linked;

            $scope.cancel = function() {
                $uibModalInstance.close();
            };

            return {}
        };
        controller.$inject = ['$uibModalInstance', '$scope'];

        return this.$uibModal.open({
            templateUrl: 'app/services/alert/redirect-to-task.html',
            controller: controller
        }).result;
    }

}

angular.module('app').service('redirectToTaskService', RedirectToTaskService);