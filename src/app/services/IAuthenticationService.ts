interface IAuthenticationService {
    checkSession(onSuccess: any, onError: any);
    destroySession(onSuccess: any, onError: any);
}

class AuthenticationService implements IAuthenticationService {
    static $inject = ['$http', '$base64', '$q', 'toastr', 'authenticationResource', 'nsi_url'];

    constructor(private $http: ng.IHttpService,
                private $base64: any,
                private $q: ng.IQService,
                private toastr: Toastr,
                private authenticationResource: IAuthenticationResource,
                private nsiUrl: string) {
    }

    public checkSession(onSuccess: any, onError: any) {
        const $http = this.$http;
        const nsiUrl = this.nsiUrl;
        let error = function (reason) {
            let currentLocation = window.location.pathname + window.location.search + window.location.hash;
            // # решетку нужно заменить иначе роутер обрубит остаток урла после
            window.location.href = '/?from=' + currentLocation.replace(/#/, '^');
        };

        this.authenticationResource.user((authUser: AuthUser) => {
            try {
                onSuccess(authUser);
            } catch (exc) {
                console.error(exc);
            }
        }, error);
    }

    public destroySession(onSuccess: any, onError: any) {
        this.authenticationResource.logout(function () {
            try {
                onSuccess();
            } catch (exc) {
                console.error(exc);
            }
        }, function (reason) {
            console.error(reason);
            try {
                onError(reason);
            } catch (exc) {
                console.error(exc);
            }
        });
    }
}

angular.module('app').service('authenticationService', AuthenticationService);
