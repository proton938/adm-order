interface IActivityHttpService {
    addApprovers(id: number | string, agreed: OrderApprovalListItem[]): ng.IPromise<any>;
}

class ActivityHttpService implements IActivityHttpService {
    static $inject = ['$http', 'activiti_rest_url', '$q'];

    constructor(private $http: ng.IHttpService, private activitiRestUrl: string, private $q: ng.IQService) {
    }

    addApprovers(id: number | string, agreed: OrderApprovalListItem[]): ng.IPromise<any> {
        let url: string = this.activitiRestUrl + '/approval/changeApprovalPersons';

        let defer: ng.IDeferred<any> = this.$q.defer();

        let requestData = {
            taskId: id,
            usersCollectionVarName: "ApprovalUsersVar",
            durationsCollectionVarName: "ApprovalDurationsVar",
            elementVarName: "ApprovalUserVar",
            approvalPersons: _.map(agreed, _ => {
                return {
                    userName: _.agreedBy.accountName//,
                    //duration: _.approvalTime
                }
            })
        };

        this.$http.post(url, requestData).then(_ => {
            defer.resolve(<any>_.data);
        }).catch(error => {
            defer.reject(error);
        });

        return defer.promise;
    }


}

angular.module('app').service('activityHttpService', ActivityHttpService);
