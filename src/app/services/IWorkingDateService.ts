interface IWorkingDateService {
    getClosestWorkingDay(): ng.IPromise<Date>;
}

class WorkingDateService implements IWorkingDateService {
    static $inject = ['$q', 'nsiResource', 'dateFilter'];

    constructor(private $q: ng.IQService,
                private nsiResource: INsiResource,
                private dateFilter: any) {
    }

    getClosestWorkingDay(): ng.IPromise<Date> {
        return this.getNextWorkingDate(new Date(Date.now()));
    }

    getNextWorkingDate(date: Date): ng.IPromise<Date> {
        return this.nsiResource.isWorkDay({date: this.dateFilter(date, 'yyyy-MM-dd')}).$promise.then((result) => {
            if (result.workDay) {
                return this.$q.resolve(date);
            } else {
                const nextDate = new Date(date.getTime());
                nextDate.setDate(nextDate.getDate() + 1);
                return this.getNextWorkingDate(nextDate)
            }
        })
    }

}

angular.module('app').service('workingDateService', WorkingDateService);