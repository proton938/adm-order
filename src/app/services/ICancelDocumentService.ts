interface ICancelDocumentService {
    checkCanceledDocument(cancelDocument: CancelType);
    cancelDocument(canceledDocument: CancelType, expireDate: Date);
}

class CancelDocumentService implements ICancelDocumentService {
    static $inject = ['$q', '$filter', 'alertService', 'orderResource'];

    constructor(private $q: ng.IQService,
                private $filter: any,
                private alertService: oasiWidgets.IAlertService,
                private orderResource: IOrderResource) {
    }

    checkCanceledDocument(cancelDocument: CancelType) {
        if (cancelDocument && cancelDocument.canceledDocumentID) {

            let documentName = cancelDocument.canceledDocumentType;

            if (cancelDocument.canceledDocumentNum) {
                documentName += ' № ' + cancelDocument.canceledDocumentNum;
            }

            if (cancelDocument.canceledDocumentDate) {
                let documentDate = this.$filter('date')(cancelDocument.canceledDocumentDate, "dd.MM.yyyy");

                documentName += ' от ' + documentDate;
            }

            return this.alertService.confirm({
                message: 'Аннулируемый документ ' +
                documentName +
                ' будет аннулирован. Отмена данной операции и смена аннулируемого документа невозможны. ' +
                'Проверьте правильность выбора аннулируемого документа',
                okButtonText: 'Продолжить',
                type: 'danger'
            });
        } else {
            return this.$q.resolve();
        }

    }

    cancelDocument(canceledDocument: CancelType, expireDate: Date) {
        if (!canceledDocument || !canceledDocument.canceledDocumentID) {
            return this.$q.resolve(true);
        }

        return this.orderResource.isExists({id: canceledDocument.canceledDocumentID}).$promise.then((result) => {
            if (!result.exists) {
                return this.alertService.message({
                    message: 'Аннулируемый документ отсутствует в подсистеме Приказы'
                }).catch(() => {
                    return this.$q.resolve();
                });
            }
            return this.orderResource.cancelDocument({
                id: canceledDocument.canceledDocumentID,
                expireDate: this.$filter('date')(expireDate, "yyyy-MM-dd"),
                send: false
            }, {}).$promise;
        }).then(() => {
            return this.alertService.message({
                message: 'В аннулируемом документе изменен статус на «Аннулирован», установлена дата аннулирования'
            }).catch(() => {
                return this.$q.resolve();
            });
        });
    }

}

angular.module('app').service('cancelDocumentService', CancelDocumentService);