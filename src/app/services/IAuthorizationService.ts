interface IAuthorizationService {
    check(subject: string, action?: string): boolean;
}

class AuthorizationService implements IAuthorizationService {
    static $inject = ['$q', 'toastr', 'prSession'];
    
    constructor(private $q: ng.IQService, private toastr: Toastr, private prSession: ISessionStorage) {
    }

    check(resource: string, action = "view"): boolean {
        return this.prSession.hasPermission(resource);
    }
}

angular.module('app').service('prAuthorizationService', AuthorizationService);