class MggtService {
    static $inject = [
        '$q',
        'orderResource',
    ];

    constructor(private $q: ng.IQService,
                private orderResource: IOrderResource) {
    }

    private needToSendOrder(order: Order) {
        return order.mailList && _.find(order.mailList, user => user.accountName === 'serov_au');
    }


    public sendOrder(order: Order): ng.IPromise<any> {
        if (this.needToSendOrder(order)) {
            return this.orderResource.sendToMggt({id: order.documentID}).$promise;
        }
        return this.$q.resolve();
    }

}

angular.module('app').service('mggtService', MggtService);
