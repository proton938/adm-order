'use strict';

interface ISessionStorage {
    cleanSession(): void;
    setAuthenticationResult(result: any): void;
    hasPermission(permission: string): boolean;
    rdTypes(): string[];
}

class SessionStorage implements ISessionStorage {
    static $inject = ['$sessionStorage', '$base64'];

    constructor(private $sessionStorage: angular.storage.IStorageService, private $base64: IBase64) {
    }

    public cleanSession(): void {
        this.$sessionStorage["pr-permissions"] = null;
        this.$sessionStorage["pr-rdTypes"] = null;
    }

    public setAuthenticationResult(result: any): void {
        this.$sessionStorage["pr-permissions"] = result.permissions;
        this.$sessionStorage["pr-rdTypes"] = result.rdTypes;
    }

    public rdTypes(): string[] {
        return this.$sessionStorage["pr-rdTypes"];
    }

    public hasPermission(permission: string): boolean {
        return this.$sessionStorage["pr-permissions"].indexOf(permission) !== -1;
    }
}

angular.module('app').service('prSession', SessionStorage);
