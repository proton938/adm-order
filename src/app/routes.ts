interface PermissionState extends angular.ui.IState {
    permission?: string;
}

interface PermissionStateProvider extends angular.ui.IStateProvider {
    state(name: string, config: PermissionState): PermissionStateProvider;

    state(config: PermissionState): PermissionStateProvider;
}

angular
    .module('app')
    .config(['$stateProvider', '$urlRouterProvider', '$locationProvider', '$httpProvider', 'toastrConfig', routesConfig])
    .run(['$rootScope', '$state', '$stateParams', 'authRestService', 'authenticationService', 'prAuthorizationService', 'prSession', 'session',
        ($rootScope: ng.IRootScopeService, $state: ng.ui.IStateService,
         $stateParams: ng.ui.IStateParamsService,
         authRestService: oasiAuthRest.AuthRestService,
         authService: IAuthenticationService,
         authorizationService: IAuthorizationService,
         prSession: ISessionStorage,
         session: oasiSecurity.ISessionStorage) => {
            $rootScope.$on('$stateChangeStart', function (event: ng.IAngularEvent, toState: PermissionState, toStateParams: ng.ui.IStateParamsService, fromState: any) {
                if (!session.isAuthenticated()) {
                    event.preventDefault();
                    authService.checkSession(
                        (response: AuthUser) => {
                            prSession.setAuthenticationResult(response);
                            authRestService.user().then((user) => {
                                session.setAuthenticationResult(user);
                                $state.go(toState, toStateParams);
                            })
                        },
                        () => {

                        }
                    );
                } else if (toState.permission && !toStateParams['permissionChecked']) {
                    event.preventDefault();
                    let result = authorizationService.check(toState.permission, 'view');

                    toStateParams['permissionChecked'] = true;
                    toStateParams['permissionFailed'] = !result;
                    $state.go(toState, toStateParams);
                }
            });
        }
    ]);

/** @ngInject */
function routesConfig($stateProvider: PermissionStateProvider,
                      $urlRouterProvider: angular.ui.IUrlRouterProvider,
                      $locationProvider: angular.ILocationProvider,
                      $httpProvider: ng.IHttpProvider,
                      toastrConfig: any) {

    $httpProvider.defaults.withCredentials = true;
    $httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';
    $httpProvider.interceptors.push('errorHandlerInterceptor');

    $urlRouterProvider.otherwise(($injector, $location) => {
        var $state = $injector.get("$state");
        $state.go('app.adm-order.orders');
    });

    angular.extend(toastrConfig, {
        timeOut: 8000,
        extendedTimeOut: 2000
    });

    $stateProvider
        .state('app', {
            abstract: true,
            url: '/app',
            params: {
                permissionChecked: false,
                permissionFailed: false
            },
            template: '<app></app>'
        })
        .state('app.adm-order', {
            abstract: true,
            url: '/adm-order',
            template: '<adm-order-app></adm-order-app>'
        })
        .state('app.adm-order.upload-scan', {
            url: '/upload-scan/:documentID',
            template: '<upload-scan></upload-scan>',
            permission: 'OASI_ADMORDER_SCANBUTTON',
            params: {
                documentID: null
            },
            data: {title: 'Загрузка файла'}
        })
        .state('app.adm-order.attach-originals', {
            url: '/attach-originals',
            template: '<attach-originals></attach-originals>',
            data: {title: 'Прикрепление оригиналов'}
        })
        .state('app.adm-order.initiate', {
            url: '/initiate',
            template: '<order-initiate></order-initiate>',
            permission: 'OASI_ADMORDER_CARD',
            params: {
                linkedDocumentID: null,
                linkedDocumentType: null
            },
            data: {title: 'Сформировать проект РД'}
        })
        .state('app.adm-order.orders', {
            url: '/orders?query',
            params: {
                query: null,
                filter: null
            },
            template: '<orders></orders>',
            permission: 'OASI_ADMORDER_VIEW_ALL',
            data: {title: 'Приказы'}
        })
        .state('app.adm-order.my-tasks', {
            url: '/my-tasks?query',
            params: {},
            template: '<my-tasks></my-tasks>',
            data: {title: 'Мои задачи'}
        })
        .state('app.adm-order.not-scanned', {
            url: '/not-scanned',
            template: '<not-scanned></not-scanned>',
            permission: 'OASI_ADMORDER_VIEW_SCAN',
            data: {title: 'Ожидают сканирования'}
        })
        .state('app.adm-order.not-sent', {
            url: '/not-sent',
            template: '<not-sent></not-sent>',
            permission: 'OASI_ADMORDER_VIEW_MAIL',
            data: {title: 'Ожидают рассылки'}
        })
        .state('app.adm-order.order', {
            url: '/order/:id',
            params: {
                id: null,
                editingOrder: false
            },
            template: '<order></order>',
            permission: 'OASI_ADMORDER_CARD',
            data: {
                title: 'Приказ',
                parent: {
                    title: 'Приказы',
                },
            }
        })
        .state('app.adm-order.process', {
            abstract: true,
            url: '/process/:documentID/task/:taskId',
            template: '<process></process>',
            params: {
                documentID: null,
                taskId: null
            },
        })
        .state('app.adm-order.process.PrepareRD', {
            url: '/PrepareRD',
            params: {},
            template: '<process-prepare></process-prepare>',
            data: {title: 'Формирование распорядительного документа'}
        })
        .state('app.adm-order.process.NegotiationProjectOrder', {
            url: '/NegotiationProjectOrder',
            params: {},
            template: '<process-approve class="hidden-xs hidden-sm"></process-approve>' +
                      '<mobile-process-approve class="visible-xs visible-sm"></mobile-process-approve>',
            data: {title: 'Согласование распорядительного документа'},
            controller: ['$localStorage', function ($localStorage) {
              this.$localStorage = $localStorage;
            }],
            controllerAs: "$stateCtrl"
        })
        .state('app.adm-order.process.PrintAndSignDoc', {
            url: '/PrintAndSignDoc',
            params: {},
            template: '<process-print-and-sign></process-print-and-sign>',
            data: {title: 'Печать и подпись распорядительного документа'}
        })
        .state('app.adm-order.process.ReviewApprovedRD', {
            url: '/ReviewApprovedRD',
            params: {},
            template: '<process-intro></process-intro>',
            data: {title: 'Ознакомление с распорядительным документа'}
        })
        .state('app.adm-order.process.EditDoc', {
            url: '/EditDoc',
            params: {},
            template: '<process-edit-doc></process-edit-doc>',
            data: {title: 'Доработка распорядительного документа'}
        })
        .state('app.adm-order.process.RegisterRD', {
            url: '/RegisterRD',
            params: {},
            template: '<process-register></process-register>',
            data: {title: 'Регистрация распорядительного документа'}
        })
        .state('app.adm-order.process.ScanRD', {
            url: '/ScanRD',
            params: {},
            template: '<process-scan></process-scan>',
            data: {title: 'Сканирование распорядительного документа'}
        })
        .state('app.adm-order.process.ToIsogdOnceMore', {
            url: '/ToIsogdOnceMore',
            params: {},
            template: '<process-toisogd></process-toisogd>',
            data: {title: 'Повторить отправку в ИСОГД'}
        })
        .state('app.adm-order.process.PublishRD', {
            url: '/PublishRD',
            params: {},
            template: '<process-publish></process-publish>',
            data: {title: 'Публикация распорядительного документа'}
        })
        .state('diagram', {
            url: '/diagram/:title/:image',
            template: '<diagram></diagram>',
            params: {
                image: "",
                title: ""
            }
        });

}
