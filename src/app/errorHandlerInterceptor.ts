class ErrorHandlerInterceptor {
    static $inject = ['$q', '$injector', 'nsi_url'];

    constructor(private $q: ng.IQService,
                private $injector: any,
                private nsiUrl: string) {
    }

    handleError(httpResponse: any): any {
        switch (httpResponse.status) {
            case 401:
                let currentLocation = window.location.pathname + window.location.search + window.location.hash;
                // # решетку нужно заменить иначе роутер обрубит остаток урла после
                window.location.href = '/?from=' + currentLocation.replace(/#/, '^');
                break;
            default:
                let toastr: Toastr = this.$injector.get('toastr');
                if (httpResponse.data && httpResponse.data.message) {
                    toastr.warning(httpResponse.data.message, "Внимание");
                } else {
                    toastr.warning("Произошла ошибка. Обратитесь к администратору.", "Внимание");
                }
                break;
        }
        return this.$q.reject(httpResponse);
    }

    responseError = this.handleError.bind(this);
}

angular.module('app').service('errorHandlerInterceptor', ErrorHandlerInterceptor);
