/**
 * модуль обеспечения серверного логгирования
 */
let logModule = angular.module("app");

//server writer
logModule.run([function () {
    ServerLogger.init('rd');
}]);

//js errors
logModule.config(globalExceptionHandlerConfig);

//http layer interceptors
logModule.factory('httpErrorInterceptor', ['$stateParams', '$q', function ($stateParams, $q) {
    return new ADMORDERServerErrorHandler($q);
}]);
logModule.factory('loginInterceptor', ['$stateParams', '$q', function ($stateParams, $q) {
    //два похожих запроса - поиск по АД и авторизация
    return new LoginInterceptor(new UrlPatternMatcher('/auth/user', ['users'], null));
}]);
logModule.factory('authenticationInterceptor', ['$stateParams', '$q', function ($stateParams, $q) {
    return new LogoutInterceptor(new UrlPatternMatcher('/auth/logout', null, null));
}]);
logModule.factory('activityTaskInterceptor', ['$stateParams', '$q', function ($stateParams, $q) {
    return new ActivityTaskInterceptor($stateParams, new UrlPatternMatcher('/service/runtime/tasks', null, null));
}]);
logModule.factory('documentsSearchInterceptor', ['$stateParams', '$q', function ($stateParams, $q) {
    return new DocumentsSearchInterceptor(new UrlPatternMatcher('/orders?query', null, null));
}]);
logModule.factory('documentUpdateInterceptor', ['$stateParams', '$q', function ($stateParams, $q) {
    return new DocumentUpdateInterceptor($stateParams, new UrlPatternMatcher('api/v1/documents', null, null));
}]);
logModule.factory('fileUploadInterceptor', ['$stateParams', '$q', function ($stateParams, $q) {
    return new FileUploadInterceptor($stateParams, new UrlPatternMatcher('\/orders\\/\\w+\\/files\\/add',
        ['createFolder', 'changeFolderInfo', 'sign', 'hashhex', 'folderinfo', 'delete', 'folderContent', 'fileinfo', 'verify'],
        null));
}]);
logModule.factory('fileDownloadInterceptor', ['$stateParams', '$q', function ($stateParams, $q) {
    return new FileDownloadInterceptor($stateParams, new UrlPatternMatcher('/files/w+',
        ['createFolder', 'changeFolderInfo', 'sign', 'hashhex', 'folderinfo', 'delete', 'folderContent', 'fileinfo', 'verify'],
        null));
}]);

logModule.config(['$httpProvider', function ($httpProvider: ng.IHttpProvider) {
    $httpProvider.interceptors.push('httpErrorInterceptor');
    $httpProvider.interceptors.push('loginInterceptor');
    $httpProvider.interceptors.push('authenticationInterceptor');
    $httpProvider.interceptors.push('activityTaskInterceptor');
    $httpProvider.interceptors.push('documentsSearchInterceptor');
    $httpProvider.interceptors.push('documentUpdateInterceptor');
    $httpProvider.interceptors.push('fileUploadInterceptor');
    $httpProvider.interceptors.push('fileDownloadInterceptor');
}]);

//app transitions monitor
logModule.run(['$stateParams', '$state', '$rootScope', function ($stateParams, $state, $rootScope) {
    let monitor = new RoutesTransitionMonitor($stateParams, $rootScope);

    //showcases
    monitor.statesMap.push(['app.adm-order.orders', ($stateParams) => {
        serverLog(new Action(ActionType.OPEN_FORM, new OpenFormMessage('Витрина Все приказы и распоряжения')))
    }]);

    monitor.statesMap.push(['app.adm-order.my-tasks', ($stateParams) => {
        serverLog(new Action(ActionType.OPEN_FORM, new OpenFormMessage('Витрина Мои задачи')));
    }]);

    monitor.statesMap.push(['app.adm-order.not-scanned', ($stateParams) => {
        serverLog(new Action(ActionType.OPEN_FORM, new OpenFormMessage('Витрина Ожидают сканирования')));
    }]);

    monitor.statesMap.push(['app.adm-order.not-sent', ($stateParams) => {
        serverLog(new Action(ActionType.OPEN_FORM, new OpenFormMessage('Витрина Ожидают рассылки')));
    }]);

    //card
    monitor.statesMap.push(['app.adm-order.order', ($stateParams) => {
        serverLog(new Action(ActionType.OPEN_FORM, new OpenFormMessage('Карточка Приказа', $stateParams['id'])));
    }]);

    monitor.statesMap.push(['app.adm-order.process.PrepareRD', ($stateParams) => {
        serverLog(new Action(ActionType.OPEN_FORM, new OpenFormMessage('Карточка Формирование распорядительного документа')));
    }]);

    monitor.statesMap.push(['app.adm-order.process.NegotiationProjectOrder', ($stateParams) => {
        serverLog(new Action(ActionType.OPEN_FORM, new OpenFormMessage('Карточка Согласование распорядительного документа')));
    }]);

    monitor.statesMap.push(['app.adm-order.process.PrintAndSignDoc', ($stateParams) => {
        serverLog(new Action(ActionType.OPEN_FORM, new OpenFormMessage('Карточка Печать и подпись распорядительного документа')));
    }]);

    monitor.statesMap.push(['app.adm-order.process.EditDoc', ($stateParams) => {
        serverLog(new Action(ActionType.OPEN_FORM, new OpenFormMessage('Карточка Доработка распорядительного документа')));
    }]);

    monitor.statesMap.push(['app.adm-order.process.RegisterRD', ($stateParams) => {
        serverLog(new Action(ActionType.OPEN_FORM, new OpenFormMessage('Карточка Регистрация распорядительного документа')));
    }]);

    monitor.statesMap.push(['app.adm-order.process.ScanRD', ($stateParams) => {
        serverLog(new Action(ActionType.OPEN_FORM, new OpenFormMessage('Карточка Сканирование распорядительного документа')));
    }]);

    monitor.start();
}]);
