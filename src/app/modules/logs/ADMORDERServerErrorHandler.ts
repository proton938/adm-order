class ADMORDERServerErrorHandler {
    constructor(private $q?: angular.IQService) {}

    response = (response: any) => {
        if (response.status >= 500) {
            serverLog(new Action(ActionType.SERVER_ERROR, new ServerErrorMessage(response)));
        }
        return response;
    };

    responseError = (response: any): any => {
        // фильтрую "шум"
        if (response.status !== 401) {
            serverLog(new Action(ActionType.SERVER_ERROR, new ServerErrorMessage(response)));
        }
        // framework agnostic
        return this.$q ? this.$q.reject(response) : response;
    };
}

angular.module('app').service('ADMORDERServerErrorHandler', ADMORDERServerErrorHandler);
