class PageTitle implements ng.IDirective {
    restrict = 'A';

    static factory(): ng.IDirectiveFactory {
        const directive = ($rootScope: ng.IRootScopeService, $timeout: ng.ITimeoutService) => new PageTitle($rootScope, $timeout);
        directive.$inject = ['$rootScope', '$timeout'];
        return directive;
    }

    constructor(private $rootScope: ng.IRootScopeService, private $timeout: ng.ITimeoutService) {
    }

    link = ($scope: ng.IScope, element: ng.IAugmentedJQuery) => {
        var listener = (event: ng.IAngularEvent, toState: ng.ui.IState, toParams: any, fromState: ng.ui.IState, fromParams: any) => {
            var title = 'ОАСИ МКА';

            if (toState.data && toState.data.title) {
                title = 'ОАСИ МКА ' + toState.data.title;
            }

            this.$timeout(function () {
                element.text(title);
            });
        };
        this.$rootScope.$on('$stateChangeStart', listener);
    }
}

angular.module('app').directive('pageTitle', PageTitle.factory());
