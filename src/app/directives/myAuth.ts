class MyAuth implements ng.IDirective {
    restrict = 'A';

    static factory(): ng.IDirectiveFactory {
        const directive = ($rootScope: ng.IRootScopeService, authorizationService: IAuthorizationService) => new MyAuth($rootScope, authorizationService);
        directive.$inject = ['$rootScope', 'prAuthorizationService'];
        return directive;
    }

    constructor(private $rootScope: ng.IRootScopeService, private authorizationService: IAuthorizationService) {
    }
    
    setVisible(element: ng.IAugmentedJQuery, visible: boolean) {
        if (visible) {
            element.removeClass('hidden');
        } else {
            element.addClass('hidden');
        }        
    }
    
    defineVisibility(element: ng.IAugmentedJQuery, subject: string, action: string) {
        let result = this.authorizationService.check(subject, action);
        this.setVisible(element, result);
    }

    link = ($scope: ng.IScope, element: ng.IAugmentedJQuery, attrs: any) => {
        let subject: string = attrs.myAuth.replace(/\s+/g, '');
        let action: string = attrs.authAction || 'view';
        if (subject.length > 0) {
            this.setVisible(element, false);
            this.defineVisibility(element, subject, action);
        }
    }
}

angular.module('app').directive('myAuth', MyAuth.factory());
