interface IFileIconScope extends ng.IScope {
    fileName: string;
    format: string;
}

class FileIcon implements ng.IDirective {
    restrict: string;
    templateUrl: string;
    scope: any;
    formats: string[];
    fileName: string;

    static factory(): ng.IDirectiveFactory {
        const directive = () => new FileIcon();
        directive.$inject = [];
        return directive;
    }

    constructor() {
        this.restrict = 'E';
        this.scope = {
            fileName: "<"
        };
        this.formats = ['csv', 'doc', 'docx', 'dwg', 'pdf', 'ppt', 'pptx', 'rar', 'txt', 'xml', 'zip', 'xls', 'xlsx'];
    }

    link = ($scope: IFileIconScope, element: JQuery, $attrs: ng.IAttributes, ngModel: ng.INgModelController) => {
        $scope.$watch(() => {
            var ind = $scope.fileName.lastIndexOf(".");
            var ext = $scope.fileName.substr(ind + 1, $scope.fileName.length).toLowerCase();
            let format: string = _.find(this.formats, function (f) {
                return f === ext;
            });
            if (!format) {
                format = 'unknown';
            }
            return format;
        }, (format) => {
            console.log("File format has been changed to " + format);
            element.children().remove();
            $('<img>').attr('src', 'images/icons/' + format + '.png').addClass('our-icon-file-md').appendTo(element);
        })
    }
}

angular.module('app').directive('fileIcon', FileIcon.factory());