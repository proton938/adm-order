interface IDateUtils {
    formatDate(date: Date): string;
    formatRequestDates(request:any): void;
    parseResponseDates(response:any): void;
    parseDates(input: any): void;
}

function DateUtils(dateFilter: any): IDateUtils {
    
    function formatDate(date: Date): string {
        return dateFilter(date, 'yyyy-MM-dd');
    }
    
    function formatDates(input: any) {
        for (let key in input) {
            if (!input.hasOwnProperty(key)) continue;

            if (angular.isDate(input[key])) {
                let date: Date = input[key];
                input[key] = formatDate(date);
            } else if (typeof input[key] === "object") {
                formatDates(input[key]);
            }
        }
    }    
    
    function parseDates(input: any) {
        for (let key in input) {
            if (!input.hasOwnProperty(key)) continue;

            if (typeof input[key] === "object") {
                parseDates(input[key]);
            } else {
                let match = /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}.*$/.exec(input[key]);

                if (typeof input[key] === "string" && match) {
                    input[key] = new Date(input[key]);
                }
                match = /^\d{4}-\d{2}-\d{2}$/.exec(input[key]);
                if (typeof input[key] === "string" && match) {
                    input[key] = new Date(input[key]);
                }
            }
        }
    }  
    
    function parseResponseDates(response:any) {
        parseDates(response.data);
        return response.data;
    }
    
    function formatRequestDates(request:any) {
        let result: any = angular.copy(request);
        formatDates(result);
        return angular.toJson(result);
    }    

    return {
        formatDate: formatDate,
        formatRequestDates: formatRequestDates,
        parseResponseDates: parseResponseDates,
        parseDates: parseDates
    };
    
}

DateUtils.$inject = ['dateFilter'];

angular.module('app').service('dateUtils', DateUtils);