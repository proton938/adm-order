interface IAuthenticationResource extends ng.resource.IResourceClass<any> {
    logout: ng.resource.IResourceMethod<any>;
    user: ng.resource.IResourceMethod<AuthUser>;
}

function AuthenticationResource($resource: ng.resource.IResourceService, baseUrl: string): IAuthenticationResource {

    let logout: ng.resource.IActionDescriptor = {
        url: '/auth/logout',
        method: 'POST'
    };

    let user: ng.resource.IActionDescriptor = {
        url: baseUrl + '/auth/user',
        method: 'GET'
    };
    
    return <IAuthenticationResource>$resource(null, {}, {
        logout: logout,
        user: user
    });

}

AuthenticationResource.$inject = ['$resource', 'base_url'];

angular.module('app').service('authenticationResource', AuthenticationResource);
