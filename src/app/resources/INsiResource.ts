interface INsiResource extends ng.resource.IResourceClass<any> {
    isWorkDay: ng.resource.IResourceMethod<ng.resource.IResource<WorkDayResult>>;
    toLocalDateTime: ng.resource.IResourceMethod<ng.resource.IResource<any>>;
    calcDuration: ng.resource.IResourceMethod<ng.resource.IResource<any>>;
    divideTimeBy: ng.resource.IResourceMethod<ng.resource.IResource<any>>;
    departments: ng.resource.IResourceMethod<Array<any>>;
}

class WorkDayResult {
    workDay: boolean;
}

function NsiResource($q: ng.IQService, $resource: ng.resource.IResourceService, nsiUrl: string): INsiResource {

    function getWorkDayResult(response: any): any {
        return {workDay: response.data};
    }

    function simplyReturnValue(response: any): any {
        return {value: response}
    }

    let isWorkDay: ng.resource.IActionDescriptor = {
        url: nsiUrl + '/businessCalendar/isWorkDay/:date',
        method: 'GET',
        interceptor: {response: getWorkDayResult}
    };

    let toLocalDateTime: ng.resource.IActionDescriptor = {
        url: nsiUrl + '/datetime/zonedDateTime/:zonedDateTime/toLocalDateTime',
        method: 'GET',
        transformResponse: simplyReturnValue
    };

    let calcDuration: ng.resource.IActionDescriptor = {
        url: nsiUrl + '/businessCalendar/calcDuration/:localDateTime',
        method: 'GET',
        transformResponse: simplyReturnValue
    };

    let divideTimeBy: ng.resource.IActionDescriptor = {
        url: nsiUrl + '/datetime/duration/:duration/divideBy/:divideBy',
        method: 'GET',
        transformResponse: simplyReturnValue
    };

    let departments: ng.resource.IActionDescriptor = {
        url: nsiUrl + '/users/departments',
        method: 'GET',
        isArray: true
    };

    return <INsiResource>$resource(null, {}, {
        isWorkDay,
        toLocalDateTime,
        calcDuration,
        divideTimeBy,
        departments
    });
}

NsiResource.$inject = ['$q', '$resource', 'nsi_url'];

angular.module('app').service('nsiResource', NsiResource);