interface IReportResource extends ng.resource.IResourceClass<any> {
	getApprovalReportUrl(id: string): string;
}

function ReportResource($resource: ng.resource.IResourceService, baseUrl: string): IReportResource {

    let result = <IReportResource>$resource(null, {}, {
    });
    result.getApprovalReportUrl = (id: string) => {
    	return baseUrl + '/wordReporter/order/' + id + '/previewReportFromTemplate/AdmOrderLS';
    }
    return result;
}

ReportResource.$inject = ['$resource', 'base_url'];

angular.module('app').service('reportResource', ReportResource);