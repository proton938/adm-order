class IdWrapper {
    id?: string;
}

class OrderCreateResult {
    id: string;
}

class OrderCountResult {
    $promise: angular.IPromise<any>;
    foundTotal: number;
}

class OrderGenerateNumberResult {
    $promise: angular.IPromise<any>;
    documentNumber: string;
}

class OrderFileDeleteResult {
    $promise: angular.IPromise<any>;
}

class OrderExistsResult {
    $promise: angular.IPromise<any>;
    exists: boolean;
}

class OrderSearchResult {
    $promise: angular.IPromise<any>;
    content: Order[];
    pageNumber: number;
    pageSize: number;
    totalPages: number;
    totalElements: number;
}

class OrderWrapper {
    document: Order
}

class OrderBarcodeResponse {
    $promise: angular.IPromise<any>;
    barcode: number;
    barcodeText: string
}

interface IOrderResource extends ng.resource.IResourceClass<any> {
    searchQuery: ng.resource.IResourceMethod<OrderSearchResult>;
    generateNumber: ng.resource.IResourceMethod<OrderGenerateNumberResult>;
    cancelDocument: ng.resource.IResourceMethod<OrderGenerateNumberResult>;
    mail: ng.resource.IResourceMethod<OrderGenerateNumberResult>;
    search: ng.resource.IResourceMethod<OrderSearchResult>;
    exportExcel: ng.resource.IResourceMethod<void>;
    exportExcelQuery: ng.resource.IResourceMethod<void>;
    notSent: ng.resource.IResourceMethod<OrderSearchResult>;
    notSentCount: ng.resource.IResourceMethod<OrderCountResult>;
    notScanned: ng.resource.IResourceMethod<OrderSearchResult>;
    notScannedCount: ng.resource.IResourceMethod<OrderCountResult>;
    getById: ng.resource.IResourceMethod<ng.resource.IResource<Order>>;
    getRelated: ng.resource.IResourceMethod<ng.resource.IResource<any>>;
    create: ng.resource.IResourceMethod<OrderCreateResult>;
    update: ng.resource.IResourceMethod<void>;
    patch: ng.resource.IResourceMethod<void>;
    sendToPSO: ng.resource.IResourceMethod<void>;
    isExists: ng.resource.IResourceMethod<OrderExistsResult>;
    deleteFile: ng.resource.IResourceMethod<OrderFileDeleteResult>;
    createDraftFile: ng.resource.IResourceMethod<OrderDraftFile>;
    getApprovalList: ng.resource.IResourceMethod<OrderApprovalListItem[]>;
    approve: ng.resource.IResourceMethod<ng.resource.IResource<OrderApprovalListItem>>;
    prepareSign: ng.resource.IResourceMethod<FileType>;
    closeCycle: ng.resource.IResourceMethod<void>;
    createDraftFilesWithoutAtt: ng.resource.IResourceMethod<ng.resource.IResource<OrderDraftFile>>;
    deleteDraftFilesWithoutAtt: ng.resource.IResourceMethod<void>;
    deleteDraftFilesAtt: ng.resource.IResourceMethod<void>;
    deleteDraftFiles: ng.resource.IResourceMethod<void>;
    combineDraftFiles: ng.resource.IResourceMethod<void>;
    addApprovers: ng.resource.IResourceMethod<ng.resource.IResource<OrderApprovalListItem[]>>;
    register: ng.resource.IResourceMethod<ng.resource.IResource<any>>;
    getCancelingDocumentId: ng.resource.IResourceMethod<ng.resource.IResource<IdWrapper>>;
    generateBarcode: ng.resource.IResourceMethod<OrderBarcodeResponse>;
    createIsogdXml: ng.resource.IResourceMethod<any>;
    sendToMggt: ng.resource.IResourceMethod<any>;
    log: ng.resource.IResourceMethod<ng.resource.IResource<any>>;
}

function OrderResource($resource: ng.resource.IResourceService, baseUrl: string, dateUtils: IDateUtils, $base64: IBase64): IOrderResource {
    function parseOrders(data: any) {
        let response = data.data;
        let orders: Order[] = _.map(response.content, parseOrder);

        response.content = orders;

        return response;
    }

    function parseOrder(response: any) {
        let order = response.document;

        dateUtils.parseDates(order);

        return order;
    }

    function parseDates(response: any) {
        dateUtils.parseDates(response.data);
        return response.data;
    }

    function parserOrderResponse(response) {
        return parseOrder(response.data);
    }

    let searchQuery: ng.resource.IActionDescriptor = {
        url: baseUrl + '/orders',
        method: 'GET',
        interceptor: {response: parseOrders}
    };

    let search: ng.resource.IActionDescriptor = {
        url: baseUrl + '/orders/filter',
        method: 'POST',
        transformRequest: dateUtils.formatRequestDates,
        interceptor: {response: parseOrders}
    };

    let exportExcel: ng.resource.IActionDescriptor = {
        url: baseUrl + '/orders/filter/excel',
        method: 'POST',
        responseType: 'arraybuffer',
        transformRequest: dateUtils.formatRequestDates,
        interceptor: {response: parseOrders}
    };

    let exportExcelQuery: ng.resource.IActionDescriptor = {
        url: baseUrl + '/orders/excel',
        method: 'GET',
        responseType: 'arraybuffer',
        interceptor: {response: parseOrders}
    };

    let notScanned: ng.resource.IActionDescriptor = {
        url: baseUrl + '/orders/not-scanned',
        method: 'POST',
        interceptor: {response: parseOrders}
    };

    let notScannedCount: ng.resource.IActionDescriptor = {
        url: baseUrl + '/orders/not-scanned/count',
        method: 'GET'
    };

    let notSent: ng.resource.IActionDescriptor = {
        url: baseUrl + '/orders/not-sent',
        method: 'GET',
        interceptor: {response: parseOrders}
    };

    let notSentCount: ng.resource.IActionDescriptor = {
        url: baseUrl + '/orders/not-sent/count',
        method: 'GET'
    };

    let generateNumber: ng.resource.IActionDescriptor = {
        url: baseUrl + '/orders/next-number',
        method: 'GET'
    };

    let cancelDocument: ng.resource.IActionDescriptor = {
        url: baseUrl + '/orders/:id/cancel',
        method: 'POST'
    };

    let sendToPSO: ng.resource.IActionDescriptor = {
        url: baseUrl + '/orders/:id/send?includeFile=:includeFile',
        method: 'POST'
    };

    let isExists: ng.resource.IActionDescriptor = {
        url: baseUrl + '/orders/:id/exists',
        method: 'GET',
    };

    let deleteFile: ng.resource.IActionDescriptor = {
        url: baseUrl + '/orders/:id/files/delete',
        method: 'POST'
    };

    let createDraftFile: ng.resource.IActionDescriptor = {
        url: baseUrl + '/orders/:id/draftFiles/create',
        method: 'POST'
    };

    let mail: ng.resource.IActionDescriptor = {
        url: baseUrl + '/orders/:id/mail',
        method: 'POST'
    };

    let getById: ng.resource.IActionDescriptor = {
        url: baseUrl + '/orders/:id',
        method: 'GET',
        interceptor: {response: parserOrderResponse}
    };

    let getRelated: ng.resource.IActionDescriptor = {
        url: baseUrl + '/orders/:id/related',
        method: 'GET'
    };

    let create: ng.resource.IActionDescriptor = {
        url: baseUrl + '/orders',
        method: 'POST'
    };

    let update: ng.resource.IActionDescriptor = {
        url: baseUrl + '/orders/:id',
        method: 'POST'
    };

    let patch: ng.resource.IActionDescriptor = {
        url: baseUrl + '/orders/:id',
        method: 'PATCH'
    };

    let getApprovalList: ng.resource.IActionDescriptor = {
        url: baseUrl + '/orders/approvalList',
        method: 'GET',
        isArray: true
    };

    let approve: ng.resource.IActionDescriptor = {
        url: baseUrl + '/orders/:id/approval',
        method: 'POST'
    };

    let prepareSign: ng.resource.IActionDescriptor = {
        url: baseUrl + '/orders/:id/sign/prepare',
        method: 'POST',
        interceptor: {response: parseDates}
    };

    let closeCycle: ng.resource.IActionDescriptor = {
        url: baseUrl + '/orders/:id/approval/cycle/close',
        method: 'POST'
    };

    let createDraftFilesWithoutAtt: ng.resource.IActionDescriptor = {
        url: baseUrl + '/orders/:id/draftFilesWithoutAtt/create',
        method: 'POST'
    };

    let deleteDraftFilesWithoutAtt: ng.resource.IActionDescriptor = {
        url: baseUrl + '/orders/:id/draftFilesWithoutAtt/delete',
        method: 'POST'
    };

    let deleteDraftFilesAtt: ng.resource.IActionDescriptor = {
        url: baseUrl + '/orders/:id/draftFilesAtt/delete',
        method: 'POST'
    };

    let deleteDraftFiles: ng.resource.IActionDescriptor = {
        url: baseUrl + '/orders/:id/draftFiles/delete',
        method: 'POST'
    };

    let combineDraftFiles: ng.resource.IActionDescriptor = {
        url: baseUrl + '/orders/:id/draftFiles/combine',
        method: 'POST'
    };

    let createIsogdXml: ng.resource.IActionDescriptor = {
        url: baseUrl + '/orders/:id/isogdXml/create',
        method: 'POST'
    };

    let addApprovers: ng.resource.IActionDescriptor = {
        url: baseUrl + '/orders/:id/approval/addApprovers',
        method: 'POST',
        isArray: true
    };

    let register: ng.resource.IActionDescriptor = {
        url: baseUrl + '/orders/:id/approval/register',
        method: 'POST'
    };

    let getCancelingDocumentId: ng.resource.IActionDescriptor = {
        url: baseUrl + '/orders/:id/canceling',
        method: 'GET'
    };

    let generateBarcode: ng.resource.IActionDescriptor = {
        url: baseUrl + '/orders/next-barcode',
        method: 'GET'
    };


    let sendToMggt: ng.resource.IActionDescriptor = {
        url: baseUrl + '/toMggt/order/:id',
        method: 'POST'
    };

    let log: ng.resource.IActionDescriptor = {
        url: baseUrl + '/log/:id/asc',
        method: 'GET',
        isArray: true
    };

    return <IOrderResource>$resource(null, {
        id: '@id',
        sort: '@sort',
        page: '@page',
        size: '@size',
        send: '@send',
        expireDate: '@expireDate',
        type: '@type',
        fileGuid: '@fileGuid',
        query: '@query',
        year: '@year'
    }, {
        searchQuery: searchQuery,
        deleteFile: deleteFile,
        createDraftFile: createDraftFile,
        generateNumber: generateNumber,
        search: search,
        getById: getById,
        create: create,
        mail: mail,
        cancelDocument: cancelDocument,
        update: update,
        patch: patch,
        notScanned: notScanned,
        notScannedCount: notScannedCount,
        notSent: notSent,
        notSentCount: notSentCount,
        getApprovalList: getApprovalList,
        approve: approve,
        prepareSign: prepareSign,
        closeCycle: closeCycle,
        exportExcel,
        exportExcelQuery,
        isExists,
        sendToPSO,
        createDraftFilesWithoutAtt,
        deleteDraftFilesWithoutAtt,
        deleteDraftFilesAtt,
        deleteDraftFiles,
        combineDraftFiles,
        addApprovers,
        register,
        getCancelingDocumentId,
        getRelated,
        generateBarcode,
        createIsogdXml,
        sendToMggt,
        log
    });
}

OrderResource.$inject = ['$resource', 'base_url', 'dateUtils', '$base64'];

angular.module('app').service('orderResource', OrderResource);
