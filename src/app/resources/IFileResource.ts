interface IFileResource extends ng.resource.IResourceClass<any> {
    deleteApprovalFile: ng.resource.IResourceMethod<any>;
    deletePaperFile: ng.resource.IResourceMethod<any>;
    deleteFile: ng.resource.IResourceMethod<ng.resource.IResource<any>>;
    deleteAdditionalMaterial: ng.resource.IResourceMethod<ng.resource.IResource<any>>;
    getUploadUrl(id: string): string;
    getApprovalUploadUrl(id: string, taskId: string): string;
    getPaperFileUploadUrl(id: string, taskId: string): string;
    getDraftFilesWithoutAttUploadUrl(id: string): string;
    getDraftFilesAttUploadUrl(id: string): string;
    getAdditionalMaterialUploadUrl(id: string): string;
    getDownloadUrl(id: string): string;
}

function FileResource($resource: ng.resource.IResourceService, baseUrl: string, filenetUrl: string): IFileResource {

    let deleteApprovalFile: ng.resource.IActionDescriptor = {
        url: baseUrl + '/orders/:id/fileApproval/delete?taskId=:taskId&fileGuid=:fileGuid',
        method: 'POST'
    };

    let deleteFile: ng.resource.IActionDescriptor = {
        url: baseUrl + '/orders/:id/files/delete',
        method: 'POST'
    };

    let deletePaperFile: ng.resource.IActionDescriptor = {
        url: baseUrl + '/orders/:id/sign/paperFile/delete',
        method: 'POST'
    };

    let deleteAdditionalMaterial: ng.resource.IActionDescriptor = {
        url: baseUrl + '/orders/:id/additionalMaterial/delete',
        method: 'POST'
    };

    let result = <IFileResource>$resource(null, {id: '@id', fileGuid: '@fileGuid', taskId: '@taskId'}, {
        deleteApprovalFile: deleteApprovalFile,
        deletePaperFile: deletePaperFile,
        deleteFile: deleteFile,
        deleteAdditionalMaterial: deleteAdditionalMaterial
    });
    result.getUploadUrl = (id: string) => {
        return baseUrl + '/orders/' + id + '/files/add';
    };
    result.getApprovalUploadUrl = (id: string, taskId: string) => {
        return baseUrl + '/orders/' + id + '/fileApproval/add?taskId=' + taskId;
    };
    result.getPaperFileUploadUrl = (id: string, taskId: string) => {
        return baseUrl + '/orders/' + id + '/sign/paperFile/add?taskId=' + taskId;
    };
    result.getDraftFilesWithoutAttUploadUrl = (id: string) => {
        return baseUrl + '/orders/' + id + '/draftFilesWithoutAtt/add';
    };
    result.getDraftFilesAttUploadUrl = (id: string) => {
        return baseUrl + '/orders/' + id + '/draftFilesAtt/add';
    };
    result.getAdditionalMaterialUploadUrl = (id: string) => {
        return baseUrl + '/orders/' + id + '/additionalMaterial/add';
    };
    result.getDownloadUrl = (id: string) => {
        return filenetUrl + "/files/" + id;
    };
    return result;
     
}

FileResource.$inject = ['$resource', 'base_url', 'filenet_url'];

angular.module('app').service('fileResource', FileResource);