class SolrSearchResult {
    response: SolrSearchResponse;
}

class SolrSearchResponse {
    docs: SolrDoc[];
}

class SolrDoc {
    link: string;
}

interface ISolrResource extends ng.resource.IResourceClass<any> {
    searchExt: ng.resource.IResourceMethod<ng.resource.IResource<SolrSearchResult>>;
}

function SolrResource($resource: ng.resource.IResourceService, solrUrl: string): ISolrResource {

    let searchExt: ng.resource.IActionDescriptor = {
        url: solrUrl + '/ext',
        method: 'POST'
    };

    return <ISolrResource>$resource(null, {}, {
        searchExt
    });
}

SolrResource.$inject = ['$resource', 'solr_url'];

angular.module('app').service('solrResource', SolrResource);