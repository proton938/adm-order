var app = angular
    .module('app', [
        'ngAnimate',
        'ngCookies',
        'ngTouch',
        'ngSanitize',
        'ngMessages',
        'ngAria',
        'ngResource',
        'ui.router',
        'ui.bootstrap',
        'ui.validate',
        'toastr',
        'ngFileUpload',
        'base64',
        'ngStorage',
        'nk.touchspin',
        'angular-ladda',
        'ui.select',
        'datePicker',
        "localytics.directives",
        "datatables",
        "oasi.viewService",
        // "datatables.bootstrap",
        'ngCollection',
        'ngStomp',
        'oitozero.ngSweetAlert',
        'angular.filter',
        'oasi.cryptopro',
        'oasi.bpm.history',
        'oasi.footer',
        'oasi.navbar',
        'oasi.sidebar',
        'oasi.breadcrumbs',
        'oasi-config-box',
        'oasi-security',
        'thatisuday.dropzone',
        'oasi.bpm.rest',
        'oasi.nsi.rest',
        'oasi.file.rest',
        'oasi.widgets',
        'oasi.task.wrapper.component',
        'oasi.file.manager',
        'oasi.document.logs',
        'angular-jsoneditor'
    ])
    .config(["activityRestServiceProvider", "activiti_rest_url", (activityRestServiceProvider: oasiBpmRest.ActivityRestServiceProvider, activitiRestUrl: string) => {
        activityRestServiceProvider.root = activitiRestUrl;
    }])
    .config(["nsiRestServiceProvider", "nsi_url", (nsiRestServiceProvider: oasiNsiRest.NsiRestServiceProvider, nsiUrl: string) => {
        nsiRestServiceProvider.root = nsiUrl;
    }])
    .config(["fileHttpServiceProvider", "filenet_url", (fileHttpServiceProvider: oasiFileRest.FileHttpServiceProvider, filenetUrl: string) => {
        fileHttpServiceProvider.root = filenetUrl;
    }]);

// oasi-security component
app.constant('authServerApi', '/oasi/auth')
    .constant('nsiApi', '/nsi/nsi-api/v1');
